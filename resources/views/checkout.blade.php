<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EatNaija</title>
    <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,600,700" rel="stylesheet">
    <!-- inject:css-->
    <link rel="stylesheet" href="public/vendor_assets/css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="public/vendor_assets/css/brands.css">
    <link rel="stylesheet" href="public/vendor_assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="public/vendor_assets/css/jquery-ui.css">
    <link rel="stylesheet" href="public/vendor_assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="public/vendor_assets/css/line-awesome.min.css">
    <link rel="stylesheet" href="public/vendor_assets/css/magnific-popup.css">
    <link rel="stylesheet" href="public/vendor_assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="public/vendor_assets/css/select2.min.css">
    <link rel="stylesheet" href="public/vendor_assets/css/slick.css">
    <link rel="stylesheet" href="public/style.css">
    <!-- endinject -->
    <link rel="icon" type="image/png" sizes="32x32" href="public/favicon.ico">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/hilios/jQuery.countdown/2.1.0/dist/jquery.countdown.min.js"></script> 
</head>

<body>
@guest
    <section class="intro-wrapper bgimage overlay overlay--dark" >
        <div class="bg_image_holder"><img src="public/img/intro.jpg" alt=""></div>
        <div class="mainmenu-wrapper">
            <div class="menu-area menu1 menu--light">
                <div class="top-menu-area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="menu-fullwidth">
                                    <div class="logo-wrapper order-lg-0 order-sm-1">
                                        <div class="logo logo-top">
                                            <a href="/"><img src="public/img/logo-white.png" alt="logo image" class="img-fluid"></a>
                                        </div>
                                    </div><!-- ends: .logo-wrapper -->
                                    @if (Route::has('register'))
                                    <div class="menu-container order-lg-1 order-sm-0">
                                        <div class="d_menu">
                                            <nav class="navbar navbar-expand-lg mainmenu__menu">
                                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#direo-navbar-collapse" aria-controls="direo-navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                                                    <span class="navbar-toggler-icon icon-menu"><i class="la la-reorder"></i></span>
                                                </button>
                                                <!-- Collect the nav links, forms, and other content for toggling -->
                                                <div class="collapse navbar-collapse" id="direo-navbar-collapse">
                                                    <ul class="navbar-nav">
                                                        <li>
                                                            <a href="/">Home</a>
                                                        </li>
                                                        <li class="dropdown has_dropdown">
                                                          <a href="/about">About Us</a> 
                                                           
                                                        </li>
                                                         <li class="dropdown has_dropdown">
                                                           <a href="/contactus">Contact Us</a> 
                                                           
                                                        </li>
                                                        <li class="dropdown has_dropdown">
                                                            <a href="#" class="dropdown-toggle" id="drop4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
                                                            <ul class="dropdown-menu" aria-labelledby="drop4">
                                                            <li><a href="/restaurant">Restaurant</a></li>
                                                                <li><a href="/food_ports">Food Port</a></li>
                                                                <li><a href="/cafe">Cafe/Eatery</a></li>
                                                                <li><a href="/Healthy&Wellness">Healthy & Wellness Products</a></li>
                                                                <li><a href="/foodcompany">Food Company</a></li>
                                                                <li><a href="/foodequipment">Food Equipment</a></li>
                                                            </ul>
                                                        </li>
                                                      
                                                </div>
                                                <!-- /.navbar-collapse -->
                                            </nav>
                                        </div>
                                    </div>
                                    <div class="menu-right order-lg-2 order-sm-2">
                                       
                                        <!-- start .author-area -->
                                        <div class="author-area">
                                            <div class="author__access_area">
                                                <ul class="d-flex list-unstyled align-items-center">
                                                    <li>
                                                        <a href="add-listing.html" class="btn btn-xs btn-gradient btn-gradient-two" data-toggle="modal" data-target="#login_modal">
                                                          
                                                            <span class="la la-plus"></span> Sign in
                                                           
                                                        </a>
                                                    </li>
                                                    <li>
                                                        
                                                        <a href="" class="access-link" data-toggle="modal" data-target="#signup_modal">Register</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="offcanvas-menu d-none">
                                        <ul class="d-flex list-unstyled align-items-center">
                                                    <li>
                                                        <a href="add-listing.html" class="btn btn-xs btn-gradient btn-gradient-two" data-toggle="modal" data-target="#login_modal">
                                                            <span class="la la-plus"></span> Sign in
                                                        </a>
                                                    </li>
                                        </div><!-- ends: .offcanvas-menu -->
                                    </div><!-- ends: .menu-right -->
                                </div>
                            </div>
                        </div>
                        <!-- end /.row -->
                    </div>
                    <!-- end /.container -->
                </div>
                <!-- end  -->
            </div>
        </div><!-- ends: .mainmenu-wrapper -->
                                        @endif
						@else
                        <section class="intro-wrapper bgimage overlay overlay--dark" style="height:400px">
        <div class="bg_image_holder"><img src="public/img/intro.jpg" alt=""></div>
        <div class="mainmenu-wrapper">
            <div class="menu-area menu1 menu--light">
                <div class="top-menu-area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="menu-fullwidth">
                                    <div class="logo-wrapper order-lg-0 order-sm-1">
                                        <div class="logo logo-top">
                                            <a href="/"><img src="public/img/Eat-naija.png" alt="logo image" class="img-fluid"></a>
                                        </div>
                                    </div><!-- ends: .logo-wrapper -->
                                    
                                    <div class="menu-container order-lg-1 order-sm-0">
                                        <div class="d_menu">
                                            <nav class="navbar navbar-expand-lg mainmenu__menu">
                                               <button class="navbar-toggler offcanvas-menu__user" type="button" aria-expanded="false" aria-label="Toggle navigation">
                                                    <span class="navbar-toggler-icon icon-menu"><i class="la la-reorder"></i></span>
                                                </button>
                                                <!-- Collect the nav links, forms, and other content for toggling -->
                                                <div class="collapse navbar-collapse" id="direo-navbar-collapse" >
                                                    <ul class="navbar-nav" >
                                                        <li>
                                                            <a href="/">Home</a>
                                                        </li>
                                                        <li class="dropdown has_dropdown">
                                                           <a href="/about">About Us</a> 
                                                           
                                                        </li>
                                                         <li class="dropdown has_dropdown">
                                                           <a href="/contactus">Contact Us</a> 
                                                           
                                                        </li>
                                                        <li class="dropdown has_dropdown">
                                                            <a href="#" class="dropdown-toggle" id="drop4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
                                                            <ul class="dropdown-menu" aria-labelledby="drop4">
                                                            <li><a href="/restaurant">Restaurant</a></li>
                                                                <li><a href="/food_ports">Food Port</a></li>
                                                                <li><a href="/cafe">Cafe/Eatery</a></li>
                                                                <li><a href="/Healthy&Wellness">Healthy & Wellness Products</a></li>
                                                                <li><a href="/foodcompany">Food Company</a></li>
                                                                <li><a href="/foodequipment">Food Equipment</a></li>
                                                            </ul>
                                                        </li>
                                                      
                                                </div>
                                                <!-- /.navbar-collapse -->
                                            </nav>
                                        </div>
                                    </div>
                                    <div class="menu-right order-lg-2 order-sm-2">
                                       
                                        <!-- start .author-area -->
                                        <div class="author-area">
                                            <div class="author__access_area">
                                                <ul class="d-flex list-unstyled align-items-center">
                                                    <li>
                                                        <a href="/cart" class="btn btn-xs btn-gradient btn-gradient-two" >
                                                      <span class="la la-shopping-cart"></span>  <span> {{$cartnum}} </span> 
                                                        </a>
                                                    </li>
                                                    <li  class="btn btn-xs btn-gradient btn-gradient-two text-white"><a href="{{ route('logout') }}" title="Register" style="color:white" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                         {{ __('Logout') }}
                                    </a>
                                   
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end .author-area -->
                                        
                                                      
                                        <div class="offcanvas-menu d-none">
                                             <a href="/cart" class="btn btn-xs btn-gradient btn-gradient-two" style="margin-right:5px;" >
                                                      <span class="la la-shopping-cart"></span>  <span> {{$cartnum}} </span> 
                                                        </a>    
                                            
                                                   
                                            <div class="offcanvas-menu__contents">
                                                <a href="" class="offcanvas-menu__close"><i class="la la-times-circle"></i></a>
                                                <div class="author-avatar">
                                                    @if(Auth::user()->image==NULL)
                                                    <img src="public/img/author-avatar.png" alt="" class="rounded-circle">
                                                    @else
                                               <img src="public/storage/user_image/{{Auth::user()->image}}" alt="" class="rounded-circle" style="width:120px">
                                               @endif
                                                </div>
                                                <ul class="list-unstyled">
                                                       <li>
                                                            <a href="/">Home</a>
                                                        </li>
                                                        <li class="dropdown has_dropdown">
                                                           <a href="/about">About Us</a> 
                                                           
                                                        </li>
                                                         <li class="dropdown has_dropdown">
                                                           <a href="/contactus">Contact Us</a> 
                                                           
                                                        </li>
                                                        <li class="dropdown has_dropdown">
                                                            <a href="#" class="dropdown-toggle" id="drop4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
                                                            <ul class="dropdown-menu" aria-labelledby="drop4">
                                                            <li><a href="/restaurant">Restaurant</a></li>
                                                                <li><a href="/food_ports">Food Port</a></li>
                                                                <li><a href="/cafe">Cafe/Eatery</a></li>
                                                                <li><a href="/Healthy&Wellness">Healthy & Wellness Products</a></li>
                                                                <li><a href="/foodcompany">Food Company</a></li>
                                                                <li><a href="/foodequipment">Food Equipment</a></li>
                                                            </ul>
                                                        </li>
                                                    <li><a href="/profile{{ Auth::user()->id }}">My Profile</a></li>
                                                     <li><a href="{{ route('logout') }}" title="Register" style="color:black" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                         {{ __('Logout') }}
                                    </a>
                                   
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form></li>
                                                </ul>
                                                
                                            </div><!-- ends: .author-info -->
                                        </div><!-- ends: .offcanvas-menu -->
                                    </div><!-- ends: .menu-right -->
                                </div>
                            </div>
                        </div>
                        <!-- end /.row -->
                    </div>
                    <!-- end /.container -->
                </div>
                <!-- end  -->
            </div>
        </div><!-- ends: .mainmenu-wrapper -->
        <div class="directory_content_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="search_title_area">
                        
                            <h2 class="title">Checkout</h2>
                        </div><!-- ends: .search_title_area -->
                      
                       
                    </div><!-- ends: .col-lg-10 -->
                </div>
            </div>
        </div><!-- ends: .directory_search_area -->
        @endguest
        <main class="py-4">
            @yield('content')
        </main>
</section>

        <section class="checkout-wrapper section-padding-strict section-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 listing-items">
                    <div class="row">
                       
                       
                <div class="col-lg-12 order-0 order-lg-1">
                    <div class="checkout-form">
                        @if(!$cart)
                                            @else
                            <div class="checkout-table table-responsive">
                                <table id="directorist-checkout-table" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Cart</th>
                                            <th colspan="2">Name</th>
                                            <th><strong>Price</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          @foreach($cart  as $cat)
                                        <tr>
                                       
                                            <td>
                                                <div class="custom-control custom-checkbox checkbox-outline checkbox-outline-primary custom-control-inline">
                                                {!! Form::open(['action' =>['App\Http\Controllers\CartController@destroy_cart', $cat->id], 'method'=> 'POST'])!!}
                                                {{Form::hidden('_method','DELETE')}}

													<button class="btn btn-outline-danger m-right-10" type="submit">×</button>
													{!!Form::close()!!}
                                                </div>
                                            </td>

                                            <td>
                                                <div class="custom-control custom-checkbox checkbox-outline checkbox-outline-primary custom-control-inline">
                                                     <?php
                                                                  $filename = 'public/storage/upload_image/';


                                                            ?>
                                                         @if( file_exists($filename.'/'.$cat->image))
                                                      
                                                       
                                                        <img src="{{ asset('public/storage/upload_image/'.$cat->image) }}" alt="listing image" style="height:100px">
                              @else
                                     <img src="{{ $cat->image }}" alt="listing image" style="height:100px">
                                                        @endif
                                               
                                                </div>
                                            </td>
                                            
                                            <td>
                                                <h4>{{$cat->product_name}}</h4>
                                               
                                            </td>
                                            <td colspan="4" class="text-left ">
                                            <div id="atbdp_checkout_total_amount" style="width:100px">  &#8358 {{number_format($cat->price, 1)}} </div>
                                            </td>
                                            @endforeach
                                        </tr>
                                     
                                        <tr>
                                            <td colspan="4" class="text-right vertical-middle">
                                                <strong>Delivery</strong>
                                            </td>
                                            <td class="vertical-middle">
                                                <div id="atbdp_checkout_total_amount" >&#8358 {{number_format($delivery, 2)}}</div>
                                                <!--total amount will be populated by JS-->
                                            </td>
                                        </tr>
                                      
                                        <tr>
                                            <td colspan="4" class="text-right vertical-middle">
                                                <strong>Total amount</strong>
                                            </td>
                                            <td class="vertical-middle">
                                                <div id="atbdp_checkout_total_amount" >&#8358 {{number_format($total, 2)}}</div>
                                                <!--total amount will be populated by JS-->
                                            </td>
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div><!-- ends: .checkout-table -->
                             <div class="col-lg-12">
                            <div class="listings-sidebar">
                                <div class="search-area default-ad-search">
                                    
                                       
                                            <h5> Update Details</h5>
                                            
                                                        {!! Form::open(['action' => ['App\Http\Controllers\HomeController@update_user', Auth::user()->id], 'method' => 'POST']) !!}
                                                        
                                               {{ csrf_field() }}  
											
													<div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Phone</label>
      <input type="text" class="form-control" id="inputEmail4" placeholder="Phone Number" name="phone" value="{{ Auth::user()->phone }}"  required>
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">City</label>
      <input type="text" class="form-control" id="inputPassword4" placeholder="City"    name="city" value="{{ Auth::user()->city }}" required>
    </div>
  </div>
  <div class="form-group">
    <label for="inputAddress">Address</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="address" value="{{ Auth::user()->address }}" name="address" required>
  </div>
  <div class="form-group">
    <label for="inputAddress2">State</label>
    <input type="text" class="form-control" id="inputAddress2"  placeholder="State" value="{{ Auth::user()->state }}" name="state" required>
  </div>
 <button type="submit" class="btn btn-primary ">Update </button>
    </div>

												
												
           

    {!! Form::close() !!}
                                            
                                       

                            </div>
                        </div><!-- ends: .col-lg-4 -->
                        <br>
                            <div class="text-right">
    @php
$array = array(array('metaname' => 'color', 'metavalue' => 'blue'),
                array('metaname' => 'size', 'metavalue' => 'big'));
                
        $total = (int)$total * 100;
        
@endphp

<form method="POST" id="paymentForm" action="{{ route('pay') }}" >
     
{{ csrf_field() }}   
    <input type="hidden" id="amount" value="{{ $total }}" id="form3" class="form-control "  required/> 
    <input type="hidden" name="payment_method" value="both" /> 
    <input type="hidden" name="description" value="Fund Wallet" /> 
    <input type="hidden" name="country" value="NG" /> 
    <input type="hidden" name="currency" value="NGN" /> 
    <input type="hidden" id="email" value="{{ Auth::user()->email }}" /> 
    <input type="hidden" name="firstname" value="{{ Auth::user()->name }}" /> 
    <input type="hidden" name="metadata" value="{{ json_encode($array) }}" > 
    <input type="hidden" name="phonenumber" value="{{Auth::user()->phone}}" /> 
    {{-- <input type="hidden" name="paymentplan" value="362" />  --}}
    {{-- <input type="hidden" name="ref" value="MY_NAME_5uwh2a2a7f270ac98" />  --}}
    {{-- <input type="hidden" name="logo" value="https://pbs.twimg.com/profile_images/915859962554929153/jnVxGxVj.jpg" />  --}}
    {{-- <input type="hidden" name="title" value="Flamez Co" />  --}}
    {{-- <input type="hidden" name="delivery" value="{{ $delivery }}" />  --}}
                             

  @if ($delivery)
     <button class="btn btn-md btn-info" id="atbdp_checkout_submit_btn" type="button" onClick="payWithPaystack()">Pay Now</button>
     
  @else
    <b style="color:red;">Please update your Address to be able to complete your checkout</b>
  @endif
 <a href="/" class="btn btn-outline-danger m-right-10">Continue Shopping</a>
</form>
                      
                    </div><!-- ends: .checkout-form -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                
<script>
function activatePlacesSearch(){
			var input=document.getElementById('inputAddress');
			var autocomplete=new google.maps.places.Autocomplete(input);

		}
</script>
<script async
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD1wnXxMJDWVXxG9SEAgZyq_VIMO4fB9-s&libraries=places&callback=activatePlacesSearch">
</script>
                

<script src="https://js.paystack.co/v1/inline.js"></script> 

<script>
const paymentForm = document.getElementById('paymentForm');

paymentForm.addEventListener("submit", payWithPaystack, false);

function payWithPaystack(e) {

 

  let handler = PaystackPop.setup({

    key: 'pk_live_5b032da2a254503cbe843d173dc7bf9142be6a98', // Replace with your public key

    email: document.getElementById("email").value,

    amount: document.getElementById("amount").value,

    ref: ''+Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you

    // label: "Optional string that replaces customer email"

    onClose: function(){

      alert('Window closed.');

    },

    callback: function(response){
  $.ajax({
    url: 'http://www.yoururl.com/verify_transaction?reference='+ response.reference,
    method: 'get',
    success: function (response) {
      alert(response.data.status)
    }
  });
}

  });

  handler.openIframe();

}

</script> 

                    
                    
                     @endif
                </div><!-- ends: .col-lg-12 -->
            </div>
        </div>
    </section><!-- ends: .checkout-wrapper -->				

		