@extends('layout.footer')
@extends('layout.head')
@section('content')
<div class="directory_content_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="search_title_area">
                        <br><br>
                            <h2 class="title">Food Equipment</h2>
                            <p class="sub_title">EatNaija helps you find and buy meals from restaurants and food businesses in and around Nigeria..</p>
                        </div><!-- ends: .search_title_area -->
                        <form action="/search" method="get" class="search_form">
                            <div class="atbd_seach_fields_wrapper">
                                <div class="single_search_field search_query">
                                    <input class="form-control search_fields" name="item" type="text" placeholder="What are you looking for?">
                                </div>
                               
                                <div class="single_search_field ">
                                    <input class="form-control search_fields" name="location" type="text" placeholder="Location">
                                        
                                </div>
                                <div class="atbd_submit_btn">
                                    <button type="submit" class="btn btn-block btn-gradient btn-gradient-two btn-md btn_search">Search</button>
                                </div>
                            </div>
                        </form><!-- ends: .search_form -->
                        
                    </div><!-- ends: .col-lg-10 -->
                </div>
            </div>
        </div><!-- ends: .directory_search_area -->
        </section><!-- ends: .intro-wrapper -->


    <section class="all-listing-wrapper section-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="atbd_generic_header">
                        <div class="atbd_generic_header_title">
                            <h4>All Food Equipments</h4>
                            <p>Total Listing Found: {{ $vendors->count() }}</p>
                        </div><!-- ends: .atbd_generic_header_title -->
                        <div class="atbd_listing_action_btn btn-toolbar align-items-center" role="toolbar">
                            <!-- Views dropdown -->
                          
                            <!-- Orderby dropdown -->
                            
                        </div><!-- ends: .atbd_listing_action_btn -->
                    </div><!-- ends: .atbd_generic_header -->
                </div><!-- ends: .col-lg-12 -->
                <div class="col-lg-12 listing-items">
                    <div class="row">
                        <div class="col-lg-4 order-1 order-lg-0 mt-5 mt-lg-0">
                            <div class="listings-sidebar">
                                <div class="search-area default-ad-search">
                                    
                                        <div class="filter-checklist">
                                            <h5> Tags</h5>
                                            <div class="checklist-items tags-checklist">
                                                <div class="custom-control custom-checkbox checkbox-outline checkbox-outline-primary">
                                                    <input type="checkbox" class="custom-control-input" id="tag1" >
                                                    <label class="custom-control-label" for="tag1">Restaurant</label>
                                                </div>
                                                <div class="custom-control custom-checkbox checkbox-outline checkbox-outline-primary">
                                                    <input type="checkbox" class="custom-control-input" id="tag2">
                                                    <label class="custom-control-label" for="tag2">Food Port</label>
                                                </div>
                                                <div class="custom-control custom-checkbox checkbox-outline checkbox-outline-primary">
                                                    <input type="checkbox" class="custom-control-input" id="tag3" >
                                                    <label class="custom-control-label" for="tag3">Cafe/Eatery</label>
                                                </div>
                                                <div class="custom-control custom-checkbox checkbox-outline checkbox-outline-primary">
                                                    <input type="checkbox" class="custom-control-input" id="tag4">
                                                    <label class="custom-control-label" for="tag4">Healthy & Wellness Products</label>
                                                </div>
                                                <div class="custom-control custom-checkbox checkbox-outline checkbox-outline-primary">
                                                    <input type="checkbox" class="custom-control-input" id="tag5" >
                                                    <label class="custom-control-label" for="tag5">Food Company</label>
                                                </div>
                                                <div class="custom-control custom-checkbox checkbox-outline checkbox-outline-primary">
                                                    <input type="checkbox" class="custom-control-input" id="tag6" checked>
                                                    <label class="custom-control-label" for="tag6">Food Equipment</label>
                                                </div>
                                               
                                            </div>
                                        </div><!-- ends: .filter-checklist -->
                                       </div>
                            </div>
                        </div><!-- ends: .col-lg-4 -->
                        <div class="col-lg-8 order-0 order-lg-1">
                        @foreach($vendors as $vendor)
                            <div class="row">
                           
                                <div class="col-lg-12">
                               
                                    <div class="atbd_single_listing atbd_listing_list">
                                        <article class="atbd_single_listing_wrapper">
                                            <figure class="atbd_listing_thumbnail_area">
                                                <div class="atbd_listing_image">
                                                    <a href="menu{{$vendor->id}}">
                                                           <?php
                                                                  $filename = 'public/storage/vendor_image/';


                                                            ?>
                                                         @if( file_exists($filename.'/'.$vendor->image))
                                                      
                                                       
                                                        <img src="{{ asset('public/storage/vendor_image/'.$vendor->image) }}" alt="listing image" >
                              @else
                                     <img src="{{ $vendor->image }}" alt="listing image" style="height:300px">
                                                        @endif
                                                    </a>
                                                </div><!-- ends: .atbd_listing_image -->
                                                <div class="atbd_thumbnail_overlay_content">
                                                    <ul class="atbd_upper_badge">
                                                        <li><span class="atbd_badge atbd_badge_featured">Featured</span></li>
                                                    </ul><!-- ends .atbd_upper_badge -->
                                                </div><!-- ends: .atbd_thumbnail_overlay_content -->
                                            </figure><!-- ends: .atbd_listing_thumbnail_area -->
                                            <div class="atbd_listing_info">
                                                <div class="atbd_content_upper">
                                                    <h4 class="atbd_listing_title">
                                                        <a href="menu{{$vendor->id}}">{{ $vendor->company_name }}</a>
                                                    </h4>
                                                    <div class="atbd_listing_meta">
                                                        @php
                                                         if(!$vendor->rating){
                                                              $vendor->rating=0;
                                                               
                                                          }else{
                                                             $vendor->rating;
                                                          } 
                                                          @endphp
                                                        <span class="atbd_meta atbd_listing_rating" style="background:orange">{{ $vendor->rating }}<i class="la la-star"></i></span>
                                                        
                                                        <span class="atbd_meta atbd_badge_open">Open Now</span>
                                                    </div><!-- End atbd listing meta -->
                                                    <div class="atbd_listing_data_list">
                                                        <ul>
                                                            <li>
                                                                <p><span class="la la-map-marker"></span>{{ $vendor->address }}</p>
                                                            </li>
                                                           
                                                            <li>
                                                                <p><span class="la la-calendar-check-o"></span>Joined {{ $vendor->created_at->format('M-Y') }}</p>
                                                            </li>
                                                        </ul>
                                                    </div><!-- End atbd listing meta -->
                                                </div><!-- end .atbd_content_upper -->
                                                <div class="atbd_listing_bottom_content">
                                                    <div class="atbd_content_left">
                                                        <div class="atbd_listing_category">
                                                            <a href=""><span class="la la-cutlery"></span>{{ $vendor->category }}</a>
                                                        </div>
                                                    </div>
                                                    
                                                </div><!-- end .atbd_listing_bottom_content -->
                                            </div><!-- ends: .atbd_listing_info -->
                                        </article><!-- atbd_single_listing_wrapper -->
                                    </div>
                                 
                                </div><!-- ends: .col-lg-12 -->
                               
                </div><!-- ends: .listing-items -->
                @endforeach
            </div>
             </div>
            <div class="row">
                                <div class="col-lg-12">
                                    <nav class="navigation pagination d-flex justify-content-end" role="navigation">
                                        <div class="nav-links">
                                        {{ $vendors->links() }}
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div><!-- ends: .col-lg-8 -->
                    </div>
                </div><!-- ends: .listing-items -->
            
        </div>
        </section>
        @endsection