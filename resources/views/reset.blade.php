
@extends('layout.footer')
@extends('layout.head')
@section('content')
<div class="directory_content_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="search_title_area">
                        <br><br>
                            <h2 class="title">Reset Password</h2>
                          
                        
                    </div><!-- ends: .col-lg-10 -->
                </div>
            </div>
        </div><!-- ends: .directory_search_area -->
        </section><!-- ends: .intro-wrapper -->



        <section class="checkout-wrapper section-padding-strict section-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 listing-items">
                    <div class="row">
                         <div class="col-lg-12">
                            <div class="listings-sidebar">
                                <div class="search-area default-ad-search">
                             <h5> Reset Password</h5>
                             
                                                        {!! Form::open(['action' => ['App\Http\Controllers\HomeController@reset', $user->id], 'method' => 'POST']) !!}
                                               {{ csrf_field() }} 
                        	<div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">New Password</label>
      <input type="password" class="form-control" id="inputEmail4" placeholder="Password" name="password"  required>
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Confirm Password</label>
      <input type="password" class="form-control" id="inputPassword4" placeholder="Confirm Password"    name="password_confirmation"  required>
    </div>
  </div>
  
 <button type="submit" class="btn btn-primary ">Change my Password</button>
    </div>
     {!! Form::close() !!}
 </div><!-- ends: .col-lg-12 -->
            </div>
        </div>
    </section><!-- ends: .checkout-wrapper -->				

			@endsection