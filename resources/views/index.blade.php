
@extends('layout.footer')
@extends('layout.head')
@section('content')
<div class="modal" id="mod" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Advert Placement</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
      
      @foreach($adverts as $key => $advert)
    <div class="carousel-item {{$key == 0 ? 'active' : '' }}">
      <img class="d-block w-100 h75" src="{{ asset('public/storage/adverts/'.$advert->image) }}" alt="Advert slide">
    </div>
    @endforeach
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
 </div>
    </div>
  </div>
</div>
<div class="directory_content_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="search_title_area">
                        <br><br>
                            <h2 class="title">DISCOVER, REVIEW, EXPLORE</h2>
                            <p class="sub_title">EatNaija helps you find and buy meals from restaurants and food businesses in and around Nigeria..</p>
                           
                        </div><!-- ends: .search_title_area -->
                        <form action="/search" method="get" class="search_form">
                            <div class="atbd_seach_fields_wrapper">
                                <div class="single_search_field search_query">
                                    <input class="form-control search_fields" name="item" type="text" placeholder="What are you looking for?">
                                </div>
                               
                                <div class="single_search_field ">
                                    <input class="form-control search_fields" name="location"  type="text" placeholder="Location">
                                        
                                </div>
                                <div class="atbd_submit_btn">
                                    <button type="submit" class="btn btn-block btn-gradient btn-gradient-two btn-md btn_search">Search</button>
                                </div>
                            </div>
                        </form><!-- ends: .search_form -->
                       
                    </div><!-- ends: .col-lg-10 -->
                </div>
            </div>
        </div><!-- ends: .directory_search_area -->
    </section><!-- ends: .intro-wrapper -->
    <section class="categories-cards section-padding-two">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title"><br><br>
                        <h2>What are you looking for?</h2>
                        <p>Explore And Connect With Great Local Food Vendors...</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="category-single category--img">
                        <figure class="category--img4">
                            <img src="public/img/cat1.jpg" alt="">
                            <figcaption class="overlay-bg">
                                <a href="/restaurant" class="cat-box">
                                    <div>
                                        <div class="icon">
                                            <span class="la la-cutlery"></span>
                                        </div>
                                        <h4 class="cat-name">Restaurants</h4>
                                        <span class="badge badge-pill badge-success"  style="background:orange">{{ $restaurant->count() }} Listings</span>
                                    </div>
                                </a>
                            </figcaption>
                        </figure>
                    </div><!-- ends: .category-single -->
                </div><!-- ends: .col -->
                <div class="col-lg-4 col-sm-6">
                    <div class="category-single category--img">
                        <figure class="category--img4">
                            <img src="public/img/farm.jpg" style="height:280px" alt="">
                            <figcaption class="overlay-bg">
                                <a href="/food_ports" class="cat-box">
                                    <div>
                                        <div class="icon">
                                            <span class="la la-bank"></span>
                                        </div>
                                        <h4 class="cat-name">Food Port</h4>
                                        <span class="badge badge-pill badge-success" style="background:orange">{{ $food_port->count() }}  Listings</span>
                                    </div>
                                </a>
                            </figcaption>
                        </figure>
                    </div><!-- ends: .category-single -->
                </div><!-- ends: .col -->
                <div class="col-lg-4 col-sm-6">
                    <div class="category-single category--img">
                        <figure class="category--img4">
                            <img src="public/img/cat6.jpg"  alt="">
                            <figcaption class="overlay-bg">
                                <a href="/cafe" class="cat-box">
                                    <div>
                                        <div class="icon">
                                            <span class="la la-glass"></span>
                                        </div>
                                        <h4 class="cat-name"> Cafe/Eatery</h4>
                                        <span class="badge badge-pill badge-success"  style="background:orange">{{ $cafe->count() }} Listings</span>
                                    </div>
                                </a>
                            </figcaption>
                        </figure>
                    </div><!-- ends: .category-single -->
                </div><!-- ends: .col -->
                <div class="col-lg-4 col-sm-6">
                    <div class="category-single category--img">
                        <figure class="category--img4">
                            <img src="public/img/health.jpeg" style="height:280px" alt="">
                            <figcaption class="overlay-bg">
                                <a href="/Healthy&Wellness" class="cat-box">
                                    <div>
                                        <div class="icon">
                                            <span class="la la-map-marker"></span>
                                        </div>
                                        <h4 class="cat-name">Healthy & Wellness Products</h4>
                                        <span class="badge badge-pill badge-success"  style="background:orange">{{ $snack->count() }} Listings</span>
                                    </div>
                                </a>
                            </figcaption>
                        </figure>
                    </div><!-- ends: .category-single -->
                </div><!-- ends: .col -->
                <div class="col-lg-4 col-sm-6">
                    <div class="category-single category--img">
                        <figure class="category--img4">
                            <img src="public/img/foodco.jpeg" style="height:280px" alt="">
                            <figcaption class="overlay-bg">
                                <a href="/foodcompany" class="cat-box">
                                    <div>
                                        <div class="icon">
                                            <span class="la la-bed"></span>
                                        </div>
                                        <h4 class="cat-name">Food Company</h4>
                                        <span class="badge badge-pill badge-success"  style="background:orange">{{ $foodC->count() }} Listings</span>
                                    </div>
                                </a>
                            </figcaption>
                        </figure>
                    </div><!-- ends: .category-single -->
                </div><!-- ends: .col -->
                <div class="col-lg-4 col-sm-6">
                    <div class="category-single category--img">
                        <figure class="category--img4">
                            <img src="public/img/equip.jpeg" style="height:280px" alt="">
                            <figcaption class="overlay-bg">
                                <a href="/foodequipment" class="cat-box">
                                    <div>
                                        <div class="icon">
                                            <span class="la la-shopping-cart"></span>
                                        </div>
                                        <h4 class="cat-name">Food Equipment</h4>
                                        <span class="badge badge-pill badge-success"  style="background:orange">{{ $foodE->count() }} Listings</span>
                                    </div>
                                </a>
                            </figcaption>
                        </figure>
                    </div><!-- ends: .category-single -->
                </div><!-- ends: .col -->
            </div>
        </div>
    </section><!-- ends: .categories-cards -->
    <section class="listing-cards section-bg section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Restaurants Around Nigeria</h2>
                        <p>Explore the popular Restaurants around Nigeria</p>
                    </div>
                </div>
                <div class="listing-cards-wrapper col-lg-12">
               
                    <div class="row">
                    @foreach($vendors as $vendor)
                        <div class="col-lg-4 col-sm-6">
                      
                            <div class="atbd_single_listing ">
                                <article class="atbd_single_listing_wrapper">
                                    <figure class="atbd_listing_thumbnail_area">
                                        <div class="atbd_listing_image " >
                                            <a href="menu{{$vendor->id}}">
                                                  <?php
                                                                  $filename = 'public/storage/vendor_image/';


                                                            ?>
                                                         @if( file_exists($filename.'/'.$vendor->image))
                                                      
                                                       
                                                        <img class="img-fluid" src="{{ asset('public/storage/vendor_image/'.$vendor->image) }}" style="height:200px" alt="listing image" >
                                                         @elseif(!$vendor->image)
                              <img src="https://via.placeholder.com/400x300/EBFAFF/001E29/?text={{$vendor->vendor_name}}"  class="img-fluid" alt="listing image" style="height:200px;"/> <!-- max-width:100%; -->
                                                  
                              @else
                                     <img class="img-fluid" src="{{ $vendor->image }}" alt="listing image" style="height:200px">
                                                       
                                                         @endif
                                            </a>
                                        </div><!-- ends: .atbd_listing_image -->
                                        <div class="atbd_author atbd_author--thumb">
                                            <a href="menu{{$vendor->id}}">
                                                   <?php
                                                                  $filename = 'public/storage/vendor_image/';


                                                            ?>
                                                         @if( file_exists($filename.'/'.$vendor->image))
                                                      
                                                       
                                                        <img class="img-fluid" src="{{ asset('public/storage/vendor_image/'.$vendor->image) }}" style="height:28px" alt="listing image" >
                                                         @elseif(!$vendor->image)
                              <img src="https://via.placeholder.com/400x300/EBFAFF/001E29/?text={{$vendor->vendor_name}}"  class="img-fluid" alt="listing image" style="height:28px;"/> <!-- max-width:100%; -->
                                                  
                              @else
                                     <img class="img-fluid" src="{{ $vendor->image }}" alt="listing image" style="height:28px">
                                                       
                                                         @endif
                                                <span class="custom-tooltip">{{ $vendor->company_name }}</span>
                                            </a>
                                        </div>
                                        <div class="atbd_thumbnail_overlay_content">
                                            <ul class="atbd_upper_badge">
                                                <li><span class="atbd_badge atbd_badge_featured">Featured</span></li>
                                            </ul><!-- ends .atbd_upper_badge -->
                                        </div><!-- ends: .atbd_thumbnail_overlay_content -->
                                    </figure><!-- ends: .atbd_listing_thumbnail_area -->
                                    <div class="atbd_listing_info">
                                        <div class="atbd_content_upper">
                                            <h4 class="atbd_listing_title">
                                                <a href="">{{ $vendor->company_name }}</a>
                                            </h4>
                                            <div class="atbd_listing_meta">
  @php
                                                         if(!$vendor->rating){
                                                              $vendor->rating=0;
                                                               
                                                          }else{
                                                             $vendor->rating;
                                                          } 
                                                          @endphp
                                                        <span class="atbd_meta atbd_listing_rating"  style="background:orange">{{ $vendor->rating }}<i class="la la-star"></i></span>                                               
                                                <span class="atbd_meta atbd_badge_open">Open Now</span>
                                            </div><!-- End atbd listing meta -->
                                            <div class="atbd_listing_data_list">
                                                <ul>
                                                    <li>
                                                        <p><span class="la la-map-marker"></span>{{ $vendor->address }}</p>
                                                    </li>
                                                
                                                    <li>
                                                        <p><span class="la la-calendar-check-o"></span>Joined {{ $vendor->created_at->format('M-Y') }}</p>
                                                    </li>
                                                </ul>
                                            </div><!-- End atbd listing meta -->
                                        </div><!-- end .atbd_content_upper -->
                                        <div class="atbd_listing_bottom_content">
                                            <div class="atbd_content_left">
                                                <div class="atbd_listing_category">
                                                    <a href=""><span class="la la-glass"></span>{{ $vendor->category }}</a>
                                                </div>
                                            </div>
                                           
                                        </div><!-- end .atbd_listing_bottom_content -->
                                    </div><!-- ends: .atbd_listing_info -->
                                </article><!-- atbd_single_listing_wrapper -->
                            </div>
                           
                        </div><!-- ends: .col-lg-4 -->
                        @endforeach
                        <div class="col-lg-12 text-center m-top-20">
                            <a href="/restaurant" class="btn btn-gradient btn-gradient-two">Explore All</a>
                        </div>
                    </div>
                </div><!-- ends: .listing-cards-wrapper -->
            </div>
        </div>
    </section><!-- ends: .listing-cards -->
    <section class="cta section-padding border-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>How It Works?</h2>
                        <p>Get A Free Meal In Three Easy Steps</p>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row align-items-center">
                        <div class="col-lg-5 col-md-12">
                            <img src="public/img/12.png" alt="" class="img-fluid" style="height:700px;">
                        </div>
                        <div class="col-lg-5 offset-lg-1 col-md-6 mt-5 mt-md-0">
                            <ul class="feature-list-wrapper list-unstyled">
                                <li>
                                    <div class="icon"><span class="circle-secondary"><i class="la la-check-circle"></i></span></div>
                                    <div class="list-content">
                                        <h4>Find Restaurants around you</h4>
                                        <p>Locate the best restaurants close to your current location with a few clicks.</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon"><span class="circle-success"><i class="la la-line-chart"></i></span></div>
                                    <div class="list-content">
                                        <h4>Review & Explore your taste</h4>
                                        <p>Review restaurants according to services provided and taste.</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon"><span class="circle-primary"><i class="la la-money"></i></span></div>
                                    <div class="list-content">
                                        <h4>Get Discounts</h4>
                                        <p>You get rewarded with free or discount meals for reviewing selected restaurant listings</p>
                                    </div>
                                </li>
                            </ul><!-- ends: .feature-list-wrapper -->
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- ends: .cta -->
   
   
   
    <section class="subscribe-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <h1>Subscribe to Newsletter</h1>
                    <p>Subscribe to get update and information. Don't worry, we won't send spam!</p>
                    <form action="{{ route('newsletter') }}" method="post" class="subscribe-form m-top-40">
                         @csrf
                        <div class="form-group">
                            <span class="la la-envelope-o"></span>
                            <input type="email" placeholder="Enter your email" name="email" required>
                        </div>
                        <button type="submit" class="btn btn-gradient btn-gradient-two">Submit</button>
                    </form>
                   
                </div>
            </div>
        </div>
    </section><!-- ends: .subscribe-wrapper -->

   <script>
    $(document).ready(function(){
        $("#mod").modal('show');
    });
</script>
 
    @endsection
   