@extends('layout.footer')
@extends('layout.head')
@section('content')
 <title>{{ $vendor->company_name }}</title>

@php 
    $title = $vendor->company_name;
    
@endphp
<meta property="og:title" content="{{$title}}" />
<meta property="og:type" content="website" />

    <style>
        .containerr{
  width: 300px;
  background: #111;
  padding: 5px 15px;
  border: 1px solid #444;
  border-radius: 5px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
}
.containerr .post{
  display: none;
}
.containerr .text{
  font-size: 18px;
  color: #666;
  font-weight: 100;
}
.containerr .edit{
  position: absolute;
  right: 5px;
  top: 5px;
  font-size: 16px;
  color: #666;
  font-weight: 200;
  cursor: pointer;
}
.containerr .edit:hover{
  text-decoration: underline;
}
.containerr .star-widget input{
  display: none;
}
.star-widget label{
  font-size: 30px;
  color: #444;
  padding: 10px;
  float: right;
  transition: all 0.2s ease;
}
input:not(:checked) ~ label:hover,
input:not(:checked) ~ label:hover ~ label{
  color: #fd4;
}
input:checked ~ label{
  color: #fd4;
}
input#rate-5:checked ~ label{
  color: #fe7;
  text-shadow: 0 0 20px #952;
}
#rate-1:checked ~ form header:before{
  content: "I just hate it 😠";
}
#rate-2:checked ~ form header:before{
  content: "I don't like it 😒";
}
#rate-3:checked ~ form header:before{
  content: "It is awesome 😄";
}
#rate-4:checked ~ form header:before{
  content: "I just like it 😎";
}
#rate-5:checked ~ form header:before{
  content: "I just love it 😍";
}
.containerr form{
  display: none;
}
input:checked ~ form{
  display: block;
}
form header{
  width: 100%;
  font-size: 25px;
  color: #fe7;
  font-weight: 500;
  margin: 5px 0 20px 0;
  text-align: center;
  transition: all 0.2s ease;
}
form .textarea{
  height: 100px;
  width: 100%;
  overflow: hidden;
}
form .textarea textarea{
  height: 100%;
  width: 100%;
  outline: none;
  color: #eee;
  border: 1px solid #333;
  background: #222;
  padding: 10px;
  font-size: 17px;
  resize: none;
}
.textarea textarea:focus{
  border-color: #444;
}

form .btn{
  height: 45px;
  width: 100%;
  margin: 15px 0;
}
form .btn button{
  height: 100%;
  width: 100%;
  border: 1px solid #444;
  outline: none;
  background: #222;
  color: #999;
  font-size: 17px;
  font-weight: 500;
  text-transform: uppercase;
  cursor: pointer;
  transition: all 0.3s ease;
}

form .btn button:hover{
  background: #1b1b1b;
}

    </style>                    
        <div class="listing-info content_above">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <ul class="list-unstyled listing-info--badges">
                            <li><span class="atbd_badge atbd_badge_featured">Featured</span></li>
                            <li><span class="atbd_badge atbd_badge_popular">Popular</span></li>
                        </ul>
                        <ul class="list-unstyled listing-info--meta">
                             @php
                                                         if(!$vendor->rating){
                                                              $vendor->rating=0;
                                                               
                                                          }else{
                                                             $vendor->rating;
                                                          } 
                                                          @endphp
                            <li>
                                <div class="average-ratings">
                                    <span class="atbd_meta atbd_listing_rating"  style="background:orange">{{ $vendor->rating }}<i class="la la-star"></i></span>
                                    <span><strong>{{ $ratings->count() }}</strong> Reviews</span>
                                </div>
                            </li>
                            <li>
                                <div class="atbd_listing_category">
                                    <a href=""><span class="la la-glass"></span>{{ $vendor->category }}</a>
                                </div>
                            </li>
                        </ul><!-- ends: .listing-info-meta -->
                        <h1>{{ $vendor->company_name }}</h1>
                        
                    </div>
                    <div class="col-lg-4 col-md-5 d-flex align-items-end justify-content-start justify-content-md-end">
                        <div class="atbd_listing_action_area">
                            
                            <div class="atbd_action atbd_share dropdown">
                                <span class="dropdown-toggle" id="social-links" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu">
                                    <span class="la la-share"></span>Share
                                </span>
                                <div class="atbd_director_social_wrap dropdown-menu" aria-labelledby="social-links">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="http://www.facebook.com/sharer/sharer.php?u=https://eatnaija.com/menu{{$vendor->id}}&title={{$vendor->company_name}} on Eatnaija" target="_blank"><span class="fab fa-facebook-f color-facebook"></span>Facebook</a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/share?url=[https://eatnaija.com/menu{{$vendor->id}}]&text=[{{$vendor->company_name}}]&via=[via]&hashtags=[hashtags]"  target="_blank"><span class="fab fa-twitter color-twitter"></span>Twitter</a>
                                        </li>
                                      <li>
                                            <a href="http://www.linkedin.com/shareArticle?mini=true&url=https://eatnaija.com/menu{{$vendor->id}}&title={{$vendor->company_name}} on Eatnaija&source=gaafiqbn@business77.web-hosting.com" target="_blank"><span class="fab fa-linkedin-in color-linkedin"></span>LinkedIn</a>
                                        </li>
                            
                                       
                                        <li>
                                             <a href="https://api.whatsapp.com/send?text= https://eatnaija.com/menu{{$vendor->id}} {{$vendor->company_name}}" meta property="og:title" content="{{$vendor->company_name}}" target="_blank" title="{{$vendor->company_name}}"><span class="fab fa-whatsapp color-whatsapp text-success"></span>Whatsapp</a>
                                           
                                        </li>
                                    </ul>
                                </div>
                                
                            </div>
                            
                            <!-- Report Abuse-->
                            <div class="atbd_action atbd_report">
                                <div class="action_button">
                                    <a href="" data-toggle="modal" data-target="#atbdp-report-abuse-modal"><span class="la la-flag-o"></span> Report</a>
                                </div>
                                <!-- Modal (report abuse form) -->
                            </div>
                        </div><!-- ends: .atbd_listing_action_area -->
                    </div>
                </div>
            </div>
            <div class="modal fade" id="atbdp-report-abuse-modal" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="atbdp-report-abuse-modal-label">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <form action="{{ route('report') }}" method="post" id="atbdp-report-abuse-form" class="form-vertical">
                            @csrf
                            <div class="modal-header">
                                <h3 class="modal-title" id="atbdp-report-abuse-modal-label">Report Abuse</h3>
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="hidden" class="form-control" name="company_name" value="{{$vendor->company_name}}" >
                                    <input type="hidden" class="form-control" name="company_id" value="{{$vendor->id}}" >
                                    <label for="atbdp-report-abuse-message" class="not_empty">Your Complaint<span class="atbdp-star">*</span></label>
                                    <textarea class="form-control" id="atbdp-report-abuse-message" name="reports" rows="4" placeholder="Message..." required=""></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-secondary btn-sm">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="moda_claim_listing" tabindex="-1" role="dialog" aria-labelledby="claim_listing_label" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="claim_listing_label"><i class="la la-check-square"></i> Claim This Listing</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" placeholder="Your Name" class="form-control" required>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="email" class="form-control" placeholder="Email Address" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" placeholder="Phone Number" required>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="url" class="form-control" placeholder="Listing URL" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <textarea class="form-control" rows="6" placeholder="Provie Listing Information" required></textarea>
                                            <button type="submit" class="btn btn-secondary">Submit Query</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div><!-- ends: .listing-info -->
        </section><!-- ends: .intro-wrapper -->
</section>

    <section class="all-listing-wrapper section-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="atbd_generic_header">
                        <div class="atbd_generic_header_title">
                            <h4>{{ $vendor->company_name }}</h4>
                            <p>Total Listing Found: {{ $vendors->count() }}</p>
                        </div><!-- ends: .atbd_generic_header_title -->
                        <div class="atbd_listing_action_btn btn-toolbar align-items-center" role="toolbar">
                            <!-- Views dropdown -->
                          
                            <!-- Orderby dropdown -->
                            
                        </div><!-- ends: .atbd_listing_action_btn -->
                    </div><!-- ends: .atbd_generic_header -->
                </div><!-- ends: .col-lg-12 -->
                <div class="col-lg-12 listing-items">
                    <div class="row">
                        <div class="col-lg-4 order-1 order-lg-0 mt-5 mt-lg-0">
                            <div class="listings-sidebar">
                                <div class="search-area default-ad-search">
                                    
                                         {!! Form::open(['action' => ['App\Http\Controllers\HomeController@reviews'], 'method' => 'POST']) !!}
                                             <div class="filter-checklist">
                                            <h5>Rate {{ $vendor->company_name }}</h5>
                                           
                                               
                                            
                                      
 <div class="containerr">
      <div class="post">
        <div class="text">
Thanks for rating us!</div>
<div class="edit">
EDIT</div>
</div>
<div class="star-widget">
        <input type="radio" name="ratings" value="5" id="rate-5" required>
        <label for="rate-5" class="la la-star"></label>
        <input type="radio" name="ratings" value="4" id="rate-4" required>
        <label for="rate-4" class="la la-star"></label>
        <input type="radio" name="ratings" value="3" id="rate-3" required>
        <label for="rate-3" class="la la-star"></label>
        <input type="radio" name="ratings" value="2" id="rate-2" required>
        <label for="rate-2" class="la la-star"></label>
        <input type="radio" name="ratings" value="1" id="rate-1" required>
        <label for="rate-1" class="la la-star"></label>
         <input type='hidden' name='product_id' value='{{$vendor->id}}' id='comment_post_ID' />
       
          <header></header>
          <div class="textarea">
            <textare cols="30" placeholder="Describe your experience.."></textare> 
<!-- Due to more textarea tags I got a problem So I've changed the textarea tag to textare. Please correct it. -->
          
          <br />
<div class="btn">
<button type="submit">Rate</button>
          </div>
           {!! Form::close() !!}
<script>
      const btn = document.querySelector("button");
      const post = document.querySelector(".post");
      const widget = document.querySelector(".star-widget");
      const editBtn = document.querySelector(".edit");
      btn.onclick = ()=>{
        widget.style.display = "none";
        post.style.display = "block";
        editBtn.onclick = ()=>{
          widget.style.display = "block";
          post.style.display = "none";
        }
        return false;
      }
    </script>
                                        </div><!-- ends: .filter-checklist -->
                                       </div>
                            </div>
                             </div>
                            </div>
                             </div>
                           
                        </div><!-- ends: .col-lg-4 -->

                        <div class="col-lg-8 order-0 order-lg-1">
                        @foreach($vendors as $vendor)
                            <div class="row">
                           
                                <div class="col-lg-12">
                               
                                    <div class="atbd_single_listing atbd_listing_list">
                                        <article class="atbd_single_listing_wrapper">
                                            <figure class="atbd_listing_thumbnail_area">
                                                <div class="atbd_listing_image">
                                                    <a href="">
                                                         <?php
                                                                  $filename = 'public/storage/upload_image/';


                                                            ?>
                                                         @if( file_exists($filename.'/'.$vendor->image))
                                                      
                                                       
                                                        <img src="{{ asset('public/storage/upload_image/'.$vendor->image) }}" alt="listing image" data-toggle="tooltip" data-placement="right" title="{{ $vendor->description }}">
                                                         @elseif(!$vendor->image)
                              <img src="https://via.placeholder.com/400x300/EBFAFF/001E29/?text={{$vendor->vendor_name}}"  class="card-img-top" alt="listing image" style="height:120px;" data-toggle="tooltip" data-placement="right" title="{{ $vendor->description }}"/> <!-- max-width:100%; -->
                                                  
                              @else
                                     <img src="{{ $vendor->image }}" alt="listing image" style="height:300px" data-toggle="tooltip" data-placement="right" title="{{ $vendor->description }}">
                                                       
                                                         @endif
                                                    </a>
                                                </div><!-- ends: .atbd_listing_image -->
                                                <div class="atbd_thumbnail_overlay_content">
                                                    <ul class="atbd_upper_badge">
                                                        <li><span class="atbd_badge atbd_badge_featured">Featured</span></li>
                                                    </ul><!-- ends .atbd_upper_badge -->
                                                </div><!-- ends: .atbd_thumbnail_overlay_content -->
                                            </figure><!-- ends: .atbd_listing_thumbnail_area -->
                                            <div class="atbd_listing_info">
                                                <div class="atbd_content_upper">
                                                    <h4 class="atbd_listing_title">
                                                        <a href="">{{ $vendor->name }}</a>
                                                    </h4>
                                                    <div class="atbd_listing_meta">
                                                        
                                                        
                                                        <span class="atbd_meta atbd_badge_open">&#8358 {{number_format($vendor->price, 2)}}</span>
                                                    </div><!-- End atbd listing meta -->
                                                    <div class="atbd_listing_data_list">
                                                        <ul>
                                                            <li>
                                                                <p><span class="la la-map-marker"></span>{{ $vendor->location }}</p>
                                                            </li>
                                                           
                                                            <li>
                                                                <p><span class="la la-calendar-check-o"></span>Posted {{ $vendor->created_at->format('M-Y') }}</p>
                                                            </li>
                                                        </ul>
                                                    </div><!-- End atbd listing meta -->
                                                </div><!-- end .atbd_content_upper -->
                                                <div class="atbd_listing_bottom_content">
                                                    <div class="atbd_content_left">
                                                        <div class="atbd_listing_category">
                                                            <a href=""><span class="la la-cutlery"></span>{{ $vendor->category }}</a>
                                                        </div>
                                                    </div>
                                                    <ul class="atbd_content_right">
                                                       
                                                        <li class="atbd_save">
                                                        {!! Form::open(['action' => ['App\Http\Controllers\CartController@addToCart', $vendor->id], 'method' => 'POST']) !!}
                                                          <button class="btn  btn-xs btn-gradient btn-gradient-two">  <span class="la la-shopping-cart">Add to cart</span></button>
                                                          {!! Form::close() !!}
                                                        </li>
                                                        </ul>
                                                </div><!-- end .atbd_listing_bottom_content -->
                                            </div><!-- ends: .atbd_listing_info -->
                                        </article><!-- atbd_single_listing_wrapper -->
                                    </div>
                                  
                                </div><!-- ends: .col-lg-12 -->
                               
                </div><!-- ends: .listing-items -->
                @endforeach
            </div>
            </div>
            <div class="row">
                                <div class="col-lg-8">
                                    <nav class="navigation pagination d-flex justify-content-end" role="navigation">
                                        <div class="nav-links">
                                        {{ $vendors->links() }}
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div><!-- ends: .col-lg-8 -->
                    </div>
                </div><!-- ends: .listing-items -->
                
            
        </div>
        </section>
       
        @endsection