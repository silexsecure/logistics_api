@extends('layout.footer')
@extends('layout.head')
@section('content')
<div class="directory_content_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="search_title_area">
                        <br><br>
                            <h2 class="title">Shoppinng Cart</h2>
                            <p class="sub_title">EatNaija helps you find and buy meals from restaurants and food businesses in and around Nigeria..</p>
                        </div><!-- ends: .search_title_area -->
                        < <form action="/search" method="get" class="search_form">
                            <div class="atbd_seach_fields_wrapper">
                                <div class="single_search_field search_query">
                                    <input class="form-control search_fields" name="item" type="text" placeholder="What are you looking for?">
                                </div>
                               
                                <div class="single_search_field search_location">
                                    <input class="form-control search_fields" name="location" type="text" placeholder="Location">
                                        
                                </div>
                                <div class="atbd_submit_btn">
                                    <button type="submit" class="btn btn-block btn-gradient btn-gradient-one btn-md btn_search">Search</button>
                                </div>
                            </div>
                        </form><!-- ends: .search_form -->
                        
                    </div><!-- ends: .col-lg-10 -->
                </div>
            </div>
        </div><!-- ends: .directory_search_area -->
        </section><!-- ends: .intro-wrapper -->



        <section class="checkout-wrapper section-padding-strict section-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 listing-items">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="listings">
                                <div class="search-area default-ad-search">
                                    
                                        <div class="filter-checklist">
                                            <h5> Update Details</h5>
                                            
                                                        {!! Form::open(['action' => ['App\Http\Controllers\HomeController@update_profile', Auth::user()->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                                               {{ csrf_field() }}  
										
        <div class="d-flex justify-content-center">
          <div class="btn btn-mdb-color btn-rounded float-left" style="background:#333;">
            <span><input type="file" name="image"></span>
          </div>
        </div>
      </div>
      <br><br>
													
													<div class="form-group required">
														<label for="input-payment-address-1" class="control-label">Phone number</label>
														<input type="text" class="form-control" id="input-payment-address-1" placeholder="Phone number" value="{{ Auth::user()->phone }}" name="phone">
													</div>
													<div class="form-group required">
														<label for="input-payment-address-1" class="control-label">Address 1</label>
														<input type="text" class="form-control" id="input-payment-address-1" placeholder="Address 1" value="{{ Auth::user()->address }}" name="address">
													</div>
												
													<div class="form-group required">
														<label for="input-payment-city" class="control-label">City</label>
														<input type="text" class="form-control" id="input-payment-city" placeholder="City" value="{{ Auth::user()->city }}" name="city">
													</div>
													<div class="form-group required">
														<label for="input-payment-postcode" class="control-label">state</label>
														<input type="text" class="form-control" id="input-payment-postcode" placeholder="State" value="{{ Auth::user()->state }}" name="state">
													</div>
													
												
            <button type="submit" class="btn btn-primary ">Update </button>

    {!! Form::close() !!}
                                            
                                        </div><!-- ends: .filter-checklist -->
                                       </div>
                            </div>
                        </div><!-- ends: .col-lg-4 -->
                       </div>
    </section><!-- ends: .checkout-wrapper -->				
@endsection