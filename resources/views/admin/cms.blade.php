
@extends('layout.header')
<div class="main-content">

<div class="page-content">
    
    <!-- Page-Title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title mb-1">Dashboard</h4>
                    <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Welcome to EatNaija Admin Dashboard</li>
                    </ol>
                </div>
               
            </div>

        </div>
    </div>
    <!-- end page title end breadcrumb -->
<div class="page-content-wrapper">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h2 class="header-title mb-4">About Page</h2>
                                            @if(session('success'))
                 <div class="alert alert-success">
                  {{session('success')}}
                 </div>
                @endif
                                          <form>

     

                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div>
                                                            <div class="form-group mb-4">
                                                               
                                                                <textarea class="body form-control" id="body" name="body" rows="4"  >{!!$category->body!!}</textarea>
                                                                
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                   
                                                       
                                                    </div>
                                                    <button type="submit" class="btn btn-primary">Save</button>
                                                </div>
                                               
                                                
                                               </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end row -->
                        </div>
                       
                        </div>
                        <!-- end container-fluid -->
                    </div> 
                    <!-- end page-content-wrapper -->
                </div>
<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
      CKEDITOR.replace( 'body' );
      
      $('form').submit( function (event) {
	// prevent the usual form submission behaviour; the "action" attribute of the form
	event.preventDefault();
	// validation goes below...
const title = $("#body").val()
const body = CKEDITOR.instances['body'].getData();
 
	// now for the big event
	$.ajax({
	  // the server script you want to send your data to
		'url': '/create_cms',
		// all of your POST/GET variables
		'data': {
		   
		    body: body,
		    "_token":"{{csrf_token()}}"
			// 'dataname': $('input').val(), ...
		},
		// you may change this to GET, if you like...
		'type': 'post',
		 
	})
	.done( function (response) {
		// what you want to happen when an ajax call to the server is successfully completed
		// 'response' is what you get back from the script/server
		// usually you want to format your response and spit it out to the page
		swal(response.message, "", "success");
	})
	.fail( function (code, status) {
		// what you want to happen if the ajax request fails (404 error, timeout, etc.)
		// 'code' is the numeric code, and 'status' is the text explanation for the error
		// I usually just output some fancy error messages
			swal("something went wrong!", "", "error");
	})
 
});

</script>