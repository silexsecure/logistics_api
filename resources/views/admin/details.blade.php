<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
@extends('layout.header')
<div class="main-content">

<div class="page-content">
    
    <!-- Page-Title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title mb-1">{{$agent->company_name}} Details</h4>
                    <ol class="breadcrumb m-0">
                          @if($agent->suspend==NULL)
                    <button class="btn btn-info btn-md">This Account is active</button> 
                    @else
                      <button class="btn btn-danger btn-md">This Account is suspended</button> 
                    @endif
                    </ol>
                </div>
                <div class="col-md-4">
                    <div class="float-right d-none d-md-block">
                        <div class="dropdown">
                          
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- end page title end breadcrumb -->

    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4">
                    <div class="card">
                        <div class="card-body">
                        <div class="row">
                                <div class="col-6">
                                       @if(session('success'))
                 <div class="alert alert-success">
                  {{session('success')}}
                 </div>
                @endif
                                    <h5> {{$agent->company_name}}!</h5>
                                    

                                  
                                </div>

                                <div class="col-5 ml-auto">
                                    <div>
                                         <?php
                                                                  $filename = 'public/storage/vendor_image/';


                                                            ?>
                                                         @if( file_exists($filename.'/'.$agent->image))
                                                      
                                                       
                                                        <img class="img-fluid" src="{{ asset('public/storage/vendor_image/'.$agent->image) }}" alt="listing image" style="width:250px" >
                              @else
                                     <img src="{{ $agent->image }}" alt="listing image" class="img-fluid" style="width:250px">
                                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="header-title mb-4">Sales Report</h5>
                            <div class="media">
                                <div class="media-body">
                                    <p class="text-muted mb-2">Total Sales</p>
                                    <h4>&#8358 {{number_format($total, 2)}}</h4>
                                </div>
                                <div dir="ltr" class="ml-2">
                                    <input data-plugin="knob" data-width="56" data-height="56" data-linecap=round data-displayInput=false
                                    data-fgColor="orange" value="100" data-skin="tron" data-angleOffset="100"
                                    data-readOnly=true data-thickness=".17" />
                                </div>
                            </div>
                            <hr>
                           <div class="media">
                                <div class="media-body">
                                    <p class="text-muted mb-2">Last Month Sales</p>
                                    <h4>&#8358 {{number_format($last, 2)}}</h4>
                                </div>
                                <div dir="ltr" class="ml-2">
                                    <input data-plugin="knob" data-width="56" data-height="56" data-linecap=round data-displayInput=false
                                    data-fgColor="orange" value="50" data-skin="tron" data-angleOffset="56"
                                    data-readOnly=true data-thickness=".17" />
                                </div>
                            </div>
                       
<hr>
 <div class="media">
                                <div class="media-body">
                                    <p class="text-muted mb-2">This Month Sales</p>
                                    <h4>&#8358 {{number_format($present, 2)}}</h4>
                                </div>
                                <div dir="ltr" class="ml-2">
                                    <input data-plugin="knob" data-width="56" data-height="56" data-linecap=round data-displayInput=false
                                    data-fgColor="orange" value="25" data-skin="tron" data-angleOffset="56"
                                    data-readOnly=true data-thickness=".17" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header bg-transparent p-3">
                            <h5 class="header-title mb-0">Sales Status</h5>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div class="media my-2">
                                    
                                    <div class="media-body">
                                        <p class="text-muted mb-2">Available Products</p>
                                        <h5 class="mb-0">{{ $products->count()}}</h5>
                                    </div>
                                    <div class="icons-lg ml-2 align-self-center">
                                        <i class="uim uim-layer-group"></i>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media my-2">
                                    <div class="media-body">
                                        <p class="text-muted mb-2">Products Sold </p>
                                        <h5 class="mb-0">{{ $sold->count()}}</h5>
                                    </div>
                                    <div class="icons-lg ml-2 align-self-center">
                                        <i style="color:orange" class="uim uim-analytics"></i>
                                    </div>
                                </div>
                            </li>
                    
                            <li class="list-group-item">
                                <div class="media my-2">
                                    <div class="media-body">
                                        <p class="text-muted mb-2">Products Sold Last Month</p>
                                        <h5 class="mb-0">{{ $lastt->count()}}</h5>
                                    </div>
                                    <div class="icons-lg ml-2 align-self-center">
                                        <i class="uim uim-box"></i>
                                    </div>
                                </div>
                            </li>
                            
                        <li class="list-group-item">
                                <div class="media my-2">
                                    <div class="media-body">
                                        <p class="text-muted mb-2">Current Sales</p>
                                        <h5 class="mb-0">{{ $current->count()}}</h5>
                                    </div>
                                    <div class="icons-lg ml-2 align-self-center">
                                        <i class="uim uim-box"></i>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="header-title mb-4">Rating</h5>
                            
                            <h5 class="mb-0">{{ $agent->rating}} <i style="color:gold" class="fa fa-star" aria-hidden="true"></i></h5>
                           <hr>
                             <h5 class="header-title mb-4">Reports</h5>
                              <h5 class="mb-0">{{ $report->count()}}</h5>
                              <hr>
                                  <h5 class="header-title mb-4">Send Email</h5>
                                  <button data-toggle="modal" data-target-id="atbdp-report-abuse-modal2{{ $agent->id }}" data-target="#atbdp-report-abuse-modal2{{ $agent->id }}" class="btn btn-info btn-sm text-right">Email </button>
                               <hr>
                                <h5 class="header-title mb-4">Create Category For Vendor</h5>
                                  <button data-toggle="modal" data-target-id="atbdp-report-abuse-modal4{{ $agent->id }}" data-target="#atbdp-report-abuse-modal4{{ $agent->id }}" class="btn btn-primary btn-sm text-right">Create </button>
                               <hr>
                              
                                  <h5 class="header-title mb-4">Upload Food For Vendor</h5>
                                  <button data-toggle="modal" data-target-id="atbdp-report-abuse-modal2{{ $agent->id }}" data-target="#atbdp-report-abuse-modal3{{ $agent->id }}" class="btn btn-warning btn-sm text-right">Upload </button>
                               <hr>
                                @if($agent->suspend==NULL)
                                 <h5 class="header-title mb-4">Suspend Account</h5>
                                 {!! Form::open(['action' =>['App\Http\Controllers\AdminController@suspend', $agent->id], 'method'=> 'POST'])!!}
                            {{ csrf_field() }}
                         <button type="submit" class="btn btn-danger btn-sm text-right">Suspend </button>
                         {!!Form::close()!!}
                         @else
                          <h5 class="header-title mb-4">Activate Account</h5>
                          {!! Form::open(['action' =>['App\Http\Controllers\AdminController@activate', $agent->id], 'method'=> 'POST'])!!}
                            {{ csrf_field() }}
                         <button type="submit" class="btn btn-success btn-sm text-right">Activate</button>
                         {!!Form::close()!!}
                         @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- end row -->
              <div class="modal fade" id="atbdp-report-abuse-modal2{{ $agent->id }}" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="atbdp-report-abuse-modal-label">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content" style="width:5000px">
                         {!! Form::open(['action' =>['App\Http\Controllers\AdminController@send_email', $agent->id], 'method'=> 'POST'])!!}
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <h3 class="modal-title" id="atbdp-report-abuse-modal-label">Email {{ $agent->company_name }}</h3>
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                 <label for="atbdp-report-abuse-message" class="not_empty">Subject<span class="atbdp-star">*</span></label>
                                    <input type="text" class="form-control" name="subject"  required><br>
                                    <label for="atbdp-report-abuse-message" class="not_empty">Your Message<span class="atbdp-star">*</span></label>
                                    <textarea class="form-control" id="body2" name="message" rows="4" placeholder="Message..." ></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-secondary btn-sm">Submit</button>
                            </div>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
            
             <div class="modal fade" id="atbdp-report-abuse-modal3{{ $agent->id }}" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="atbdp-report-abuse-modal-label">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content" style="width:5000px">
                         {!! Form::open(['action' =>['App\Http\Controllers\AdminController@upload_food'], 'method'=> 'POST', 'enctype' => 'multipart/form-data'])!!}
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <h3 class="modal-title" id="atbdp-report-abuse-modal-label">Upload Menu</h3>
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="file-field ">
        <div class="d-flex justify-content-center">
          <div class="btn btn-mdb-color btn-rounded float-left" style="background:#333;">
            <span><input type="file" name="image"></span>
          </div>
        </div>
      </div>
                                 <label for="atbdp-report-abuse-message" class="not_empty">Food Name<span class="atbdp-star">*</span></label>
                                 <input type="hidden" class="form-control" name="vendor_id"   value="{{ $agent->id }}" required>
                                 <input type="hidden" class="form-control" name="vendor_name"   value="{{ $agent->company_name }}" required>
                                 <input type="hidden" class="form-control" name="location"   value="{{ $agent->state }}" required>
                                 <input type="hidden" class="form-control" name="main_category"   value="{{ $agent->category }}" required>
                                    <input type="text" class="form-control" name="name"  required><br>
                                    <label for="atbdp-report-abuse-message" class="not_empty">Food Category<span class="atbdp-star">*</span></label>
                                     {!! Form::select('category', $cats, old('category'),['class' =>'form-control']) !!}<br>
                                    <label for="atbdp-report-abuse-message" class="not_empty">Price<span class="atbdp-star">*</span></label>
                                    <input type="number" class="form-control" name="price"  required><br>
                                    <label for="atbdp-report-abuse-message" class="not_empty">Food Description<span class="atbdp-star">*</span></label>
                                    <textarea class="form-control"  name="description" rows="4" ></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-secondary btn-sm">Submit</button>
                            </div>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>

           <div class="modal fade" id="atbdp-report-abuse-modal4{{ $agent->id }}" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="atbdp-report-abuse-modal-label">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content" style="width:5000px">
                         {!! Form::open(['action' =>['App\Http\Controllers\AdminController@create_category'], 'method'=> 'POST', 'enctype' => 'multipart/form-data'])!!}
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <h3 class="modal-title" id="atbdp-report-abuse-modal-label">Create Category</h3>
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                 <label for="atbdp-report-abuse-message" class="not_empty">Category Name<span class="atbdp-star">*</span></label>
                                 <input type="hidden" class="form-control" name="vendor_id"   value="{{ $agent->id }}" required>
                                    <input type="text" class="form-control" name="name"  required><br>
                                
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-secondary btn-sm">Submit</button>
                            </div>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="float-right ml-2">
                               
                            </div>
                            <h5 class="header-title mb-4">Latest Transaction</h5>

                            <div class="table-responsive">
                                <table class="table table-centered table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th scope="col">Product</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Date</th>
                                            <th scope="col">status</th>
                                            <th scope="col">Amount</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                         @foreach($latests as $latest)
                                        <tr>
                                           
                                            <th scope="row">
                                                <a href="#">{{ $latest->product}}</a>
                                            </th>
                                            <td>{{ $latest->name}}</td>
                                            <td>{{ $latest->created_at->format('D-d-M-y') }}</td>
                                            <td>
                                                <div class="badge badge-soft-primary">Confirm</div>
                                            </td>
                                            <td>&#8358 {{number_format($latest->price, 2)}}</td>
                                           
                                           
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="mt-4">
                                <ul class="pagination pagination-rounded justify-content-center mb-0">
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
               
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- end page-content-wrapper -->
</div>
<!-- End Page-content -->

 <script>
ClassicEditor
.create( document.querySelector( '#body' ) )
.catch( error => {
console.error( error );
} );
</script>
            <script>
var allEditors = document.querySelectorAll('#body2');
for (var i = 0; i < allEditors.length; ++i) {
  ClassicEditor.create(allEditors[i]);
}
</script>
