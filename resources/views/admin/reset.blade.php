<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Login Admin Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
        <meta content="Themesdesign" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="public/favicon.ico">

        <!-- Bootstrap Css -->
        <link href="public/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="public/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="public/assets/css/app.min.css" rel="stylesheet" type="text/css" />

    </head>
<style>
    .bg-primary {
    background-color: #fd7e14!important;
}
</style>
    <body class="bg-primary bg-pattern">
        <div class="home-btn d-none d-sm-block">
            <a href="/"><i class="mdi mdi-home-variant h2 text-white"></i></a>
        </div>

        <div class="account-pages my-5 pt-sm-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center mb-5">
                            <a href="/" class="logo"><img src="public/img/logo-white.png" height="44" alt="logo"></a>
                            <h5 class="font-size-16 text-white-50 mb-4">Login to Admin Dashboard</h5>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="row justify-content-center">
                    <div class="col-xl-5 col-sm-8">
                        <div class="card">
                            <div class="card-body p-4">
                                <div class="p-2">
                                    <h5 class="mb-5 text-center">Reset Password</h5>
                                    @if (session('status'))
                                         <div class="mb-4 font-medium text-sm text-red" style="color:red">
                                             {{ session('status') }}
                                         </div>
                                     @endif

                                    {!! Form::open(['action' => ['App\Http\Controllers\HomeController@reset2', $user->id], 'method' => 'POST']) !!}
                                               {{ csrf_field() }} 
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-custom mb-4">
                                                    <input type="password" class="form-control" id="username" name="password" required>
                                                    <label for="username">New Password</label>
                                                </div>

                                                <div class="form-group form-group-custom mb-4">
                                                    <input type="password" class="form-control" id="userpassword"  name="password_confirmation" required>
                                                    <label for="userpassword">Confirm Password</label>
                                                </div>

                                              
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
        </div>
        <!-- end Account pages -->
 
    
        <!-- JAVASCRIPT -->
        <script src="public/assets/libs/jquery/jquery.min.js"></script>
        <script src="public/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="public/assets/libs/metismenu/metisMenu.min.js"></script>
        <script src="public/assets/libs/simplebar/simplebar.min.js"></script>
        <script src="public/assets/libs/node-waves/waves.min.js"></script>

        <script src="https://unicons.iconscout.com/release/v2.0.1/script/monochrome/bundle.js"></script>

        <script src="public/assets/js/app.js"></script>

    </body>
</html>
