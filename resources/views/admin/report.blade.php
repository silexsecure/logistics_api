@extends('layout.header')

<div class="main-content">

<div class="page-content">
    
    <!-- Page-Title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title mb-1">Dashboard</h4>
                    <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Welcome to EatNaija Admin Dashboard</li>
                    </ol>
                </div>
               
            </div>

        </div>
    </div>
    <div class="page-content-wrapper">
                        <div class="container-fluid">
    <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            
                            <h2 class="header-title mb-4">Reports</h2>
                            @if(session('success'))
                 <div class="alert alert-success">
                  {{session('success')}}
                 </div>
                @endif

                            <div class="table-responsive">
                                <table class="table table-centered table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th scope="col">company</th>
                                            
                                            <th scope="col">Complaint</th>
                                           
                                            <th scope="col">Date Sent</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($agents as $agent)
                                        <tr>
                                       
                                           
                                            <td>{{ $agent->company_name }}</td>
                                           
                                            <td>{{ $agent->reports }}</td>
                                           
                                            <td>{{ $agent->created_at->format('D-d-M-y h:i A') }}</td>
                                            <td>
                                                <div class="btn-group" role="group">
                                                   
                                                   
                                                    {!! Form::open(['action' =>['App\Http\Controllers\AdminController@destroy_report', $agent->id], 'method'=> 'POST'])!!}
                                                   
                                                     {{Form::hidden('_method','DELETE')}}
                                                    <button type="submit" class="btn btn-outline-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="mdi mdi-trash-can"></i>
                                                    </button>
                                                    {!!Form::close()!!}
                                                </div>
                                            </td>
                                           
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="mt-4">
                                <ul class="pagination pagination-rounded justify-content-center mb-4">
                                   
                                  {!! $agents->render() !!}
            
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                