@extends('layout.header')


<div class="main-content">

<div class="page-content">
    
    <!-- Page-Title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title mb-1">Dashboard</h4>
                    <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Welcome to EatNaija Admin Dashboard</li>
                    </ol>
                </div>
               
            </div>

        </div>
    </div>
    <!-- end page title end breadcrumb -->
<div class="page-content-wrapper">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h2 class="header-title mb-4">UPDATE PROFILE</h2>
                                            @if(session('success'))
                 <div class="alert alert-success">
                  {{session('success')}}
                 </div>
                @endif
                {!! Form::open(['action' => ['App\Http\Controllers\AdminController@update_admin', $admin->id], 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}

                                            @csrf

<div class="file-field ">
    <div class="d-flex justify-content-center">
                <img src="{{ asset('public/storage/admin_image/'.Auth::guard('admin')->user()->image) }}" class="img-fluid w50" style="width:100px;">
</div>
<br>
        <div class="d-flex justify-content-center">
          <div class="btn btn-mdb-color btn-rounded float-left" style="background:#333;">
            <span><input type="file" name="image"></span>
          </div>
        </div>
      </div>
      <br><br>

                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div>
                                                            <div class="form-group mb-4">
                                                                <label for="input-date1">Company Name</label>
                                                                <input type="text" id="input-date1" class="form-control input-mask" name="name"  value=" {{$admin->name}}" required>
                                                                
                                                            </div>
                                                       
                                                       </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="mt-4 mt-lg-0">
                                                            
                                                           
                                                         <div class="form-group mb-4">
                                                                <label for="input-repeat">Email</label>
                                                                <input type="email" id="input-repeat" class="form-control input-mask" name="email" value=" {{$admin->email}}"  required>
                                                                
                                                            </div>
                                                           
                                                        </div>
                                                       
                                                    </div>
                                                   
                                                </div>
                                                
                                                 <div class="row">
                                                    <div class="col-lg-6">
                                                        <div>
                                                            <div class="form-group mb-4">
                                                                <label for="input-date1">Percentage on Logistics</label>
                                                                <input type="text" id="input-date1" class="form-control input-mask" name="percentageOnLogistics"  value=" {{$admin->percentageOnLogistics}}" required>
                                                                
                                                            </div>
                                                       
                                                       </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="mt-4 mt-lg-0">
                                                            
                                                           
                                                         <div class="form-group mb-4">
                                                                <label for="input-repeat">Percentage on Product</label>
                                                                <input type="text" id="input-repeat" class="form-control input-mask" name="percentageOnProduct" value=" {{$admin->percentageOnProduct}}"  required>
                                                                
                                                            </div>
                                                           
                                                        </div>
                                                       
                                                    </div>
                                                   
                                                </div>
                                                
                                                <br>
                                                <button type="submit" class="btn btn-primary">Update</button>
                                                {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end row -->
                        </div>
                        <!-- end container-fluid -->
                    </div> 
                    <!-- end page-content-wrapper -->
                </div>
