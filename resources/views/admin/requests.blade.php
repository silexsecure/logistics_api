<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
@extends('layout.header')


<div class="main-content">

<div class="page-content">
    
    <!-- Page-Title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title mb-1">Dashboard</h4>
                    <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Welcome to EatNaija Admin Dashboard</li>
                    </ol>
                </div>
               
            </div>

        </div>
    </div>
    <div class="page-content-wrapper">
                        <div class="container-fluid">
    <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            
                            <h2 class="header-title mb-4">VENDORS
                            <button data-toggle="modal" data-target="#atbdp-report-abuse-modal" style="float:right;" class="btn btn-info btn-sm text-right">Email All</button></h2>
                            @if(session('success'))
                 <div class="alert alert-success">
                  {{session('success')}}
                 </div>
                @endif

                            <div class="table-responsive">
                                <table class="table table-centered table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th scope="col">Image</th>
                                            <th scope="col">Company Name</th>
                                             <th scope="col">Rating</th>
                                            <th scope="col">Category</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Phone Number</th>
                                            <th scope="col">Date Joined</th>
                                            <th scope="col">Send Email</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($agents as $agent)
                                        <tr>
                                       
                                            <th scope="row">
                                                <a href="#"><img class="w50"  src=" {{ $agent->image }} "   alt="img" style="width:50px;"></a>
                                            </th>
                                            <td>{{ $agent->company_name }}</td>
                                            @if($agent->rating==NULL)
                                             <td>No Rating yet</td>
                                             @else
                                            <td>{{ $agent->rating }}<i class="fa fa-star" aria-hidden="true"></i></td>
                                            @endif
                                            <td>{{ $agent->category }}</td>
                                            <td>{{ $agent->email }}</td>
                                            <td>
                                            {{ $agent->phone }}
                                            </td>
                                            <td>{{ $agent->created_at->format('D-d-M-y h:i A') }}</td>
                                            <td> <button data-toggle="modal" data-target-id="atbdp-report-abuse-modal2{{ $agent->id }}" data-target="#atbdp-report-abuse-modal2{{ $agent->id }}" class="btn btn-info btn-sm text-right">Email </button></td>
                                            <td>
                                                
                                                <div class="btn-group" role="group">
                                                     {!! Form::open(['action' =>['App\Http\Controllers\AdminController@approve', $agent->id], 'method'=> 'POST'])!!}
                                                    <button type="submit" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Approve">
                                                       Approve
                                                    </button>
                                                   {!!Form::close()!!}
                                                    {!! Form::open(['action' =>['App\Http\Controllers\AdminController@destroy', $agent->id], 'method'=> 'POST'])!!}
                                       
                                                    
                                                     {{Form::hidden('_method','DELETE')}}
                                                    <button type="submit" class="btn btn-outline-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="mdi mdi-trash-can"></i>
                                                    </button>
                                                    {!!Form::close()!!}
                                                </div>
                                            </td>
                                           
                                        </tr>
                                        <div class="modal fade" id="atbdp-report-abuse-modal2{{ $agent->id }}" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="atbdp-report-abuse-modal-label">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content" style="width:5000px">
                         {!! Form::open(['action' =>['App\Http\Controllers\AdminController@send_email', $agent->id], 'method'=> 'POST'])!!}
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <h3 class="modal-title" id="atbdp-report-abuse-modal-label">Email {{ $agent->company_name }}</h3>
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                 <label for="atbdp-report-abuse-message" class="not_empty">Subject<span class="atbdp-star">*</span></label>
                                    <input type="text" class="form-control" name="subject"  required><br>
                                    <label for="atbdp-report-abuse-message" class="not_empty">Your Message<span class="atbdp-star">*</span></label>
                                    <textarea class="form-control" id="body2" name="message" rows="4" placeholder="Message..." ></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-secondary btn-sm">Submit</button>
                            </div>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="mt-4">
                                <ul class="pagination pagination-rounded justify-content-center mb-0">
                                   
                                   {{ $agents->links() }}
            
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div  class="modal fade" id="atbdp-report-abuse-modal" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="atbdp-report-abuse-modal-label">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content" style="width:5000px">
                        {!! Form::open(['action' =>['App\Http\Controllers\AdminController@bulk_email'], 'method'=> 'POST'])!!}
                           {{ csrf_field() }}
                            <div class="modal-header">
                                <h3 class="modal-title" id="atbdp-report-abuse-modal-label">Send Bulk Email</h3>
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                 <label for="atbdp-report-abuse-message" class="not_empty">Subject<span class="atbdp-star">*</span></label>
                                    <input type="text" class="form-control" name="subject"  required><br>
                                    <label for="atbdp-report-abuse-message" class="not_empty">Your Message<span class="atbdp-star">*</span></label>
                                    <textarea class="form-control" id="body" name="message" rows="4" placeholder="Message..." ></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-secondary btn-sm">Submit</button>
                            </div>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
            
            <script>
ClassicEditor
.create( document.querySelector( '#body' ) )
.catch( error => {
console.error( error );
} );
</script>
            <script>
var allEditors = document.querySelectorAll('#body2');
for (var i = 0; i < allEditors.length; ++i) {
  ClassicEditor.create(allEditors[i]);
}
</script>
