@extends('layout.header')
<div class="main-content">

<div class="page-content">
    
    <!-- Page-Title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title mb-1">Dashboard</h4>
                    <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Welcome to EatNaija Admin Dashboard</li>
                    </ol>
                </div>
               
            </div>

        </div>
    </div>
    <!-- end page title end breadcrumb -->
<div class="page-content-wrapper">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h2 class="header-title mb-4">ADD Chatbot response based on category</h2>
                                            @if(session('success'))
                 <div class="alert alert-success">
                  {{session('success')}}
                 </div>
                @endif
                                            {!! Form::open(['action' => 'App\Http\Controllers\AdminController@createchatbot', 'method' => 'POST']) !!}

                                            @csrf


     

                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div>
                                                            <div class="form-group mb-4">
                                                                 <label for="input-currency">Category</label>
                                                                {!! Form::select('category', $cats, old('name'),['class' =>'form-control']) !!}
                                                               <br>
                                                                <label for="input-date1">Response</label>
                                                                <textarea type="text" id="input-date1" class="form-control input-mask" name="name"></textarea>
                                                                
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                   
                                                       
                                                    </div>
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                               
                                                
                                                {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end row -->
                        </div>
                        <div class="container-fluid">
    <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            
                            <h2 class="header-title mb-4">Response</h2>

                            <div class="table-responsive">
                                <table class="table table-centered table-hover mb-0">
                                    <thead>
                                        <tr>
                                            
                                            <th scope="col">Category </th>
                                             <th scope="col">Response</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $category)
                                        <tr>
                                       
                                            
                                        <th scope="row">{{ $category->category }}</th>
                                         <th scope="row">{{ $category->name }}</th>
                                            
                                            <td>
                                                <div class="btn-group" role="group">
                                                    
                                                    {!! Form::open(['action' =>['App\Http\Controllers\AdminController@destroychatbot', $category->id], 'method'=> 'POST'])!!}
                                                   
                                                     {{Form::hidden('_method','DELETE')}}
                                                    <button type="submit" class="btn btn-outline-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="mdi mdi-trash-can"></i>
                                                    </button>
                                                    {!!Form::close()!!}
                                                </div>
                                            </td>
                                         
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="mt-4">
                                <ul class="pagination pagination-rounded justify-content-center mb-0">
                                   
                                </ul>
                            </div>
                        </div>
                        <!-- end container-fluid -->
                    </div> 
                    <!-- end page-content-wrapper -->
                </div>
