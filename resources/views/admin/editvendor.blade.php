@extends('layout.header')


<div class="main-content">

<div class="page-content">
    
    <!-- Page-Title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title mb-1">Dashboard</h4>
                    <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Welcome to EatNaija Admin Dashboard</li>
                    </ol>
                </div>
               
            </div>

        </div>
    </div>
    <!-- end page title end breadcrumb -->
<div class="page-content-wrapper">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h2 class="header-title mb-4">UPDATE VENDOR</h2>
                                            @if(session('success'))
                 <div class="alert alert-success">
                  {{session('success')}}
                 </div>
                @endif
                {!! Form::open(['action' => ['App\Http\Controllers\AdminController@update', $agent->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) !!}

                                            @csrf

<div class="file-field ">
        <div class="d-flex justify-content-center">
          <div class="btn btn-mdb-color btn-rounded float-left" style="background:#333;">
            <span><input type="file" name="image" value="{{$agent->image}}"></span>
          </div>
        </div>
      </div>
      <br><br>

                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div>
                                                            <div class="form-group mb-4">
                                                                <label for="input-date1">Company Name</label>
                                                                <input type="text" id="input-date1" class="form-control input-mask" name="company_name" value="{{$agent->company_name}}" required>
                                                                
                                                            </div>
                                                            <div class="form-group mb-4">
                                                                <label for="input-date2">Phone Number</label>
                                                                <input type="text" id="input-date2" class="form-control input-mask" name="phone" value="{{$agent->phone}}" required>
                                                                
                                                            </div>
                                                            <div class="form-group mb-4">
                                                                <label for="input-datetime">City</label>
                                                                <input type="text" id="input-datetime" class="form-control input-mask" name="city" value="{{$agent->city}}" required>
                                                                
                                                            </div>
                                                            <div class="form-group mb-0">
                                                                <label for="input-currency">Category</label>
                                                                {!! Form::select('category', $cats, old('name'),['class' =>'form-control']) !!}
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="mt-4 mt-lg-0">
                                                            <div class="form-group mb-4">
                                                                <label for="input-repeat">Email</label>
                                                                <input type="email" id="input-repeat" class="form-control input-mask" name="email" value="{{$agent->email}}" required>
                                                                
                                                            </div>
                                                            <div class="form-group mb-4">
                                                                <label for="input-mask">Adddress</label>
                                                                <input type="text" id="input-mask" class="form-control input-mask" name="address" value="{{$agent->address}}" required>
                                                                
                                                            </div>
                                                            <div class="form-group mb-4">
                                                                <label for="input-ip">State</label>
                                                                <input type="text" id="input-ip" class="form-control input-mask" name="state" value="{{$agent->state}}" required>
                                                                
        
                                                            </div>
                                                           
                                                           
                                                        </div>
                                                       
                                                    </div>
                                                   
                                                </div>
                                                <br>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end row -->
                        </div>
                        <!-- end container-fluid -->
                    </div> 
                    <!-- end page-content-wrapper -->
                </div>
