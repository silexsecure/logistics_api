<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
<!-- MDBootstrap Datatables  -->
<link rel="stylesheet" type="text/css" href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
@extends('layout.header')

<div class="main-content">

<div class="page-content">
    
    <!-- Page-Title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title mb-1">Dashboard</h4>
                    <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Welcome to EatNaija Admin Dashboard</li>
                    </ol>
                </div>
               
            </div>

        </div>
    </div>
    <div class="page-content-wrapper">
                        <div class="container-fluid">
    <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            
                            <h2 class="header-title mb-4">Forms submitted
                            </h2>
                            @if(session('success'))
                 <div class="alert alert-success">
                  {{session('success')}}
                 </div>
                @endif

                            <div class="table-responsive">
                                <table class="table table-centered table-hover mb-0" id="dtBasicExample">
                                    <thead>
                                        <tr>
                                            <th scope="col">Name</th>
                                            
                                            <th scope="col">Email</th>
                                            <th scope="col">Subject</th>
                                             <th scope="col">Message</th>
                                            <th scope="col">Date Sent</th>
                                             <th scope="col">Reply</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($agents as $agent)
                                        <tr>
                                       
                                           
                                            <td>{{ $agent->name }}</td>
                                           
                                            <td>{{ $agent->email }}</td>
                                            <td>
                                            {{ $agent->subject }}
                                            </td>
                                            <td>
                                            {{ $agent->message }}
                                            </td>
                                            <td>{{ $agent->created_at->format('D-d-M-y h:i A') }}</td>
                                   <td> <button data-toggle="modal" data-target-id="atbdp-report-abuse-modal2{{ $agent->id }}" data-target="#atbdp-report-abuse-modal2{{ $agent->id }}" class="btn btn-info btn-sm text-right">Email </button></td>
                                            <td>
                                                <div class="btn-group" role="group">
                                                   
                                                   
                                                    {!! Form::open(['action' =>['App\Http\Controllers\AdminController@destroy_contact', $agent->id], 'method'=> 'POST'])!!}
                                                   
                                                     {{Form::hidden('_method','DELETE')}}
                                                    <button type="submit" class="btn btn-outline-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="mdi mdi-trash-can"></i>
                                                    </button>
                                                    {!!Form::close()!!}
                                                </div>
                                            </td>
                                           
                                        </tr>
                                         <div class="modal fade" id="atbdp-report-abuse-modal2{{ $agent->id }}" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="atbdp-report-abuse-modal-label">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content" style="width:5000px">
                         {!! Form::open(['action' =>['App\Http\Controllers\AdminController@send_email3', $agent->id], 'method'=> 'POST'])!!}
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <h3 class="modal-title" id="atbdp-report-abuse-modal-label">Email {{ $agent->name }}</h3>
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="hidden" class="form-control" name="name" value="{{ $agent->name }}"  >
                                    <input type="hidden" class="form-control" name="email" value="{{ $agent->email }}"  >
                                 <label for="atbdp-report-abuse-message" class="not_empty">Subject<span class="atbdp-star">*</span></label>
                                    <input type="text" class="form-control" name="subject"  required><br>
                                    <label for="atbdp-report-abuse-message" class="not_empty">Your Message<span class="atbdp-star">*</span></label>
                                    <textarea class="form-control" id="body2" name="message" rows="4" placeholder="Message..." ></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-secondary btn-sm">Submit</button>
                            </div>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="mt-4">
                                <ul class="pagination pagination-rounded justify-content-center mb-4">
                                   
                                  {!! $agents->render() !!}
            
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
              
            <script>
ClassicEditor
.create( document.querySelector( '#body' ) )
.catch( error => {
console.error( error );
} );
</script>
            <script>
var allEditors = document.querySelectorAll('#body2');
for (var i = 0; i < allEditors.length; ++i) {
  ClassicEditor.create(allEditors[i]);
}
</script>
<!-- MDBootstrap Datatables  -->
<script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
  <script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
  
<script>
$.noConflict();
  $(function(){
  $('#dtBasicExample').DataTable();
  })
</script>