@extends('layout.header')


<div class="main-content">

<div class="page-content">
    
    <!-- Page-Title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title mb-1">Dashboard</h4>
                    <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Welcome to EatNaija Admin Dashboard</li>
                    </ol>
                </div>
               
            </div>

        </div>
    </div>
    <div class="page-content-wrapper">
                        <div class="container-fluid">
    <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            
                            <h2 class="header-title mb-4">ORDERS</h2>
                            @if(session('success'))
                 <div class="alert alert-success">
                  {{session('success')}}
                 </div>
                @endif

                            <div class="table-responsive">
                                <table id="datatable-buttons" class="table table-centered table-hover mb-0 dt-responsive">
                                    <thead>
                                        <tr>
                                            <th scope="col">Product</th>
                                            <th scope="col">Price</th>
                                            <th scope="col">Quantity</th>
                                            <th scope="col">Customer Name</th>
                                            <th scope="col">Phone Number</th>
                                            <th scope="col">Ordered On</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($agents as $agent)
                                        <tr>
                                        <td>{{ $agent->product }}</td>
                                           <td>&#8358 {{number_format($agent->price, 2)}}</td>
                                            <td>{{ $agent->quantity }}</td>
                                            <td>{{ $agent->name }}</td>
                                            <td>{{ $agent->phone }}</td>
                                            <td>{{ $agent->created_at->format('D-d-M-y h:i A') }}</td>
                                            <td>
                                                <div class="btn-group" role="group">
                                                   
                                                   
                                                    {!! Form::open(['action' =>['App\Http\Controllers\AdminController@destroy_transaction', $agent->id], 'method'=> 'POST'])!!}
                                            
                                                     {{Form::hidden('_method','DELETE')}}
                                                    <button type="submit" class="btn btn-outline-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="mdi mdi-trash-can"></i>
                                                    </button>
                                                    {!!Form::close()!!}
                                                </div>
                                            </td>
                                           
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="mt-4">
                                <ul class="pagination pagination-rounded justify-content-center mb-0">
                                   
                                   {{ $agents->links() }}
            
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                 <!-- Required datatable js -->

        <script src="/public/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="/public/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="/public/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="/public/assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
        <script src="/public/assets/libs/jszip/jszip.min.js"></script>
        <script src="/public/assets/libs/pdfmake/build/pdfmake.min.js"></script>
        <script src="/public/assets/libs/pdfmake/build/vfs_fonts.js"></script>
        <script src="/public/assets/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="/public/assets/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="/public/assets/libs/datatables.net-buttons/js/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="/public/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="/public/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

        <!-- Datatable init js -->
        <script src="/public/assets/js/pages/datatables.init.js"></script>

        <script src="/public/assets/js/app.js"></script>
 