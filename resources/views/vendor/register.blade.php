<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Register for EatNaija Vendor Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <!-- App favicon -->
        <link rel="shortcut icon" href="public/favicon.ico">

        <!-- Bootstrap Css -->
        <link href="public/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="public/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="public/assets/css/app.min.css" rel="stylesheet" type="text/css" />

    </head>
<style>
     .bg-primary {
    background-color: #fd7e14!important;
}
</style>
    <body class="bg-primary bg-pattern">
        <div class="home-btn d-none d-sm-block">
            <a href="/"><i class="mdi mdi-home-variant h2 text-white"></i></a>
        </div>

        <div class="account-pages my-5 pt-sm-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center mb-5">
                            <a href="/" class="logo"><img src="public/img/logo-white.png" height="44" alt="logo"></a>
                            <h5 class="font-size-16 text-white-50 mb-4"> Vendor Dashboard</h5>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="row justify-content-center">
                    <div class="col-xl-5 col-sm-8">
                        <div class="card">
                            <div class="card-body p-4">
                                <div class="p-2">
                                    <h5 class="mb-5 text-center">Register Account For EatNaija Vendor.</h5>
                {!! Form::open(['action' => 'App\Http\Controllers\AdminController@create_vendor', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                @csrf

<div class="file-field ">
        <div class="d-flex justify-content-center">
          <div class="btn btn-mdb-color btn-rounded float-left" style="background:#333;">
            <span><input type="file" name="image"></span>
          </div>
        </div>
      </div>
      <br><br>

                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div>
                                                            <div class="form-group mb-4">
                                                                <label for="input-date1">Company Name</label>
                                                                <input type="text" id="input-date1" class="form-control input-mask" name="company_name" required>
                                                                
                                                            </div>
                                                            <div class="form-group mb-4">
                                                                <label for="input-date2">Phone Number</label>
                                                                <input type="text" id="input-date2" class="form-control input-mask" name="phone" required>
                                                                
                                                            </div>
                                                            <div class="form-group mb-4">
                                                                <label for="input-datetime">City</label>
                                                                <input type="text" id="input-datetime" class="form-control input-mask" name="city" required>
                                                                
                                                            </div>
                                                            <div class="form-group mb-0">
                                                                <label for="input-currency">Category</label>
                                                                {!! Form::select('category', $cats, old('name'),['class' =>'form-control']) !!}
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="mt-4 mt-lg-0">
                                                            <div class="form-group mb-4">
                                                                <label for="input-repeat">Email</label>
                                                                <input type="email" id="input-repeat" class="form-control input-mask" name="email" required>
                                                                
                                                            </div>
                                                            <div class="form-group mb-4">
                                                                <label for="input-mask">Adddress</label>
                                                                <input type="text" id="input-mask" class="form-control input-mask" name="address" required>
                                                                
                                                            </div>
                                                            <div class="form-group mb-4">
                                                                <label for="input-ip">State</label>
                                                                <input type="text" id="input-ip" class="form-control input-mask" name="state" required>
                                                                
        
                                                            </div>
                                                            <div class="form-group mb-4">
                                                                <label for="input-ip">Password</label>
                                                                <input type="text" id="input-ip" class="form-control input-mask" name="password" required>
                                                                
        
                                                            </div>
                                                           
                                                        </div>
                                                       
                                                    </div>
                                                   
                                                </div>
                                                <br>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
        </div>
        <!-- end Account pages -->

        <!-- JAVASCRIPT -->
        <script src="public/assets/libs/jquery/jquery.min.js"></script>
        <script src="public/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="public/assets/libs/metismenu/metisMenu.min.js"></script>
        <script src="public/assets/libs/simplebar/simplebar.min.js"></script>
        <script src="public/assets/libs/node-waves/waves.min.js"></script>

        <script src="https://unicons.iconscout.com/release/v2.0.1/script/monochrome/bundle.js"></script>


        <script src="public/assets/js/app.js"></script>

    </body>
</html>
