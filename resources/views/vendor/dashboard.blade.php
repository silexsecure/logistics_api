@extends('layout.headerV')
<div class="main-content">

<div class="page-content">
    
    <!-- Page-Title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title mb-1">Dashboard</h4>
                    <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Welcome to EatNaija Vendor Dashboard</li>
                    </ol>
                </div>
                <div class="col-md-4">
                    <div class="float-right d-none d-md-block">
                        <div class="dropdown">
                           
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- end page title end breadcrumb -->

    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4">
                    <div class="card">
                        <div class="card-body">
                        <div class="row">
                                <div class="col-6">
                                    <h5>Welcome Back {{ Auth::guard('vendor')->user()->company_name }}!</h5>
                                    <p class="text-muted">Vendor Dashboard</p>

                                  
                                </div>

                                <div class="col-5 ml-auto">
                                    <div>
                                       <?php
                                                                  $filename = 'public/storage/vendor_image/';


                                                            ?>
                                                         @if( file_exists($filename.'/'.Auth::guard('vendor')->user()->image))
                                                      
                                                       
                                                        <img class="img-fluid" src="{{ asset('public/storage/vendor_image/'.Auth::guard('vendor')->user()->image) }}" alt="listing image" style="width:250px" >
                              @else
                                     <img src="{{ Auth::guard('vendor')->user()->image }}" alt="listing image" class="img-fluid" style="width:250px">
                                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="header-title mb-4">Sales Report</h5>
                            <div class="media">
                                <div class="media-body">
                                    <p class="text-muted mb-2">Total Sales</p>
                                    <h4>&#8358 {{number_format($total, 2)}}</h4>
                                </div>
                                <div dir="ltr" class="ml-2">
                                    <input data-plugin="knob" data-width="56" data-height="56" data-linecap=round data-displayInput=false
                                    data-fgColor="#2fa97c" value="56" data-skin="tron" data-angleOffset="56"
                                    data-readOnly=true data-thickness=".17" />
                                </div>
                            </div>
                            <hr>
                            <div class="media">
                            <div class="media-body">
                                    <p class="text-muted mb-2">Last Month Sales</p>
                                    <h4>&#8358 {{number_format($last, 2)}}</h4>
                                </div>
                                <div dir="ltr" class="ml-2">
                                    <input data-plugin="knob" data-width="56" data-height="56" data-linecap=round data-displayInput=false
                                    data-fgColor="#2fa97c" value="56" data-skin="tron" data-angleOffset="56"
                                    data-readOnly=true data-thickness=".17" />
                                </div>
                            </div>
                            <hr>
                            <div class="media">
                            <div class="media-body">
                                    <p class="text-muted mb-2">This Month Sales</p>
                                    <h4>&#8358 {{number_format($present, 2)}}</h4>
                                </div>
                                <div dir="ltr" class="ml-2">
                                    <input data-plugin="knob" data-width="56" data-height="56" data-linecap=round data-displayInput=false
                                    data-fgColor="#2fa97c" value="56" data-skin="tron" data-angleOffset="56"
                                    data-readOnly=true data-thickness=".17" />
                                </div>
                            </div>
                           
                               
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header bg-transparent p-3">
                            <h5 class="header-title mb-0">Sales Status</h5>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div class="media my-2">
                                    
                                    <div class="media-body">
                                        <p class="text-muted mb-2">Available Products</p>
                                        <h5 class="mb-0">{{ $products->count()}}</h5>
                                    </div>
                                    <div class="icons-lg ml-2 align-self-center">
                                        <i class="uim uim-layer-group"></i>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media my-2">
                                    <div class="media-body">
                                        <p class="text-muted mb-2">Products Sold </p>
                                        <h5 class="mb-0">{{ $sold->count()}}</h5>
                                    </div>
                                    <div class="icons-lg ml-2 align-self-center">
                                        <i class="uim uim-analytics"></i>
                                    </div>
                                </div>
                            </li>
                           
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="header-title mb-4">Best Selling Item</h5>
                            @if($best_f)
                            <h5 class="mb-0">{{ $best_f}}</h5>
                           @else
                            <h5 class="mb-0">{{ $best_f->name}}</h5>
                            @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- end row -->

          

          <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="float-right ml-2">
                                <a href="/orders">View all</a>
                            </div>
                            <h5 class="header-title mb-4">Latest Transaction</h5>

                            <div class="table-responsive">
                                <table class="table table-centered table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th scope="col">Product</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Date</th>
                                            <th scope="col">status</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         @foreach($latests as $latest)
                                        <tr>
                                           
                                            <th scope="row">
                                                <a href="#">{{ $latest->product}}</a>
                                            </th>
                                            <td>{{ $latest->name}}</td>
                                            <td>{{ $latest->created_at->format('D-d-M-y') }}</td>
                                            <td>
                                                <div class="badge badge-soft-primary">Confirm</div>
                                            </td>
                                            <td>&#8358 {{number_format($latest->price, 2)}}</td>
                                            <td>
                                                <div class="btn-group" role="group">
                                                     {!! Form::open(['action' =>['App\Http\Controllers\AdminController@destroy_transaction', $latest->id], 'method'=> 'POST'])!!}
                                                   
                                                     {{Form::hidden('_method','DELETE')}}
                                                    <button type="submit" class="btn btn-outline-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="mdi mdi-trash-can"></i>
                                                    </button>
                                                     {!!Form::close()!!}
                                                </div>
                                            </td>
                                           
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="mt-4">
                                <ul class="pagination pagination-rounded justify-content-center mb-0">
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
               
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- end page-content-wrapper -->
</div>
<!-- End Page-content -->

