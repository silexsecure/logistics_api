@extends('layout.headerV')


<div class="main-content">

<div class="page-content">
    
    <!-- Page-Title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title mb-1">Dashboard</h4>
                    <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Welcome to EatNaija Vendor Dashboard</li>
                    </ol>
                </div>
               
            </div>

        </div>
    </div>
    <div class="page-content-wrapper">
                        <div class="container-fluid">
    <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            
                            <h2 class="header-title mb-4">ORDERS</h2>
                            @if(session('success'))
                 <div class="alert alert-success">
                  {{session('success')}}
                 </div>
                @endif

                            <div class="table-responsive">
                                <table class="table table-centered table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th scope="col">Product</th>
                                             <th scope="col">Order ID</th>
                                            <th scope="col">Price</th>
                                            <th scope="col">Quantity</th>
                                            <th scope="col">Customer Name</th>
                                            <th scope="col">Phone Number</th>
                                            <th scope="col">Ordered On</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($agents as $agent)
                                    
                                        <tr>
                                        <td>{{ $agent->product }}</td>
                                        <td>{{ $agent->order_no }}</td>
                                           <td>&#8358 {{number_format($agent->price, 2)}}</td>
                                            <td>{{ $agent->quantity }}</td>
                                            <td>{{ $agent->name }}</td>
                                            <td>{{ $agent->phone }}</td>
                                            <td>{{ $agent->created_at->format('D-d-M-y h:i A') }}</td>
                                            <td>
                                                <div class="btn-group" role="group">
                                                   
                                                    {!! Form::open(['action' =>['App\Http\Controllers\VendorController@accept', $agent->id], 'method'=> 'POST'])!!}
                                            
                                                    
                                                    <button type="submit" class="btn btn-outline-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="Accept">
                                                        Accept
                                                    </button>
                                                    {!!Form::close()!!}
                                                    
                                                     {!! Form::open(['action' =>['App\Http\Controllers\VendorController@reject', $agent->id], 'method'=> 'POST'])!!}
                                            
                                                    
                                                    <button type="submit" class="btn btn-outline-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="Reject">
                                                       Reject
                                                    </button>
                                                    {!!Form::close()!!}
                                                   
                                                    {!! Form::open(['action' =>['App\Http\Controllers\AdminController@destroy_transaction', $agent->id], 'method'=> 'POST'])!!}
                                            
                                                     {{Form::hidden('_method','DELETE')}}
                                                    <button type="submit" class="btn btn-outline-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="mdi mdi-trash-can"></i>
                                                    </button>
                                                    {!!Form::close()!!}
                                                </div>
                                            </td>
                                           
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="mt-4">
                                <ul class="pagination pagination-rounded justify-content-center mb-0">
                                   
                                   {{ $agents->links() }}
            
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                