@extends('layout.headerV')
<div class="main-content">

<div class="page-content">
    
    <!-- Page-Title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title mb-1">Vendor Profile</h4>
                    <ol class="breadcrumb m-0">
                  
                    </ol>
                </div>
                <div class="col-md-4">
                    <div class="float-right d-none d-md-block">
                        <div class="dropdown">
                           
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- end page title end breadcrumb -->

    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            {!! Form::open(['action' => 'App\Http\Controllers\VendorController@updateVendorProfile', 'method' => 'PUT']) !!}

                                            @csrf
                        <div class="row">
                             
                                <div class="col-lg-6">
                                     <div class="form-group mb-4">
                                        <label for="input-date1">Company Name</label>
                                        <input type="text" id="input-date1" class="form-control input-mask" name="company_name" value="{{$admin->company_name}}" required>
                                    </div>
                                 </div>
                                  <div class="col-lg-6">
                                     <div class="form-group mb-4">
                                        <label for="input-date1">Company Category</label>
                                        <input type="text" id="input-date1" class="form-control input-mask" name="company_category" value="{{$admin->category}}" disabled>
                                    </div>
                                 </div>
                            </div>
                            
                            <div class="row">
                                
                                <div class="col-lg-6">
                                     <div class="form-group mb-4">
                                        <label for="input-date1">Email</label>
                                        <input type="text" id="email" class="form-control input-mask" name="email" value="{{$admin->email}}" required>
                                    </div>
                                 </div>
                                  <div class="col-lg-6">
                                     <div class="form-group mb-4">
                                        <label for="input-date1">Phone Number</label>
                                        <input type="number" id="company_name" class="form-control input-mask" name="phone_number" value="{{$admin->phone}}" required>
                                    </div>
                                 </div>
                                
                            </div>
                            
                            
                             <div class="row">
                                
                                <div class="col-lg-3">
                                     <div class="form-group mb-4">
                                        <label for="input-date1">Account Number</label>
                                        <input type="text" id="account_number" class="form-control input-mask" name="account_number" value="{{$admin->account}}" required>
                                    </div>
                                 </div>
                                 
                                 <div class="col-lg-3">
                                     <div class="form-group mb-8">
                                        <label for="input-date1">Bank Name</label>
                                        <input type="text" id="bank_name" class="form-control input-mask" name="bank_name" value="{{$admin->bank_name}}" required>
                                    </div>
                                 </div>
                                  <div class="col-lg-2">
                                     <div class="form-group mb-4">
                                        <label for="input-date1">Bank Code</label>
                                        <input type="text" id="bank_code" class="form-control input-mask" name="bank_code" value="{{$admin->bank_code}}" required>
                                    </div>
                                 </div>
                            </div>
                            
                             <div class="row">
                                
                                <div class="col-lg-6">
                                     <div class="form-group mb-4">
                                        <label for="input-date1">Address</label>
                                        <textarea class="form-control input-mask" name="vendor_address" required>
                                            {{$admin->address}}
                                        </textarea>
                                    </div>
                                 </div>
                                  <div class="col-lg-3">
                                     <div class="form-group mb-4">
                                        <label for="input-date1">City</label>
                                        <input type="text" id="vendor_city-date1" class="form-control input-mask" name="vendor_city" value="{{$admin->city}}" required>
                                    </div>
                                 </div>
                                 <div class="col-lg-3">
                                     <div class="form-group mb-4">
                                        <label for="input-date1">State</label>
                                        <input type="text" id="vendor_state-date1" class="form-control input-mask" name="vendor_state" value="{{$admin->state}}" required>
                                    </div>
                                 </div>
                                 
                            </div>
                            <div class="row">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                             {!! Form::close() !!}
                        </div>
                    </div>
                
                </div>

                

               
                    </div>
                </div>

            </div>
            <!-- end row -->

    </div>
    <!-- end page-content-wrapper -->
</div>
<!-- End Page-content -->

