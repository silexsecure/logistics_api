
@extends('layout.footer')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EatNaija</title>
    <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,600,700" rel="stylesheet">
    <!-- inject:css-->
    <link rel="stylesheet" href="public/vendor_assets/css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="public/vendor_assets/css/brands.css">
    <link rel="stylesheet" href="public/vendor_assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="public/vendor_assets/css/jquery-ui.css">
    <link rel="stylesheet" href="public/vendor_assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="public/vendor_assets/css/line-awesome.min.css">
    <link rel="stylesheet" href="public/vendor_assets/css/magnific-popup.css">
    <link rel="stylesheet" href="public/vendor_assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="public/vendor_assets/css/select2.min.css">
    <link rel="stylesheet" href="public/vendor_assets/css/slick.css">
    <link rel="stylesheet" href="public/style.css">
    <!-- endinject -->
    <link rel="icon" type="image/png" sizes="32x32" href="public/img/fevicon.png">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/hilios/jQuery.countdown/2.1.0/dist/jquery.countdown.min.js"></script> 
</head>

<body>
<section class="intro-wrapper bgimage overlay overlay--dark"  style="height:100px">
        <div class="bg_image_holder"  style="height:100px"><img src="public/img/intro.jpg" alt=""></div>
        <div class="mainmenu-wrapper">
            <div class="menu-area menu1 menu--light">
                <div class="top-menu-area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="menu-fullwidth">
                                    <div class="logo-wrapper order-lg-0 order-sm-1">
                                        <div class="logo logo-top">
                                            <a href="/"><img src="public/img/logo-white.png" alt="logo image" class="img-fluid"></a>
                                        </div>
                                    </div><!-- ends: .logo-wrapper -->
                                    @if (Route::has('register'))
                                    <div class="menu-container order-lg-1 order-sm-0">
                                        <div class="d_menu">
                                            <nav class="navbar navbar-expand-lg mainmenu__menu">
                                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#direo-navbar-collapse" aria-controls="direo-navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                                                    <span class="navbar-toggler-icon icon-menu"><i class="la la-reorder"></i></span>
                                                </button>
                                                <!-- Collect the nav links, forms, and other content for toggling -->
                                                <div class="collapse navbar-collapse" id="direo-navbar-collapse">
                                                    <ul class="navbar-nav">
                                                        <li>
                                                            <a href="/">Home</a>
                                                        </li>
                                                        <li class="dropdown has_dropdown">
                                                           <a href="/about">About Us</a> 
                                                           
                                                        </li>
                                                         <li class="dropdown has_dropdown">
                                                           <a href="/contactus">Contact Us</a> 
                                                           
                                                        </li>
                                                        <li class="dropdown has_dropdown">
                                                            <a href="#" class="dropdown-toggle" id="drop4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
                                                            <ul class="dropdown-menu" aria-labelledby="drop4">
                                                            <li><a href="/restaurant">Restaurant</a></li>
                                                                <li><a href="/food_ports">Food Port</a></li>
                                                                <li><a href="/cafe">Cafe/Eatery</a></li>
                                                                <li><a href="/Healthy&Wellness">Healthy & Wellness Products</a></li>
                                                                <li><a href="/foodcompany">Food Company</a></li>
                                                                <li><a href="/foodequipment">Food Equipment</a></li>
                                                            </ul>
                                                        </li>
                                                      
                                                </div>
                                                <!-- /.navbar-collapse -->
                                            </nav>
                                        </div>
                                    </div>
                                    <div class="menu-right order-lg-2 order-sm-2">
                                       
                                        <!-- start .author-area -->
                                        <div class="author-area">
                                            <div class="author__access_area">
                                                <ul class="d-flex list-unstyled align-items-center">
                                                    <li>
                                                        <a href="add-listing.html" class="btn btn-xs btn-gradient btn-gradient-two" data-toggle="modal" data-target="#login_modal">
                                                          
                                                            <span class="la la-plus"></span> Sign in
                                                           
                                                        </a>
                                                    </li>
                                                    <li>
                                                        
                                                        <a href="" class="access-link" data-toggle="modal" data-target="#signup_modal">Register</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="offcanvas-menu d-none">
                                        <ul class="d-flex list-unstyled align-items-center">
                                                    <li>
                                                        <a href="add-listing.html" class="btn btn-xs btn-gradient btn-gradient-two" data-toggle="modal" data-target="#login_modal">
                                                            <span class="la la-plus"></span> Sign in
                                                        </a>
                                                    </li>
                                        </div><!-- ends: .offcanvas-menu -->
                                    </div><!-- ends: .menu-right -->
                                </div>
                            </div>
                        </div>
                        <!-- end /.row -->
                    </div>
                    <!-- end /.container -->
                </div>
                <!-- end  -->
            </div>
        </div><!-- ends: .mainmenu-wrapper -->
                                        @endif
					



</section>
        <section class="checkout-wrapper section-padding-strict section-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 listing-items">
                    <div class="row">
                         <div class="col-lg-9 justify-content-center">
                            <div class="listings-sidebar">
                                <div class="search-area default-ad-search">
                             <h5>Validate Your Account</h5>
                             <br>
                             
                                                        {!! Form::open(['action' => 'App\Http\Controllers\HomeController@loginWithOtp', 'method' => 'POST']) !!}
                                               {{ csrf_field() }} 
                        	<div class="form-row">
    <div class="form-group col-md-12">
     
      <input type="text" class="form-control" id="inputEmail4" placeholder="Enter OTP" name="otp"  required>
    </div>
    
  </div>
  
 <button type="submit" class="btn btn-primary ">Validate</button>
    </div>
     {!! Form::close() !!}
 </div><!-- ends: .col-lg-12 -->
            </div>
        </div>
    </section><!-- ends: .checkout-wrapper -->				

		