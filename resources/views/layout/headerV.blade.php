<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>EatNaija Vendor Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
        <meta content="Themesdesign" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="public/assets/images/favicon.ico">

        <!-- datepicker -->
        <link href="public/assets/libs/air-datepicker/css/datepicker.min.css" rel="stylesheet" type="text/css" />

        <!-- jvectormap -->
        <link href="public/assets/libs/jqvmap/jqvmap.min.css" rel="stylesheet" />

        <!-- Bootstrap Css -->
        <link href="public/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="public/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="public/assets/css/app.min.css" rel="stylesheet" type="text/css" />

    </head>
<style>
.page-title-box {
    background-color: #fd7e14 !important;
    margin: 0 -12px;
    background-image:{{asset('assets/assets/images/bg-pattern.png')}};
    background-size: cover;
}
body[data-topbar=colored] #page-topbar {
    background-color: #fd7e14 !important;
    -webkit-box-shadow: none;
    box-shadow: none;
}
body[data-topbar=colored] .dropdown.show .header-item {
    background-color: #fd7e14 !important;
}
.mm-active .active {
    color: #fd7e14 !important;
}

#sidebar-menu ul li a .uim-svg {
    fill: #fd7e14!important;
}

              </style>
    <body data-topbar="colored">
    @if(Auth::guard('vendor')->check())
        <!-- Begin page -->
        <div id="layout-wrapper">

            <header id="page-topbar">
                <div class="navbar-header">
                    <div class="d-flex">
                        <!-- LOGO -->
                        <div class="navbar-brand-box ">
                        <br>
                            <a href="/" class="logo logo-dark">
                                <span class="logo-sm">
                                    <img src="public/img/Eat-naija.png" alt="" height="25">
                                </span>
                                <span class="logo-lg">
                                    <img src="public/img/Eat-naija.png" alt="" height="40">
                                </span>
                            </a>

                            <a href="/" class="logo logo-light">
                                <span class="logo-sm">
                                    <img src="public/img/Eat-naija.png" alt="" height="22">
                                </span>
                                <span class="logo-lg">
                                    <img src="public/img/Eat-naija.png" alt="" height="20">
                                </span>
                            </a>

                        </div>
         <div class="vertical-menu">

<div data-simplebar class="h-100">

    <!--- Sidemenu -->
    <div id="sidebar-menu">
        <!-- Left Menu Start -->
        <ul class="metismenu list-unstyled" id="side-menu">
            <li class="menu-title">Menu</li>

            <li>
                <a href="/vendor_dashboard" class="waves-effect">
                    <div class="d-inline-block icons-sm mr-1"><i class="uim uim-airplay"></i></div>
                    <span>Dashboard</span>
                </a>
            </li>

           

            <li>
                <a href="javascript: void(0);" class="has-arrow waves-effect">
                    <div class="d-inline-block icons-sm mr-1"><i class="uim uim-comment-message"></i></div>
                    <span>Menu</span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                                    <li><a href="/upload_menu">Upload Menu</a></li>
                                    <li><a href="/yourmenu">View Menu</a></li>
                                
                                </ul>
            </li>

            <li>
                <a href="javascript: void(0);" class="has-arrow waves-effect">
                    <div class="d-inline-block icons-sm mr-1"><i class="uim uim-sign-in-alt"></i></div>
        
                    <span>Categories</span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                                    <li><a href="/categories">Add Category</a></li>
                                    
                                
                                </ul>
            </li>

            <li>
                <a href="javascript: void(0);" class="has-arrow waves-effect">
                    <div class="d-inline-block icons-sm mr-1"><i class="uim uim-grids"></i></div>
                    <span>Orders</span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                                     <li><a href="/orderrequests">Order Requests</a></li>
                                    <li><a href="/orders">Accepted Orders</a></li>
                                    <li><a href="/rejectedorders">Rejected Orders</a></li>
                                
                                </ul>
            </li>

          
        </ul>

    </div>
    <!-- Sidebar -->
</div>
</div>
<!-- Left Sidebar End -->

         

                        <button type="button" class="btn btn-sm px-3 font-size-24 header-item waves-effect" id="vertical-menu-btn">
                            <i class="mdi mdi-backburger"></i>
                        </button>

                        <!-- App Search-->
                       
                    </div>

                    <div class="d-flex">

                       
            
                      
                       

                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle header-profile-user"  src="{{ asset('public/storage/vendor_image/'.Auth::guard('vendor')->user()->image) }}" alt="Header Avatar">
                                <span class="d-none d-sm-inline-block ml-1">{{ Auth::guard('vendor')->user()->company_name }}</span>
                                <i class="mdi mdi-chevron-down d-none d-sm-inline-block"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- item-->
                                <a class="dropdown-item" href="/profile"><i class="mdi mdi-face-profile font-size-16 align-middle mr-1"></i> Profile</a>
                               
                                <div class="dropdown-divider"></div>
                                
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"> <i class="mdi mdi-logout font-size-16 align-middle mr-1"></i>
                                    {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                            </div>
                        </div>
            
                    </div>
                </div>

            </header>

          @endif




            <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                2020 © EatNaija.
                            </div>
                            <div class="col-sm-6">
                                <div class="text-sm-right d-none d-sm-block">
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

       
           
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- JAVASCRIPT -->
        <script src="public/assets/libs/jquery/jquery.min.js"></script>
        <script src="public/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="public/assets/libs/metismenu/metisMenu.min.js"></script>
        <script src="public/assets/libs/simplebar/simplebar.min.js"></script>
        <script src="public/assets/libs/node-waves/waves.min.js"></script>

        <script src="https://unicons.iconscout.com/release/v2.0.1/script/monochrome/bundle.js"></script>

        <!-- datepicker -->
        <script src="public/assets/libs/air-datepicker/js/datepicker.min.js"></script>
        <script src="public/assets/libs/air-datepicker/js/i18n/datepicker.en.js"></script>

        <!-- apexcharts -->
        <script src="public/assets/libs/apexcharts/apexcharts.min.js"></script>

        <script src="public/assets/libs/jquery-knob/jquery.knob.min.js"></script> 

        <!-- Jq vector map -->
        <script src="public/assets/libs/jqvmap/jquery.vmap.min.js"></script>
        <script src="public/assets/libs/jqvmap/maps/jquery.vmap.usa.js"></script>

        <script src="public/assets/js/pages/dashboard.init.js"></script>

        <script src="public/assets/js/app.js"></script>

    </body>
</html>
