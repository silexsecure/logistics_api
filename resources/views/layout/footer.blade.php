<footer class="footer-three footer-grey p-top-95">
        <div class="footer-top p-bottom-25">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-sm-6">
                        <div class="widget widget_pages">
                            <h2 class="widget-title">Company Info</h2>
                            <ul class="list-unstyled">
                                <li class="page-item"><a href="/about">About Us</a></li>
                                <li class="page-item"><a href="/contactus">Contact Us</a></li>
                               
                                <li class="page-item"><a href="/privacy_policy">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div><!-- ends: .col-lg-3 -->
                   
                    <div class="col-lg-3 col-sm-6">
                        <div class="widget widget_social">
                            <h2 class="widget-title">Connect with Us</h2>
                            <ul class="list-unstyled social-list">
                                <li><a href="https://twitter.com/Askeatnaija" target="_blank"><span class="twitter"><i class="fab fa-twitter"></i></span> Twitter</a></li>
                                <li><a  href="https://web.facebook.com/theeatnaija/" target="_blank"><span class="facebook"><i class="fab fa-facebook-f"></i></span> Facebook</a></li>
                                <li><a href="https://www.instagram.com/the_eatnaija/" target="_blank"><span class="instagram"><i class="fab fa-instagram"></i></span> Instagram</a></li>
                                <li><a  href=" https://api.whatsapp.com/send?phone=+2348036216373" target="_blank"><span class="gplus"><i class="fab fa-whatsapp"></i></span> Whatsapp</a></li>
                            </ul>
                        </div><!-- ends: .widget -->
                    </div><!-- ends: .col-lg-3 -->
                    <div class="col-lg-1 d-flex justify-content-lg-center  col-sm-6">
                        
                        </div><!-- ends: .col-lg-3 -->
                    <div class="col-lg-4 col-sm-6">
                        <div class="widget widget_text">
                            <h2 class="widget-title">EatNaija on Mobile</h2>
                            <div class="textwidget">
                                <p>Download the EatNaija app today..</p>
                                <ul class="list-unstyled store-btns">
                                    <li><a href="" class="btn-gradient btn-gradient-two btn btn-md btn-icon icon-left"><span class="fab fa-apple"></span> App Store</a></li>
                                    <li><a href="" class="btn btn-dark btn-md btn-icon btn-icon"><span class="fab fa-android"></span> Google Play</a></li>
                                </ul>
                            </div>
                        </div><!-- ends: .widget -->
                    </div><!-- ends: .col-lg-3 -->
                </div>
            </div>
        </div><!-- ends: .footer-top -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-bottom--content">
                            <a href="" class="footer-logo"><img src="img/logo.png" alt=""></a>
                            <p class="m-0 copy-text">©2020 <a href="">EatNaija</a></p>
                            <ul class="list-unstyled lng-list">
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- ends: .footer-bottom -->
    </footer><!-- ends: .footer -->
    <div class="modal fade" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="login_modal_label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    

                    <h5 class="modal-title" id="login_modal_label"><i class="la la-lock"></i> Sign In</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                     @if (session('error'))
                                         <div class="mb-4 font-medium text-sm text-red" style="color:red">
                                             {{ session('error') }}
                                         </div>
                                     @endif
                   	<?php
                        					
                        				Session::put('url.intended',URL::previous())
                        					
                        						
                        							?>
                        						
                    <form method="POST" id="login-form" action="{{ route('logged_in') }}">
            @csrf
                        <input type="email" class="form-control" placeholder=" Email" name="email" required>
                        <input type="password" class="form-control" placeholder="Password" name="password" required>
                        <div class="keep_signed custom-control custom-checkbox checkbox-outline checkbox-outline-primary">
                            <input type="checkbox" class="custom-control-input" name="keep_signed_in" value="1" id="keep_signed_in">
                            <label for="keep_signed_in" class="not_empty custom-control-label">Keep me signed in</label>
                        </div>
                        <button type="submit" class="btn btn-block btn-lg btn-gradient btn-gradient-two">Sign In</button>
                    </form>
                    <div class="form-excerpts">
                        <ul class="list-unstyled">
                            <li>Not a member? <a data-toggle="modal" data-target="#signup_modal">Sign up</a></li>
                            <li><a href="" data-toggle="modal" data-target="#login_modalll">Recover Password</a></li>
                            
                        </ul>
                         <p><a href="/register_vendor">Want to be a Vendor?</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="signup_modal" tabindex="-1" role="dialog" aria-labelledby="signup_modal_label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="signup_modal_label"><i class="la la-lock"></i> Sign Up</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form  id="signup-form" method="POST" action="{{ route('reg_user') }}">
            @csrf
                        <input type="email" class="form-control" placeholder="Email" name="email" required>
                        <input type="text" class="form-control" placeholder="Fullname" name="name" required>
                         <input type="text" class="form-control" placeholder="Phone Number" name="phone" required>
                        <input type="password" class="form-control" placeholder="Password" name="password" required>
                        <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" required>
                     
                        <button type="submit" class="btn btn-block btn-lg btn-gradient btn-gradient-two">Sign Up</button>
                    </form>
                    <div class="form-excerpts">
                        <ul class="list-unstyled">
                            <li>Already a member? <a href="" data-toggle="modal" data-target="#login_modal">Sign In</a></li>
                            <li  ><a href="" data-toggle="modal" data-target="#login_modalll">Recover Password</a></li>
                            <br>
                          
                        </ul>
                        <p><a href="/register_vendor">Want to be a Vendor?</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    
    
     <div class="modal fade" id="login_modall" tabindex="-1" role="dialog" aria-labelledby="signup_modal_label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="signup_modal_label"><i class="la la-lock"></i> Alert</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  
                   <p>{{session('success')}}</p> 
                </div>
            </div>
        </div>
    </div>

    
    
     <div class="modal fade" id="login_modalll" tabindex="-1" role="dialog" aria-labelledby="login_modal_label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="login_modal_label"><i class="la la-lock"></i> Reset Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                                       <p>Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to set a new one.</p>

                    <form method="POST" id="login-form" action="{{ route('send_mail') }}">
            @csrf
                        <input type="email" class="form-control" placeholder=" Email" name="email" required>
                       
                <br>
                        <button type="submit" class="btn btn-block btn-md btn-gradient btn-gradient-two">Send Email</button>
                    </form>
            
                </div>
            </div>
        </div>
    </div>
    
<div class="modal fade" id="myModal">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<div class="pull-left">My Gallery Title</div>
<button type="button" class="close" data-dismiss="modal" title="Close"> <span class="glyphicon glyphicon-remove"></span></button>
</div>
<div class="modal-body">

<!--CAROUSEL CODE GOES HERE-->
<div id="myGallery" class="carousel slide" data-interval="false">
<div class="carousel-inner">
<div class="item active"> <img src="https://placeimg.com/600/400/nature/1" alt="item0">
<div class="carousel-caption">
<h3>Heading 3</h3>
<p>Slide 0  description.</p>
</div>
</div>
<div class="item"> <img src="https://placeimg.com/600/400/nature/2" alt="item1">
<div class="carousel-caption">
<h3>Heading 3</h3>
<p>Slide 1 description.</p>
</div>
</div>
<div class="item"> <img src="https://placeimg.com/600/400/nature/3" alt="item2">
<div class="carousel-caption">
<h3>Heading 3</h3>
<p>Slide 2  description.</p>
</div>
</div>
<div class="item"> <img src="https://placeimg.com/600/400/nature/4" alt="item3">
<div class="carousel-caption">
<h3>Heading 3</h3>
<p>Slide 3 description.</p>
</div>
</div>
<div class="item"> <img src="https://placeimg.com/600/400/nature/5" alt="item4">
<div class="carousel-caption">
<h3>Heading 3</h3>
<p>Slide 4 description.</p>
</div>
</div>
<div class="item"> <img src="https://placeimg.com/600/400/nature/6" alt="item5">
<div class="carousel-caption">
<h3>Heading 3</h3>
<p>Slide 5 description.</p>
</div>
</div>
<!--end carousel-inner--></div>
<!--Begin Previous and Next buttons-->
<a class="left carousel-control" href="#myGallery" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#myGallery" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span></a>
<!--end carousel--></div>
<!--end modal-body--></div>
<div class="modal-footer">
<div class="pull-left">
<small>Photographs by <a href="https://placeimg.com" target="new">placeimg.com</a></small>
</div>
<button class="btn-sm close" type="button" data-dismiss="modal">Close</button>
<!--end modal-footer--></div>
<!--end modal-content--></div>
<!--end modal-dialoge--></div>
<!--end myModal-->></div>    

    @if(Session::has('error')) 
    <script type="text/javascript">
        $(document).ready(function() {
            $('#login_modal').modal('show');
        });
	</script>
	@endif
	
	 @if(Session::has('success')) 
    <script type="text/javascript">
        $(document).ready(function() {
            $('#login_modall').modal('show');
        });
	</script>
	@endif
	   

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0C5etf1GVmL_ldVAichWwFFVcDfa1y_c"></script>
    <!-- inject:js-->
    <script src="public/vendor_assets/js/jquery/jquery-1.12.3.js"></script>
    <script src="public/vendor_assets/js/bootstrap/popper.js"></script>
    <script src="public/vendor_assets/js/bootstrap/bootstrap.min.js"></script>
    <script src="public/vendor_assets/js/jquery-ui.min.js"></script>
    <script src="public/vendor_assets/js/jquery.barrating.min.js"></script>
    <script src="public/vendor_assets/js/jquery.counterup.min.js"></script>
    <script src="public/vendor_assets/js/jquery.magnific-popup.min.js"></script>
    <script src="public/vendor_assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="public/vendor_assets/js/jquery.waypoints.min.js"></script>
    <script src="public/vendor_assets/js/masonry.pkgd.min.js"></script>
    <script src="public/vendor_assets/js/owl.carousel.min.js"></script>
    <script src="public/vendor_assets/js/select2.full.min.js"></script>
    <script src="public/vendor_assets/js/slick.min.js"></script>
    <script src="public/theme_assets/js/locator.js"></script>
    <script src="public/theme_assets/js/main.js"></script>
    <script src="public/theme_assets/js/map.js"></script>
    <!-- endinject-->
    

</body>

</html>