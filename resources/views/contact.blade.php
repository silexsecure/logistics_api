 @extends('layout.footer')
 <style>
 .bb {
  
   padding-top:50px;
}
     .cap {
    background-color: #fa8b0c;
        margin-top:0;

}

.msg-form {
    background-color: white;
    padding: 20px
}

.pad-icon {
    padding-top: 50px;
    padding-left: 20px
}

.pad-icon a {
    text-decoration: none;
    margin-right: 40px
}

.input-group input:focus {
    border: 1px solid blue
}

.pad-icon a:active {
    height: 30px;
    width: 30px;
    background-color: #0080ff;
    border-radius: 100%
}

.links {
    padding-top: 50px;
    width: 50%
}
.la {
   color:white;
}

#bordering a:active {
    border: 1px solid #0080ff
}
.pad-icon a {
    color: white;
    text-decoration: none;
    background-color: transparent;
}

@media(min-width:568px) {
    .cap {
        margin: 100px 30px;
        width: 96%;
        padding-top: 50px;
        padding-bottom: 50px
    }
}

@media(max-width:567px) {
    .cap {
        margin: 10px 10px;
        width: 94%;
        padding-top: 20px;
        padding-bottom: 20px
    }

    .pad {
        padding-top: 20px
    }
}
 </style>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EatNaija</title>
    <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,600,700" rel="stylesheet">
    <!-- inject:css-->
    <link rel="stylesheet" href="public/vendor_assets/css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="public/vendor_assets/css/brands.css">
    <link rel="stylesheet" href="public/vendor_assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="public/vendor_assets/css/jquery-ui.css">
    <link rel="stylesheet" href="public/vendor_assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="public/vendor_assets/css/line-awesome.min.css">
    <link rel="stylesheet" href="public/vendor_assets/css/magnific-popup.css">
    <link rel="stylesheet" href="public/vendor_assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="public/vendor_assets/css/select2.min.css">
    <link rel="stylesheet" href="public/vendor_assets/css/slick.css">
    <link rel="stylesheet" href="public/style.css">
    <!-- endinject -->
    <link rel="icon" type="image/png" sizes="32x32" href="public/favicon.ico">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/hilios/jQuery.countdown/2.1.0/dist/jquery.countdown.min.js"></script> 
</head>

<body>
@guest
    <section class="intro-wrapper bgimage overlay overlay--dark" style="height:400px">
        <div class="bg_image_holder"><img src="public/img/intro.jpg" alt=""></div>
        <div class="mainmenu-wrapper">
            <div class="menu-area menu1 menu--light">
                <div class="top-menu-area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="menu-fullwidth">
                                    <div class="logo-wrapper order-lg-0 order-sm-1">
                                        <div class="logo logo-top">
                                            <a href="/"><img src="public/img/Eat-naija.png" alt="logo image" class="img-fluid"></a>
                                        </div>
                                    </div><!-- ends: .logo-wrapper -->
                                    @if (Route::has('register'))
                                    <div class="menu-container order-lg-1 order-sm-0">
                                        <div class="d_menu">
                                            <nav class="navbar navbar-expand-lg mainmenu__menu">
                                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#direo-navbar-collapse" aria-controls="direo-navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                                                    <span class="navbar-toggler-icon icon-menu"><i class="la la-reorder"></i></span>
                                                </button>
                                                <!-- Collect the nav links, forms, and other content for toggling -->
                                                <div class="collapse navbar-collapse" id="direo-navbar-collapse">
                                                    <ul class="navbar-nav">
                                                        <li>
                                                            <a href="/">Home</a>
                                                        </li>
                                                        <li class="dropdown has_dropdown">
                                                          <a href="/about">About Us</a> 
                                                           
                                                        </li>
                                                         <li class="dropdown has_dropdown">
                                                           <a href="/contactus">Contact Us</a> 
                                                           
                                                        </li>
                                                        <li class="dropdown has_dropdown">
                                                            <a href="#" class="dropdown-toggle" id="drop4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
                                                            <ul class="dropdown-menu"  aria-labelledby="drop4">
                                                            <li><a href="/restaurant">Restaurant</a></li>
                                                                 <li><a href="/food_ports">Food Port</a></li>
                                                                <li><a href="/cafe">Cafe/Eatery</a></li>
                                                                <li><a href="/Healthy&Wellness">Healthy & Wellness Products</a></li>
                                                                <li><a href="/foodcompany">Food Company</a></li>
                                                                <li><a href="/foodequipment">Food Equipment</a></li>
                                                            </ul>
                                                        </li>
                                                      
                                                </div>
                                                <!-- /.navbar-collapse -->
                                            </nav>
                                        </div>
                                    </div>
                                    <div class="menu-right order-lg-2 order-sm-2">
                                       
                                        <!-- start .author-area -->
                                        <div class="author-area">
                                            <div class="author__access_area">
                                                <ul class="d-flex list-unstyled align-items-center">
                                                    <li>
                                                        <a href="add-listing.html" class="btn btn-xs btn-gradient btn-gradient-two" data-toggle="modal" data-target="#login_modal">
                                                          
                                                            <span class="la la-plus"></span> Sign in
                                                           
                                                        </a>
                                                    </li>
                                                    <li>
                                                        
                                                        <a href="" class="access-link" data-toggle="modal" data-target="#signup_modal">Register</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="offcanvas-menu d-none">
                                        <ul class="d-flex list-unstyled align-items-center">
                                                    <li>
                                                        <a href="add-listing.html" class="btn btn-xs btn-gradient btn-gradient-two" data-toggle="modal" data-target="#login_modal">
                                                            <span class="la la-plus"></span> Sign in
                                                        </a>
                                                    </li>
                                        </div><!-- ends: .offcanvas-menu -->
                                    </div><!-- ends: .menu-right -->
                                </div>
                            </div>
                        </div>
                        <!-- end /.row -->
                    </div>
                    <!-- end /.container -->
                </div>
                <!-- end  -->
            </div>
        </div><!-- ends: .mainmenu-wrapper -->
                                        @endif
						@else
						
						
						
                        <section class="intro-wrapper bgimage overlay overlay--dark" style="height:400px">
        <div class="bg_image_holder"><img src="public/img/intro.jpg" alt=""></div>
        <div class="mainmenu-wrapper">
            <div class="menu-area menu1 menu--light">
                <div class="top-menu-area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="menu-fullwidth">
                                    <div class="logo-wrapper order-lg-0 order-sm-1">
                                        <div class="logo logo-top">
                                            <a href="/"><img src="public/img/logo-white.png" alt="logo image" class="img-fluid"></a>
                                        </div>
                                    </div><!-- ends: .logo-wrapper -->
                                    
                                    <div class="menu-container order-lg-1 order-sm-0">
                                        <div class="d_menu">
                                            <nav class="navbar navbar-expand-lg mainmenu__menu">
                                               <button class="navbar-toggler offcanvas-menu__user" type="button" aria-expanded="false" aria-label="Toggle navigation">
                                                    <span class="navbar-toggler-icon icon-menu"><i class="la la-reorder"></i></span>
                                                </button>
                                                <!-- Collect the nav links, forms, and other content for toggling -->
                                                <div class="collapse navbar-collapse" id="direo-navbar-collapse" >
                                                    <ul class="navbar-nav" >
                                                        <li>
                                                            <a href="/">Home</a>
                                                        </li>
                                                        <li class="dropdown has_dropdown">
                                                           <a href="/about">About Us</a> 
                                                           
                                                        </li>
                                                         <li class="dropdown has_dropdown">
                                                           <a href="/contactus">Contact Us</a> 
                                                           
                                                        </li>
                                                        <li class="dropdown has_dropdown">
                                                            <a href="#" class="dropdown-toggle" id="drop4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
                                                            <ul class="dropdown-menu" aria-labelledby="drop4">
                                                            <li><a href="/restaurant">Restaurant</a></li>
                                                                 <li><a href="/food_ports">Food Port</a></li>
                                                                <li><a href="/cafe">Cafe/Eatery</a></li>
                                                                <li><a href="/Healthy&Wellness">Healthy & Wellness Products</a></li>
                                                                <li><a href="/foodcompany">Food Company</a></li>
                                                                <li><a href="/foodequipment">Food Equipment</a></li>
                                                            </ul>
                                                        </li>
                                                      
                                                </div>
                                                <!-- /.navbar-collapse -->
                                            </nav>
                                        </div>
                                    </div>
                                    <div class="menu-right order-lg-2 order-sm-2">
                                       
                                        <!-- start .author-area -->
                                        <div class="author-area">
                                            <div class="author__access_area">
                                                <ul class="d-flex list-unstyled align-items-center">
                                                    <li>
                                                        <a href="/cart" class="btn btn-xs btn-gradient btn-gradient-two" >
                                                      <span class="la la-shopping-cart"></span>  <span> {{$cartnum}} </span> 
                                                        </a>
                                                    </li>
                                                    <li  class="btn btn-xs btn-gradient btn-gradient-two text-white"><a href="{{ route('logout') }}" title="Register" style="color:white" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                         {{ __('Logout') }}
                                    </a>
                                   
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end .author-area -->
                                        
                                                      
                                        <div class="offcanvas-menu d-none">
                                             <a href="/cart" class="btn btn-xs btn-gradient btn-gradient-two" style="margin-right:5px;" >
                                                      <span class="la la-shopping-cart"></span>  <span> {{$cartnum}} </span> 
                                                        </a>    
                                            
                                                   
                                            <div class="offcanvas-menu__contents">
                                                <a href="" class="offcanvas-menu__close"><i class="la la-times-circle"></i></a>
                                                <div class="author-avatar">
                                                    @if(Auth::user()->image==NULL)
                                                    <img src="public/img/author-avatar.png" alt="" class="rounded-circle">
                                                    @else
                                               <img src="public/storage/user_image/{{Auth::user()->image}}" alt="" class="rounded-circle" style="width:120px">
                                               @endif
                                                </div>
                                                <ul class="list-unstyled">
                                                       <li>
                                                            <a href="/">Home</a>
                                                        </li>
                                                        <li class="dropdown has_dropdown">
                                                           <a href="/about">About Us</a> 
                                                           
                                                        </li>
                                                         <li class="dropdown has_dropdown">
                                                           <a href="/contactus">Contact Us</a> 
                                                           
                                                        </li>
                                                        <li class="dropdown has_dropdown">
                                                            <a href="#" class="dropdown-toggle" id="drop4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
                                                            <ul class="dropdown-menu" aria-labelledby="drop4">
                                                            <li><a href="/restaurant">Restaurant</a></li>
                                                                 <li><a href="/food_ports">Food Port</a></li>
                                                                <li><a href="/cafe">Cafe/Eatery</a></li>
                                                                <li><a href="/Healthy&Wellness">Healthy & Wellness Products</a></li>
                                                                <li><a href="/foodcompany">Food Company</a></li>
                                                                <li><a href="/foodequipment">Food Equipment</a></li>
                                                            </ul>
                                                        </li>
                                                    <li><a href="/profile{{ Auth::user()->id }}">My Profile</a></li>
                                                     <li><a href="{{ route('logout') }}" title="Register" style="color:black" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                         {{ __('Logout') }}
                                    </a>
                                   
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form></li>
                                                </ul>
                                                
                                            </div><!-- ends: .author-info -->
                                        </div><!-- ends: .offcanvas-menu -->
                                    </div><!-- ends: .menu-right -->
                                </div>
                            </div>
                        </div>
                        <!-- end /.row -->
                    </div>
                    <!-- end /.container -->
                </div>
                <!-- end  -->
            </div>
        </div><!-- ends: .mainmenu-wrapper -->
        @endguest
        <div class="directory_content_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="search_title_area">
                        
                            <h2 class="title">Contact Us</h2>
                        </div><!-- ends: .search_title_area -->
                      
                       
                    </div><!-- ends: .col-lg-10 -->
                </div>
            </div>
        </div><!-- ends: .directory_search_area -->
</section>
<h2 class="text-center bb">Contact Form</h2>
<section>
<div class="container-fluid cap rounded">
    <div class="container">
    <div class="row px-9">
        <div class="col-sm-7">
            <div>
                <h3 class="text-white">Contact Us</h3>
            </div>
            <div class="links" id="bordering"> <a href="#" class="btn rounded text-white p-3"><i class="la la-phone icon  pr-3"></i>+2347088881776 (WhatsApp Only)</a>
            <a href="#" class="btn rounded text-white p-3"><i class="la la-envelope icon  pr-3"></i>eat9aija@gmail.com</a>
            <a href="#" class="btn rounded text-white p-2"><i class="la la-map-marker icon pr-3"></i>No 34 T.O.S Benson Crescent Utako</a> </div>
            <div class="pt-lg-4 d-flex flex-row justify-content-start">
                             <p class="pad-icon"><a href="https://web.facebook.com/theeatnaija/" target="_blank"><span class="fab fa-facebook-f"></span></a></p>
                                <p class="pad-icon"><a href="https://twitter.com/Askeatnaija" target="_blank"><span class="fab fa-twitter"></span></a></p>
                                <p class="pad-icon"><a href="https://www.instagram.com/the_eatnaija/" target="_blank"><span class="fab fa-instagram"></span></a></p>
                                <p class="pad-icon"><a href=" https://api.whatsapp.com/send?phone=+2348036216373" target="_blank"><span class="fab fa-whatsapp"></span></a></p>
                         </div>
        </div>
        <div class="col-sm-5 pad">
           
                <form id="atbdp-contact-form" class="rounded msg-form" action="{{ route('contact') }}" method="post" role="form">
                                @csrf
                <div class="form-group"> <label for="name" class="h6">Your Name</label>
                    <div class="input-group border rounded">
                        <div class="input-group-addon px-2 pt-1">
                            <p class=" text-primary"></p>
                        </div> <input type="text" class="form-control border-0" name="name">
                    </div>
                </div>
                <div class="form-group"> <label for="name" class="h6">Subject</label>
                    <div class="input-group border rounded">
                        <div class="input-group-addon px-2 pt-1">
                            <p class="\ text-primary"></p>
                        </div> <input type="text" class="form-control border-0" name="subject">
                    </div>
                </div>
                <div class="form-group"> <label for="name" class="h6">Email</label>
                    <div class="input-group border rounded">
                        <div class="input-group-addon px-2 pt-1"> <i class=" text-primary"></i> </div> <input type="email" class="form-control border-0" name="email">
                    </div>
                </div>
                <div class="form-group"> <label for="msg" class="h6">Message</label> <textarea name="message" id="msgus" cols="10" rows="5" class="form-control bg-light" placeholder="Message"></textarea> </div>
                <div class="form-group d-flex justify-content-end"> 
                <button type="submit" class="btn btn-gradient btn-gradient-two btn-block">Submit</button> </div>
            </form>
        </div>
    </div>
</div>
 </div>      
   </section> 
   