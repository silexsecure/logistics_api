<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// VENDORS
Route::post('/login/vendor','App\Http\Controllers\VendorappController@authenticate');
Route::post('/register/vendor','App\Http\Controllers\VendorappController@create_vendor');
Route::post('/upload','App\Http\Controllers\VendorappController@store');
Route::get('/products','App\Http\Controllers\VendorappController@products');
Route::get('/categories','App\Http\Controllers\VendorappController@categories');
Route::get('/requests','App\Http\Controllers\VendorappController@requests');
Route::get('/five_requests','App\Http\Controllers\VendorappController@five_requests');
Route::post('/accept/{id}', 'App\Http\Controllers\VendorappController@accept');
Route::post('/reject/{id}', 'App\Http\Controllers\VendorappController@reject');
Route::post('/deliver/{id}', 'App\Http\Controllers\VendorappController@deliver');
Route::get('/amount','App\Http\Controllers\VendorappController@amount');
Route::post('/update/{id}', 'App\Http\Controllers\VendorappController@update_store');
Route::delete('/delete_store/{id}', 'App\Http\Controllers\VendorappController@delete_store');
Route::post('/create_category','App\Http\Controllers\VendorappController@createcategory');
Route::get('/menu','App\Http\Controllers\VendorappController@category');
Route::get('/dashboard','App\Http\Controllers\VendorappController@dashboard');
Route::post('/changepassword', 'App\Http\Controllers\VendorappController@changePassword');
Route::post('/update_vendor', 'App\Http\Controllers\VendorappController@update_vendor');
Route::post('/reset_password', 'App\Http\Controllers\VendorappController@password_reset');
Route::get('/banks','App\Http\Controllers\VendorappController@bank_details');
Route::post('/getDistanceAmount','App\Http\Controllers\VendorappController@getDistanceAmount');


//  USERS
Route::post('/login/user','App\Http\Controllers\UsersController@authenticate');
Route::post('/register/user','App\Http\Controllers\UsersController@create_user');
Route::post('/register/mail','App\Http\Controllers\UsersController@gmail_create_user');
Route::post('/otp','App\Http\Controllers\UsersController@loginWithOtp');
Route::post('/changeuserpassword', 'App\Http\Controllers\UsersController@changePassword');
Route::post('/addtocart/{id}', 'App\Http\Controllers\UsersController@addToCart');
Route::post('/subtractcart/{id}', 'App\Http\Controllers\UsersController@subtractfromCart');
Route::get('/cart', 'App\Http\Controllers\UsersController@viewcart');
Route::post('/checkout', 'App\Http\Controllers\UsersController@checkout');
Route::delete('/deletecart/{id}', 'App\Http\Controllers\UsersController@destroy2');
Route::get('/restaurants', 'App\Http\Controllers\UsersController@restaurants');
Route::get('/menu/{id}', 'App\Http\Controllers\UsersController@menu');
Route::post('/update','App\Http\Controllers\UsersController@update_user');
Route::get('/food_ports', 'App\Http\Controllers\UsersController@ports');
Route::get('/cafe', 'App\Http\Controllers\UsersController@cafe');
Route::get('/Healthy&Wellness', 'App\Http\Controllers\UsersController@wellness');
Route::get('/foodcompany', 'App\Http\Controllers\UsersController@company');
Route::get('/foodequipment', 'App\Http\Controllers\UsersController@equipment');
Route::get('/orderhistory', 'App\Http\Controllers\UsersController@orderhistory');
Route::post('/resetpassword', 'App\Http\Controllers\UsersController@password_reset');
Route::get('/profile', 'App\Http\Controllers\UsersController@profile');
Route::post('/report', 'App\Http\Controllers\UsersController@report');
Route::get('/get_delivery', 'App\Http\Controllers\UsersController@checkout2');
Route::post('/delivery', 'App\Http\Controllers\UsersController@delivery');
Route::get('/getCategories', 'App\Http\Controllers\UsersController@getCategories');
Route::get('/getVendorDetails/{id}','App\Http\Controllers\VendorappController@getVendorDetails');
Route::get('/amountInKM','App\Http\Controllers\VendorappController@amountInKM');

Route::post('/splitePayment','App\Http\Controllers\UsersController@splitePayment');


//Logistics
Route::get('/getAllLogistics', 'App\Http\Controllers\LogisticsController@getAllLogistics');
Route::get('/getAllPendingLogistics', 'App\Http\Controllers\LogisticsController@getAllPendingLogistics');
Route::get('/ViewSingleLogistics/{id}', 'App\Http\Controllers\LogisticsController@ViewSingleLogistics');
Route::get('/ViewAllMyDeliveredLogistics/{delivery_man_id}', 'App\Http\Controllers\LogisticsController@ViewAllMyDeliveredLogistics');
Route::get('/ViewAllMyOnGoingLogistics/{delivery_man_id}', 'App\Http\Controllers\LogisticsController@ViewAllMyOnGoingLogistics');
Route::get('/ViewMyRejectedLogistics/{delivery_man_id}', 'App\Http\Controllers\LogisticsController@ViewMyRejectedLogistics');
Route::get('/ViewMyCanceledLogistics/{delivery_man_id}', 'App\Http\Controllers\LogisticsController@ViewMyCanceledLogistics');
Route::post('/authenticate', 'App\Http\Controllers\LogisticsController@authenticate');

Route::PUT('/pickALogistics', 'App\Http\Controllers\LogisticsController@pickALogistics');
Route::post('/canceleALogistics', 'App\Http\Controllers\LogisticsController@canceleALogistics');


Route::match(["POST","GET"],'/SendDeliveryCode', 'App\Http\Controllers\LogisticsController@SendDeliveryCode');
Route::post('/canceledelivery', 'App\Http\Controllers\LogisticsController@canceledelivery');
Route::post('/confirmdelivery', 'App\Http\Controllers\LogisticsController@confirmdelivery');

Route::post('/checkBalance', 'App\Http\Controllers\LogisticsController@checkBalance');
Route::post('/fundwalet', 'App\Http\Controllers\LogisticsController@fundwalet');
Route::post('/validate_bank', 'App\Http\Controllers\LogisticsController@validate_bank');
Route::post('/withdraw', 'App\Http\Controllers\LogisticsController@withdraw');
Route::post('/transactions', 'App\Http\Controllers\LogisticsController@transactions');
Route::get('/sms', 'App\Http\Controllers\LogisticsController@sms');
Route::get('/me/{id}', 'App\Http\Controllers\LogisticsController@me');


