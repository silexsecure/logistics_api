<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
 //   return view('index');
//});
//Admin

Route::get('/register_admin','App\Http\Controllers\AdminController@show_admin_Form');
Route::post('/register/admin','App\Http\Controllers\AdminController@create_admin');
Route::get('/login_admin','App\Http\Controllers\AdminController@showadminLoginForm');
Route::post('/login/admin','App\Http\Controllers\AdminController@adminLogin');
Route::get('/dashboard','App\Http\Controllers\AdminController@index');
Route::get('/addvendor','App\Http\Controllers\AdminController@show_vendor_Form');
Route::post('/addvendor','App\Http\Controllers\AdminController@create_vendor');
Route::get('/vendors','App\Http\Controllers\AdminController@show');
Route::delete('vendors/{id}','App\Http\Controllers\AdminController@destroy');
Route::get('{id}editvendor',   'App\Http\Controllers\AdminController@update_vendor');
Route::patch('edit_vendor/{id}', 'App\Http\Controllers\AdminController@update');
Route::get('category', 'App\Http\Controllers\AdminController@showcategory');
Route::post('addcategory', 'App\Http\Controllers\AdminController@createcategory');
Route::get('category', 'App\Http\Controllers\AdminController@category');
Route::delete('delete_category/{id}', 'App\Http\Controllers\AdminController@destroycategory');
Route::get('/users','App\Http\Controllers\AdminController@show_users');
Route::get('/orders','App\Http\Controllers\AdminController@orders');
Route::delete('delete_user/{id}', 'App\Http\Controllers\AdminController@destroy_user');
Route::delete('delete_transaction/{id}', 'App\Http\Controllers\AdminController@destroy_transaction');
Route::get('/reports', 'App\Http\Controllers\AdminController@reports');
Route::delete('delete_report/{id}', 'App\Http\Controllers\AdminController@destroy_report');
Route::get('/rejected','App\Http\Controllers\AdminController@rejected_orders');
Route::post('send_email/{id}', 'App\Http\Controllers\AdminController@send_email');
Route::post('bulk_email', 'App\Http\Controllers\AdminController@bulk_email');
Route::post('sendemail/{id}', 'App\Http\Controllers\AdminController@send_email2');
Route::post('bulkemail', 'App\Http\Controllers\AdminController@bulk_email2');
Route::get('{id}vendor_details',   'App\Http\Controllers\AdminController@update_vendor2');
Route::post('suspend/{id}', 'App\Http\Controllers\AdminController@suspend');
Route::post('activate/{id}', 'App\Http\Controllers\AdminController@activate');
Route::get('/requests','App\Http\Controllers\AdminController@show_requests');
Route::post('approve/{id}', 'App\Http\Controllers\AdminController@approve');
Route::post('/upload_food','App\Http\Controllers\AdminController@upload_food');
Route::post('/create_category','App\Http\Controllers\AdminController@create_category');
Route::post('/create_cms', 'App\Http\Controllers\AdminController@create_cms');
Route::get('/cms', 'App\Http\Controllers\AdminController@cms');
Route::post('/create_policy', 'App\Http\Controllers\AdminController@create_policy');
Route::get('/privacypolicy', 'App\Http\Controllers\AdminController@policy');
Route::post('/send_mail2', 'App\Http\Controllers\AdminController@password_reset2')->name('send_mail2');
Route::get('/reset_password{id}', 'App\Http\Controllers\AdminController@reset_password2');
Route::post('/password_reset/{id}', 'App\Http\Controllers\AdminController@reset2');
Route::get('adverts', 'App\Http\Controllers\AdminController@advert_show');
Route::post('addadvert', 'App\Http\Controllers\AdminController@create_advert');
Route::delete('delete_advert/{id}', 'App\Http\Controllers\AdminController@destroyadvert');
Route::get('chatbot', 'App\Http\Controllers\AdminController@showchatbot');
Route::post('addresponse', 'App\Http\Controllers\AdminController@createchatbot');
Route::get('chatbot', 'App\Http\Controllers\AdminController@chatbot');
Route::delete('delete_response/{id}', 'App\Http\Controllers\AdminController@destroychatbot');
Route::get('/contactform','App\Http\Controllers\AdminController@show_contact');
Route::post('sendcontactemail/{id}', 'App\Http\Controllers\AdminController@send_email3');
Route::delete('delete_contact/{id}', 'App\Http\Controllers\AdminController@destroy_contact');
Route::get('/{id}profile','App\Http\Controllers\AdminController@updateshow');
Route::post('update_profile/{id}', 'App\Http\Controllers\AdminController@update_admin');
Route::get('/addlogistics','App\Http\Controllers\AdminController@show_log_Form');
Route::post('/addlog','App\Http\Controllers\AdminController@create_log');
Route::get('/companies','App\Http\Controllers\AdminController@showLog');

//Vendor
Route::get('/register_vendor','App\Http\Controllers\AdminController@reg_vendor_Form');
Route::get('/login_vendor','App\Http\Controllers\VendorController@showvendorLoginForm');
Route::post('/login/vendor','App\Http\Controllers\VendorController@vendorLogin');
Route::get('/vendor_dashboard','App\Http\Controllers\VendorController@index');
Route::get('/upload_menu','App\Http\Controllers\SalesController@uploadForm');
Route::post('/upload','App\Http\Controllers\SalesController@store');
Route::get('categories', 'App\Http\Controllers\VendorController@showcategory');
Route::post('addcategories', 'App\Http\Controllers\VendorController@createcategory');
Route::get('categories', 'App\Http\Controllers\VendorController@category');
Route::get('/resetvendorpassword{id}', 'App\Http\Controllers\VendorappController@reset_password3');
Route::post('/vendor_reset/{id}', 'App\Http\Controllers\VendorappController@reset3');
Route::get('/yourmenu', 'App\Http\Controllers\VendorController@showmenu');
Route::get('/ordersvendors', 'App\Http\Controllers\VendorController@showorders');
Route::get('/rejectedorders', 'App\Http\Controllers\VendorController@showrejected');
Route::get('/orderrequests', 'App\Http\Controllers\VendorController@showrequests');
Route::post('/accept/{id}', 'App\Http\Controllers\VendorController@accept');
Route::post('/reject/{id}', 'App\Http\Controllers\VendorController@reject');
Route::delete('/delete_store/{id}', 'App\Http\Controllers\VendorController@delete_store');
Route::get('/profile', 'App\Http\Controllers\VendorController@profile');
Route::PUT('/updateVendorProfile', 'App\Http\Controllers\VendorController@updateVendorProfile');


Route::post('/withdraw', 'App\Http\Controllers\RaveController@withdraw')->name('withdraw');

Route::get('/',function(){
   return "What are u looking for  😠?"; 
});
// //USER
// Route::get('/', 'App\Http\Controllers\HomeController@index');
// Route::get('/about', 'App\Http\Controllers\HomeController@about');
// Route::get('/privacy_policy', 'App\Http\Controllers\HomeController@privacy');
// Route::get('/contactus', 'App\Http\Controllers\HomeController@contactus');
// Route::get('/restaurant', 'App\Http\Controllers\HomeController@restaurant');
// Route::get('/menu{id}','App\Http\Controllers\SalesController@list');
// Route::get('/food_ports', 'App\Http\Controllers\HomeController@port');
// Route::get('/cafe', 'App\Http\Controllers\HomeController@cafe');
// Route::get('/Healthy&Wellness', 'App\Http\Controllers\HomeController@wellness');
// Route::get('/foodcompany', 'App\Http\Controllers\HomeController@company');
// Route::get('/foodequipment', 'App\Http\Controllers\HomeController@equipment');
// Route::post('/cart{id}', 'App\Http\Controllers\CartController@addToCart');
// Route::delete('/cart/{id}', 'App\Http\Controllers\CartController@destroy_cart');
// Route::get('/cart', 'App\Http\Controllers\CartController@viewcart');
// Route::post('/add{id}', 'App\Http\Controllers\CartController@add');
// Route::post('/sub{id}', 'App\Http\Controllers\CartController@sub');
// Route::get('/checkout', 'App\Http\Controllers\HomeController@checkout');
// Route::post('/create_checkout/{id}', 'App\Http\Controllers\HomeController@update_user');
// Route::post('/pay', 'App\Http\Controllers\RaveController@initialize')->name('pay');
// Route::post('/payment', 'App\Http\Controllers\RaveController@payment')->name('payment');
// Route::get('/search','App\Http\Controllers\SalesController@search')->name('search');
// Route::post('/update/{id}', 'App\Http\Controllers\HomeController@update_profile');
// Route::get('/profile{id}', 'App\Http\Controllers\HomeController@profile');
// Route::post('/reviews', 'App\Http\Controllers\HomeController@reviews');
// Route::post('/send_mail', 'App\Http\Controllers\HomeController@password_reset')->name('send_mail');
// Route::get('/password_reset{id}', 'App\Http\Controllers\HomeController@reset_password');
// Route::post('/reset/{id}', 'App\Http\Controllers\HomeController@reset');
//         Route::get('/reg_user', [App\Http\Controllers\HomeController::class, 'create'])
//             ->middleware(['guest'])
//             ->name('reg_user');

//         Route::post('/reg_user', [App\Http\Controllers\HomeController::class, 'create'])
//             ->middleware(['guest']);
// Route::get('/login_with_otp', 'App\Http\Controllers\HomeController@otp_form');
// Route::post('/otp_login', 'App\Http\Controllers\HomeController@loginWithOtp');
// Route::post('/newsletter', 'App\Http\Controllers\HomeController@newsletter')->name('newsletter');

// Route::get('/linkstorage', function () {
//     Artisan::call('storage:link');
// });
// Route::post('/report', 'App\Http\Controllers\HomeController@report')->name('report');
// Route::post('logged_in','App\Http\Controllers\LoginController@authenticate')->name('logged_in');
// Route::get('/footer','App\Http\Controllers\LoginController@login');
// Route::post('/contact', 'App\Http\Controllers\HomeController@contact')->name('contact');


// //Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//   //  return view('dashboard');
// // })->name('dashboard');



Route::match(['get', 'post'], '/botman', 'App\Http\Controllers\BotmanController@handle');
