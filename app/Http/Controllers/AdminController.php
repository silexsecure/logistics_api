<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Admin2;
use App\Models\Vendor;
use App\Models\Categories;
use App\Models\Category;
use App\Models\User;
use DB;
use App\Models\Cart;
use App\Models\Sales;
use App\Models\Checkout;
use App\Models\Report;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Carbon\Carbon;
use App\Models\Cms;
use App\Models\Advert;
use App\Models\Chatbot;
use App\Models\Contact;
use Illuminate\Support\Facades\Hash;
class AdminController extends Controller
{
    public function show_admin_Form()
    {
        return view('admin.register');
    }

    protected function create_admin(Request $request)
    {  
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:250',
            'email' => 'required|string|email|max:250|unique:admin',
            'phone' => 'required|string|max:250',
            'password' => 'required|string|min:8|confirmed',
            'image' => 'image|nullable|max:1999'



        ]);

 //handle photo upload
 if($request->hasfile('image')){
    //get file name and extension
    $filenamewithext=$request->file('image')->getClientOriginalName();
    //get just file name
    $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);
    //get just extension
    $extension = $request -> file('image') -> getClientOriginalExtension();
    // filename to store
    $filenametostore= $filename. '_'.time().'.'.$extension;
    //upload image
    $path =$request->file('image')->storeAs('public/admin_image', $filenametostore);
} else{
    $filenametostore='noimage.jpg';
}

        $agents = Admin::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'password' =>Hash::make($request['password']),
            'image' =>$filenametostore,
           
        ]);
        return redirect('/login_admin');

    }

    public function showadminLoginForm()
    {
        return view('admin.login',['url' => 'admin']);
    }

    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect('dashboard');
                }
                return redirect('/login_admin')->with('status', 'Your credentials do not match our records');

    }


    public function index()
    {
      $admin = Auth::guard('admin')->user();
      if(!$admin) {
        return redirect('/login_admin');
    }else{
        $image= Auth::guard('admin')->user()->image;

       // $checkouts = Checkout::where('payment_status','1',)->get();
       $products = Sales::all();
       $vendors = Vendor::all();
       $users = User::where('email_verified_at','1')->get();
      $sold = Checkout::all();
      
      $latests= Checkout::paginate(5);
      $total=Checkout::where('accept','1')->sum('price');
      $last=Checkout::where('accept','1')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->sum('price');
      $present=Checkout::where('accept','1')->where('created_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->sum('price');
$best_vendor= Checkout::select('vendor_id')
    ->selectRaw('COUNT(*) AS count')
    ->groupBy('vendor_id')
    ->orderByDesc('count')
    ->first();
   
    if(!$best_vendor){
        $best_v=NULL;
    }else{
    $best_v=Vendor::where('id',$best_vendor->vendor_id)->first();
    }
    $best_food= Checkout::select('product_id')
    ->selectRaw('COUNT(*) AS count')
    ->groupBy('product_id')
    ->orderByDesc('count')
    ->first();
      if(!$best_food){
        $best_f=NULL;
    }else{
    $best_f=Sales::where('id',$best_food->product_id)->first();
    }
      //  $data = DB::table('product_orders')
      // ->select(
      //  DB::raw('amount as amount'),
      //  DB::raw('count(*) as number'))
      // ->groupBy('amount')
     //  ->get();
     //$array[] = ['Amount', 'Number'];
    // foreach($data as $key => $value)
    // {
     // $array[++$key] = [$value->amount, $value->number];
     //}
    }


      return view('admin.dashboard',compact(['vendors','users','products','sold','latests','total','last','present','best_v','best_f']));
    }



 public function orders()
    {
      $admin = Auth::guard('admin')->user();
      if(!$admin) {
        return redirect('/login_admin');
    }else{
       

      $agents= Checkout::where('reject','0')->orderby('id','desc')->paginate(10);
    }


      return view('admin.orders',compact(['agents']));
    }
    
    
    public function rejected_orders()
    {
      $admin = Auth::guard('admin')->user();
      if(!$admin) {
        return redirect('/login_admin');
    }else{
       

      $agents= Checkout::where('reject','1')->orderby('id','desc')->paginate(10);
    }


      return view('admin.rejected',compact(['agents']));
    }



 public function reg_vendor_Form()
    {
       
        $cats = DB::table('category')->get()->pluck('name','name')->toArray();

            return view('vendor.register', compact('cats'));
   
}


    public function show_vendor_Form()
    {
        $admin = Auth::guard('admin')->user();
        if(!$admin) {
          return redirect('/login_admin');
      }else{
        $cats = DB::table('category')->get()->pluck('name','name')->toArray();

            return view('admin.addvendor', compact('cats'));
    }
}

    protected function create_vendor(Request $request)
    {  
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:250|unique:vendor',
            'phone' => 'required|string|max:250',
            'address' => 'required|string|max:250',
            'state' => 'required|string|max:250',
            'city' => 'required|string|max:250',
            'company_name' => 'required|string|max:250',
            'password' => 'required|string|min:8|confirmed',
            'image' => 'image|nullable|max:1999'



        ]);




 //handle photo upload
 if($request->hasfile('image')){
    //get file name and extension
    $filenamewithext=$request->file('image')->getClientOriginalName();
    //get just file name
    $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);
    //get just extension
    $extension = $request -> file('image') -> getClientOriginalExtension();
    // filename to store
    $filenametostore= $filename. '_'.time().'.'.$extension;
    //upload image
    $path =$request->file('image')->storeAs('public/vendor_image', $filenametostore);
} else{
    $filenametostore='noimage.jpg';
}

        $agents = Vendor::create([
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'state' => $request->get('state'),
            'city' => $request->get('city'),
            'password' =>Hash::make($request['password']),
            'image' =>$filenametostore,
            'address' => $request->get('address'),
            'company_name' => $request->get('company_name'),
            'category' => $request->get('category'),
             'verified'=>0,
        ]);
        return redirect ('addvendor')->with('success', 'Vendor Added');
    }


    public function show(request $request)
    {   
        $admin = Auth::guard('admin')->user();
        if(!$admin) {
          return redirect('/login_admin');
      }else{    
        $agents = Vendor::where('verified','1')->orderby('id', 'desc')->paginate(6);
        return view ('admin.vendors')->with('agents', $agents);
    }
}



 public function show_log_Form()
    {
        $admin = Auth::guard('admin')->user();
        if(!$admin) {
          return redirect('/login_admin');
      }else{
       

            return view('admin.addlog');
    }
}

    protected function create_log(Request $request)
    {  
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:250|unique:vendor',
            'phone' => 'required|string|max:250',
            'name' => 'required|string|max:250',
            'password' => 'required|string|min:8|confirmed',
            'image' => 'image|nullable|max:1999'



        ]);




 //handle photo upload
 if($request->hasfile('image')){
    //get file name and extension
    $filenamewithext=$request->file('image')->getClientOriginalName();
    //get just file name
    $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);
    //get just extension
    $extension = $request -> file('image') -> getClientOriginalExtension();
    // filename to store
    $filenametostore= $filename. '_'.time().'.'.$extension;
    //upload image
    $path =$request->file('image')->storeAs('public/log_image', $filenametostore);
} else{
    $filenametostore='noimage.jpg';
}

        $agents = Admin2::create([
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'password' =>Hash::make($request['password']),
            'image' =>$filenametostore,
            'name' => $request->get('name'),
        ]);
        return redirect ('addvendor')->with('success', 'Company Added');
    }


    public function showLog(request $request)
    {   
        $admin = Auth::guard('admin')->user();
        if(!$admin) {
          return redirect('/login_admin');
      }else{    
        $agents = Admin2::paginate(6);
        return view ('admin.log')->with('agents', $agents);
    }
}


 public function show_requests(request $request)
    {   
        $admin = Auth::guard('admin')->user();
        if(!$admin) {
          return redirect('/login_admin');
      }else{    
        $agents = Vendor::where('verified','0')->orderby('id', 'desc')->paginate(6);
        return view ('admin.requests')->with('agents', $agents);
    }
}

  public function approve($id)
    {
        $agent= Vendor::find($id);
       $agent->verified=1;
       $agent->save();
        return redirect()->back()->with('success', 'Account Approved');

    }


    public function destroy($id)
    {
        $agent= Vendor::find($id);
        $agent->delete();
        return redirect()->back()->with('success', 'Vendor Deleted');

    }
    
    
     public function destroy_user($id)
    {
        $agent= User::find($id);
        $agent->delete();
        return redirect('users')->with('success', 'User Deleted');

    }
    
    
     public function destroy_transaction($id)
    {
        $agent= Checkout::find($id);
        $agent->delete();
        return redirect()->back()->with('success', 'Transaction Deleted');

    }
    
    
      public function updateshow($id)
    {
        $admin= Admin::find($id);
        
        return view('admin.profile')->with('admin', $admin);

    }
    
      public function update_admin(Request $request, $id)
    {
        
        if($request->hasfile('image')){
            //get file name and extension
            $filenamewithext=$request->file('image')->getClientOriginalName();
            //get just file name
            $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);
            //get just extension
            $extension = $request -> file('image') -> getClientOriginalExtension();
            // filename to store
            $filenametostore= $filename. '_'.time().'.'.$extension;
            //upload image
            $path =$request->file('image')->storeAs('public/admin_image', $filenametostore);
        } else{
            $filenametostore='noimage.jpg';
        }
       
        $admin =Admin::find($id);
        $admin->name = $request -> input('name');
        $admin->email = $request -> input('email');
        $admin->image=$filenametostore;
        $admin->percentageOnLogistics = $request -> input('percentageOnLogistics');
        $admin->percentageOnProduct = $request -> input('percentageOnProduct');
         $admin -> save();
 
          return redirect()->back()->with('success', 'Profile Updated');
    }


    public function update_vendor($id)
    {
        $admin = Auth::guard('admin')->user();
        if(!$admin) {
          return redirect('/login_admin');
      }else{
        $agent= Vendor::find($id);
        $cats = DB::table('category')->get()->pluck('name','name')->toArray();
        return view('admin.editvendor')->with('agent', $agent)->with('cats',$cats);

    }
}
   
   
       public function update_vendor2($id)
    {
        $admin = Auth::guard('admin')->user();
        if(!$admin) {
          return redirect('/login_admin');
      }else{
        $agent= Vendor::find($id);
        $agentid= $agent->id;
        $cats = DB::table('category2')->where('vendor_id',$agentid)->get()->pluck('name','name')->toArray();
         $latests= Checkout::where('vendor_id',$agentid)->paginate(5);
      $total=Checkout::where('vendor_id',$agentid)->where('accept','1')->sum('price');
      $last=Checkout::where('vendor_id',$agentid)->where('accept','1')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->sum('price');
      $present=Checkout::where('vendor_id',$agentid)->where('accept','1')->where('created_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->sum('price');
     $products= Sales::where('vendor_id',$agentid)->get();
     $sold=Checkout::where('vendor_id',$agentid)->where('accept','1')->get();
      $lastt=Checkout::where('vendor_id',$agentid)->where('accept','1')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->get();
      $current=Checkout::where('vendor_id',$agentid)->where('accept','1')->where('created_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->get();
          $report = Report:: where('company_id',$agentid)->get();    
       return view('admin.details',compact(['agent','cats','products','sold','latests','total','last','present','current','lastt','report']));
    }
}
   

 public function suspend($id)
    {
        $agent= Vendor::find($id);
       $agent->suspend=1;
       $agent->save();
        return redirect()->back()->with('success', 'Account Suspended');

    }
    public function activate($id)
    {
        $agent= Vendor::find($id);
       $agent->suspend=NULL;
       $agent->save();
        return redirect()->back()->with('success', 'Account Activated');

    }


    public function update(Request $request, $id)
    {
        if($request->hasfile('image')){
            //get file name and extension
            $filenamewithext=$request->file('image')->getClientOriginalName();
            //get just file name
            $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);
            //get just extension
            $extension = $request -> file('image') -> getClientOriginalExtension();
            // filename to store
            $filenametostore= $filename. '_'.time().'.'.$extension;
            //upload image
            $path =$request->file('image')->storeAs('public/vendor_image', $filenametostore);
        } else{
            $filenametostore='noimage.jpg';
        }

        $agent =Vendor::find($id);
         $agent->company_name = $request -> input('company_name');
        $agent->email = $request -> input('email');
        $agent ->phone =$request -> input('phone');
        $agent ->state =$request -> input('state');
        $agent ->city =$request -> input('city');
        $agent ->address =$request -> input('address');
        $agent ->category =$request -> input('category');
        $agent->image=$filenametostore;
         $agent -> save();
 
          return redirect()->back()->with('success', 'Agent Updated');
    }


    public function showcategory()
    {
        $admin = Auth::guard('admin')->user();
        if(!$admin) {
          return redirect('/login_admin');
      }else{
        return view('admin.addcategory');
    }
}
    protected function createcategory(Request $request)
    {  
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:250',
           



        ]);






        $categories = Categories::create([
            'name' => $request->get('name'),
            
        ]);
        return redirect ('category')->with('success', 'Category Added');
    }
    
    
     public function category(Request $request)
    {
         $admin = Auth::guard('admin')->user();
        if(!$admin) {
          return redirect('/login_admin');
      }else{
       
       $categories = Categories::orderby('id', 'desc')->paginate(10);
        return view ('admin.addcategory')->with('categories', $categories);
    }
    }

    public function destroycategory($id)
    {
        $category= Categories::find($id);
        $category->delete();
        return redirect('category')->with('success', 'Category Deleted');

    }

    public function show_users(request $request)
    {   
        $admin = Auth::guard('admin')->user();
        if(!$admin) {
          return redirect('/login_admin');
      }else{    
        $agents = User::where('email_verified_at','1')->orderby('id', 'desc')->paginate(10);
        return view ('admin.users')->with('agents', $agents);
    }
}

 public function reports(request $request)
    {   
        $admin = Auth::guard('admin')->user();
        if(!$admin) {
          return redirect('/login_admin');
      }else{    
        $agents = Report::orderby('id', 'desc')->paginate(10);
        return view ('admin.report')->with('agents', $agents);
    }
}

 public function destroy_report($id)
    {
        $category= Report::find($id);
        $category->delete();
        return redirect('reports')->with('success', 'Report Deleted');

    }

     public function send_email(Request $request,$id)
    {
        $user=Vendor::find($id);

$to = "$user->email"; 
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'EatNaija.com'; 
 
$subject = $request->get('subject'); 
 
$htmlContent = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style type="text/css">
a[x-apple-data-detectors] {color: inherit !important;}
</style>

</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 20px 0 30px 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
<tr>
<td align="center" bgcolor="#fd7e14" style="padding: 40px 0 30px 0;">
<img src="https://eatnaija.com/public/img/logo-white.png" alt="EatNaija Logo." width="200" height="150" style="display: block;" />
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif;">
<h1 style="font-size: 24px; margin: 0;">Hello '.$user->company_name.'!</h1>
</td>
</tr>
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
<p style="margin: 0;">'.$request->get('message').'.</p>
</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td bgcolor="#fd7e14" style="padding: 30px 30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
<p style="margin: 0;">©2020 EatNaija</p>
</td>
<tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>'; 
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers);
       
        
         return redirect()->back()->with('success','Email Sent');
    }
  
  
   public function bulk_email(Request $request)
    {
        $users=Vendor::all();
foreach($users as $user){
$to = "$user->email"; 
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'EatNaija.com'; 
 
$subject = $request->get('subject'); 
 
$htmlContent = '
   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style type="text/css">
a[x-apple-data-detectors] {color: inherit !important;}
</style>

</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 20px 0 30px 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
<tr>
<td align="center" bgcolor="#fd7e14" style="padding: 40px 0 30px 0;">
<img src="https://eatnaija.com/public/img/logo-white.png" alt="EatNaija Logo." width="200" height="150" style="display: block;" />
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif;">
<h1 style="font-size: 24px; margin: 0;">Hello '.$user->company_name.'!</h1>
</td>
</tr>
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
<p style="margin: 0;">'.$request->get('message').'.</p>
</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td bgcolor="#fd7e14" style="padding: 30px 30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
<p style="margin: 0;">©2020 EatNaija</p>
</td>
<tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>'; 
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers);
       
}   
         return redirect()->back()->with('success','Email Sent');
    }  


 public function send_email2(Request $request,$id)
    {
        $user=User::find($id);

$to = "$user->email"; 
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'EatNaija.com'; 
 
$subject = $request->get('subject'); 
 
$htmlContent = '
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style type="text/css">
a[x-apple-data-detectors] {color: inherit !important;}
</style>

</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 20px 0 30px 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
<tr>
<td align="center" bgcolor="#fd7e14" style="padding: 40px 0 30px 0;">
<img src="https://eatnaija.com/public/img/logo-white.png" alt="EatNaija Logo." width="200" height="150" style="display: block;" />
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif;">
<h1 style="font-size: 24px; margin: 0;">Hello '.$user->name.'!</h1>
</td>
</tr>
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
<p style="margin: 0;">'.$request->get('message').'.</p>
</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td bgcolor="#fd7e14" style="padding: 30px 30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
<p style="margin: 0;">©2020 EatNaija</p>
</td>
<tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>'; 
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers);
       
        
         return redirect()->back()->with('success','Email Sent');
    }
  
  
   public function bulk_email2(Request $request)
    {
        $users=User::all();
foreach($users as $user){
$to = "$user->email"; 
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'EatNaija.com'; 
 
$subject = $request->get('subject'); 
 
$htmlContent = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style type="text/css">
a[x-apple-data-detectors] {color: inherit !important;}
</style>

</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 20px 0 30px 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
<tr>
<td align="center" bgcolor="#fd7e14" style="padding: 40px 0 30px 0;">
<img src="https://eatnaija.com/public/img/logo-white.png" alt="EatNaija Logo." width="200" height="150" style="display: block;" />
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif;">
<h1 style="font-size: 24px; margin: 0;">Hello '.$user->company_name.'!</h1>
</td>
</tr>
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
<p style="margin: 0;">'.$request->get('message').'.</p>
</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td bgcolor="#fd7e14" style="padding: 30px 30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
<p style="margin: 0;">©2020 EatNaija</p>
</td>
<tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>'; 
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers);
       
}   
         return redirect()->back()->with('success','Email Sent');
    }  

public function upload_food(Request $request)
    {
       
       
       //handle photo upload
 if($request->hasfile('image')){
    //get file name and extension
    $filenamewithext=$request->file('image')->getClientOriginalName();
    //get just file name
    $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);
    //get just extension
    $extension = $request -> file('image') -> getClientOriginalExtension();
    // filename to store
    $filenametostore= $filename. '_'.time().'.'.$extension;
    //upload image
    $path =$request->file('image')->storeAs('public/upload_image', $filenametostore);
} else{
    $filenametostore='noimage.jpg';
}
        
    $sale = new Sales;
    $sale ->name = $request->input('name');
    $sale ->category = $request->input('category');
    $sale ->price = $request->input('price');
    $sale ->promo_price = $request->input('promo_price');
    $sale ->vendor_name =$request->input('vendor_name');
    $sale ->description = $request->input('description');
    $sale ->vendor_id = $request->input('vendor_id');
    $sale ->main_category = $request->input('main_category');
    $sale ->location = $request->input('location');
    $sale->image= $filenametostore;
    $sale-> save();


       return redirect()->back()->with('success','Menu Uploaded');
          
    }

 protected function create_category(Request $request)
    { 
   
 $category = Category::create([
            'name' => $request->get('name'),
            'vendor_id'=>$request->get('vendor_id'),
            
        ]);
     return redirect()->back()->with('success','Category Created');
    }

protected function create_cms(Request $request)
    { 
   
        $body = $request->body;
        DB::table("cms")->where(["id"=>1])->update([
            "body"=>$body
            ]);
            
            return response()->json(["message"=>"SuccessFully Updated!"]);
    }
 
 public function cms(Request $request)
    {
       
       $category = DB::table("cms")->where(["id"=>1])->first();
        return view ('admin.cms')->with('category', $category);
    }
    
    protected function create_policy(Request $request)
    { 
   
        $body = $request->body;
         $header = $request->header;
        DB::table("policy")->where(["id"=>1])->update([
            "body"=>$body,
             "header"=>$header
            ]);
            
            return response()->json(["message"=>"SuccessFully Updated!"]);
    }
 
 public function policy(Request $request)
    {
       
       $category = DB::table("policy")->where(["id"=>1])->first();
        return view ('admin.policy')->with('category', $category);
    }
    
    
      public function password_reset2(Request $request)
    {
      $email = $request->input('email');
      
       $check=Admin::where('email',$email)->first();
if(!$check) {
   return redirect()->back()->with('success','User not found') ;
}
else {
    $user = Admin::find($check->id);
        $to = "$email"; 
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'EatNaija.com'; 
 
$subject = "Password Reset"; 
 
$htmlContent = '
     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style type="text/css">
a[x-apple-data-detectors] {color: inherit !important;}
</style>

</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 20px 0 30px 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
<tr>
<td align="center" bgcolor="#fd7e14" style="padding: 40px 0 30px 0;">
<img src="https://eatnaija.com/public/img/logo-white.png" alt="EatNaija Logo." width="200" height="150" style="display: block;" />
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif;">
<h1 style="font-size: 24px; margin: 0;">Hello '.$user->name.'!</h1>
</td>
</tr>
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
<p style="margin: 0;">Follow this link to reset your password <a href="https://eatnaija.com/reset_password'.$user->id.'">Reset Password</a></p>
</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td bgcolor="#fd7e14" style="padding: 30px 30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
<p style="margin: 0;">©2020 EatNaija</p>
</td>
<tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>'; 
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers); 
 
          return redirect()->back()->with('success','Email Sent');
    }
    }
    
 public function reset_password2($id)
    {
        
      $user = Admin::find($id);
     return view('admin.reset')->with('user',$user);
   
    }
    
    
    public function reset2(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:8|confirmed',
          ]);
 
        $user = Admin::find($id);
        $user ->password =Hash::make($request['password']);        
        $user -> save();
 
          return redirect('/login_admin')->with('error','Password Updated');
    }
    
    
      public function advert_show()
    {
      $categories = Advert::orderby('id', 'desc')->paginate(10);
        return view ('admin.advert')->with('categories', $categories);
    }

    protected function create_advert(Request $request)
    {  
 //handle photo upload
 if($request->hasfile('image')){
    //get file name and extension
    $filenamewithext=$request->file('image')->getClientOriginalName();
    //get just file name
    $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);
    //get just extension
    $extension = $request -> file('image') -> getClientOriginalExtension();
    // filename to store
    $filenametostore= $filename. '_'.time().'.'.$extension;
    //upload image
    $path =$request->file('image')->storeAs('public/adverts', $filenametostore);
} else{
    $filenametostore='noimage.jpg';
}

        $agents = Advert::create([
            
            'image' =>$filenametostore,
           
        ]);
        return redirect()->back()->with('success','Advert Uploaded');

    }

 public function destroyadvert($id)
    {
        $category= Advert::find($id);
        $category->delete();
        return redirect()->back()->with('success','Advert Deleted');

    }
    
    
        public function showchatbot()
    {
        $admin = Auth::guard('admin')->user();
        if(!$admin) {
          return redirect('/login_admin');
      }else{
           $cats = DB::table('category')->get()->pluck('name','name')->toArray();
        return view('admin.chatbot',compact('cats'));
    }
}
    protected function createchatbot(Request $request)
    {  




        $categories = Chatbot::create([
            'name' => $request->get('name'),
             'category' => $request->get('category'),
            
        ]);
        return redirect()->back()->with('success', 'Response Added');
    }
    
    
     public function chatbot(Request $request)
    {
     $admin = Auth::guard('admin')->user();
        if(!$admin) {
          return redirect('/login_admin');
      }else{
       $categories = Chatbot::orderby('id', 'desc')->paginate(10);
                  $cats = DB::table('category')->get()->pluck('name','name')->toArray();
        return view('admin.chatbot',compact('cats','categories'));
    }
    }

    public function destroychatbot($id)
    {
        $category= Chatbot::find($id);
        $category->delete();
        return redirect()->back()->with('success', 'Response Deleted');

    }
    
     public function show_contact(request $request)
    {   
        $admin = Auth::guard('admin')->user();
        if(!$admin) {
          return redirect('/login_admin');
      }else{    
        $agents = Contact::orderby('id', 'desc')->paginate(10);
        return view ('admin.contact')->with('agents', $agents);
    }
}


 public function send_email3(Request $request,$id)
    {
       
$email = $request->get('email'); 

$to = "$email"; 
$from = 'eatnusyn@eatnaija.com'; 
$fromName = 'EatNaija.com'; 
 
$subject = $request->get('subject'); 
 
$htmlContent = '
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style type="text/css">
a[x-apple-data-detectors] {color: inherit !important;}
</style>

</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 20px 0 30px 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
<tr>
<td align="center" bgcolor="#fd7e14" style="padding: 40px 0 30px 0;">
<img src="https://eatnaija.com/public/img/logo-white.png" alt="EatNaija Logo." width="200" height="150" style="display: block;" />
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif;">
<h1 style="font-size: 24px; margin: 0;">Hello '.$request->get('name').'!</h1>
</td>
</tr>
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
<p style="margin: 0;">'.$request->get('message').'.</p>
</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td bgcolor="#fd7e14" style="padding: 30px 30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
<p style="margin: 0;">©2020 EatNaija</p>
</td>
<tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>'; 
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers);
       
        
         return redirect()->back()->with('success','Email Sent');
    }
  

public function destroy_contact($id)
    {
        $category= Contact::find($id);
        $category->delete();
        return redirect()->back()->with('success', 'Deleted');

    }




}
