<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Rave;
use Illuminate\Support\Facades\Auth;
use App\Models\Checkout;
use App\Models\Cart;
use App\Models\Data;
use App\Models\Vendor;
use App\Models\Maincart;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Carbon\Carbon;
class RaveController extends Controller
{
    public function initialize(){
        //This initializes payment and redirects to the payment gateway//The initialize method takes the parameter of the redirect URL
        Rave::initialize(route('callback'));
   

    }
    /**
     * Obtain Rave callback information
     * @return void
     */public function payment(Request $request){
        // view the data response
    
   
            $user = Auth::User();
            $userid=$user->id;
              $parent= Maincart::where('user_id',$userid)->orderby('id','desc')->first();
               $parentid= $parent->id;
            $maincarts = Cart::where('user_id',$userid)->where('parent_id',$parentid)->get();
             foreach($maincarts as $maincart){
            $main = $maincart->parent_id;

            
           
             }
   
   
            
            foreach($maincarts as $maincart){
                $vat = 10;
                $price=$maincart->price;
               $vatTopay = ($price / 100) * $vat;
               $totalprice=$price - $vatTopay ;
                  Checkout::create([
    
                'name' => Auth::user()->name,
                'phone' => Auth::user()->phone,
                'address1'=>Auth::user()->address,
                'address2'=>Auth::user()->address2,
                'city'=>Auth::user()->city,
                'state'=>Auth::user()->state,
                'user_id'=> $userid,
                'payment_status'=> 1,
                'product_id' => $maincart->product_id,
                'cart_id'  => $maincart->parent_id,
                'product'  => $maincart->product_name,
                'quantity'  => $maincart->quantity,
                'price'  => $totalprice,
                'vendor_id'  => $maincart->vendor_id,
                'image' => $maincart->image,
                'accept' => 0,
                'reject' => 0,
                'delivered' => 0,
                'order_no'=>$request->number,
                'total'=>$request->amount,
            ]);
        
            
             $maincart->delete();
             
             $details=Vendor::where('id',$maincart->vendor_id)->orderby('id','desc')->first();
              Data::create([
                  'name'=>Auth::user()->name,
                  'phone'=>Auth::user()->phone,
                  'email'=>Auth::user()->email,
                  'pickup'=>$details->company_name,
                  'depart'=>$details->address,
                  'currentadd'=>Auth::user()->address,
                  'cost'=>$request->delivery,
                  'delivery_code'=>$request->number,
                  'pending'=>1,
            
                 ]); 
     $to = "$user->email"; 
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'EatNaija.com'; 
 
$subject = "Receipt of Transaction"; 
 
$htmlContent = '
   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="x-apple-disable-message-reformatting" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="color-scheme" content="light dark" />
    <meta name="supported-color-schemes" content="light dark" />
    <title></title>
    <style type="text/css" rel="stylesheet" media="all">
    /* Base ------------------------------ */
    
    @import url("https://fonts.googleapis.com/css?family=Nunito+Sans:400,700&display=swap");
    body {
      width: 100% !important;
      height: 100%;
      margin: 0;
      -webkit-text-size-adjust: none;
    }
    
    a {
      color: #3869D4;
    }
    
    a img {
      border: none;
    }
    
    td {
      word-break: break-word;
    }
    
    .preheader {
      display: none !important;
      visibility: hidden;
      mso-hide: all;
      font-size: 1px;
      line-height: 1px;
      max-height: 0;
      max-width: 0;
      opacity: 0;
      overflow: hidden;
    }
    /* Type ------------------------------ */
    
    body,
    td,
    th {
      font-family: "Nunito Sans", Helvetica, Arial, sans-serif;
    }
    
    h1 {
      margin-top: 0;
      color: #333333;
      font-size: 22px;
      font-weight: bold;
      text-align: left;
    }
    
    h2 {
      margin-top: 0;
      color: #333333;
      font-size: 16px;
      font-weight: bold;
      text-align: left;
    }
    
    h3 {
      margin-top: 0;
      color: #333333;
      font-size: 14px;
      font-weight: bold;
      text-align: left;
    }
    
    td,
    th {
      font-size: 16px;
    }
    
    p,
    ul,
    ol,
    blockquote {
      margin: .4em 0 1.1875em;
      font-size: 16px;
      line-height: 1.625;
    }
    
    p.sub {
      font-size: 13px;
    }
    /* Utilities ------------------------------ */
    
    .align-right {
      text-align: right;
    }
    
    .align-left {
      text-align: left;
    }
    
    .align-center {
      text-align: center;
    }
    /* Buttons ------------------------------ */
    
    .button {
      background-color: #3869D4;
      border-top: 10px solid #3869D4;
      border-right: 18px solid #3869D4;
      border-bottom: 10px solid #3869D4;
      border-left: 18px solid #3869D4;
      display: inline-block;
      color: #FFF;
      text-decoration: none;
      border-radius: 3px;
      box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
      -webkit-text-size-adjust: none;
      box-sizing: border-box;
    }
    
    .button--green {
      background-color: #22BC66;
      border-top: 10px solid #22BC66;
      border-right: 18px solid #22BC66;
      border-bottom: 10px solid #22BC66;
      border-left: 18px solid #22BC66;
    }
    
    .button--red {
      background-color: #FF6136;
      border-top: 10px solid #FF6136;
      border-right: 18px solid #FF6136;
      border-bottom: 10px solid #FF6136;
      border-left: 18px solid #FF6136;
    }
    
    @media only screen and (max-width: 500px) {
      .button {
        width: 100% !important;
        text-align: center !important;
      }
    }
    /* Attribute list ------------------------------ */
    
    .attributes {
      margin: 0 0 21px;
    }
    
    .attributes_content {
      background-color: #F4F4F7;
      padding: 16px;
    }
    
    .attributes_item {
      padding: 0;
    }
    /* Related Items ------------------------------ */
    
    .related {
      width: 100%;
      margin: 0;
      padding: 25px 0 0 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    
    .related_item {
      padding: 10px 0;
      color: #CBCCCF;
      font-size: 15px;
      line-height: 18px;
    }
    
    .related_item-title {
      display: block;
      margin: .5em 0 0;
    }
    
    .related_item-thumb {
      display: block;
      padding-bottom: 10px;
    }
    
    .related_heading {
      border-top: 1px solid #CBCCCF;
      text-align: center;
      padding: 25px 0 10px;
    }
    /* Discount Code ------------------------------ */
    
    .discount {
      width: 100%;
      margin: 0;
      padding: 24px;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #F4F4F7;
      border: 2px dashed #CBCCCF;
    }
    
    .discount_heading {
      text-align: center;
    }
    
    .discount_body {
      text-align: center;
      font-size: 15px;
    }
    /* Social Icons ------------------------------ */
    
    .social {
      width: auto;
    }
    
    .social td {
      padding: 0;
      width: auto;
    }
    
    .social_icon {
      height: 20px;
      margin: 0 8px 10px 8px;
      padding: 0;
    }
    /* Data table ------------------------------ */
    
    .purchase {
      width: 100%;
      margin: 0;
      padding: 35px 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    
    .purchase_content {
      width: 100%;
      margin: 0;
      padding: 25px 0 0 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    
    .purchase_item {
      padding: 10px 0;
      color: #51545E;
      font-size: 15px;
      line-height: 18px;
    }
    
    .purchase_heading {
      padding-bottom: 8px;
      border-bottom: 1px solid #EAEAEC;
    }
    
    .purchase_heading p {
      margin: 0;
      color: #85878E;
      font-size: 12px;
    }
    
    .purchase_footer {
      padding-top: 15px;
      border-top: 1px solid #EAEAEC;
    }
    
    .purchase_total {
      margin: 0;
      text-align: right;
      font-weight: bold;
      color: #333333;
    }
    
    .purchase_total--label {
      padding: 0 15px 0 0;
    }
    
    body {
      background-color: #F4F4F7;
      color: #51545E;
    }
    
    p {
      color: #51545E;
    }
    
    p.sub {
      color: #6B6E76;
    }
    
    .email-wrapper {
      width: 100%;
      margin: 0;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #F4F4F7;
    }
    
    .email-content {
      width: 100%;
      margin: 0;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    /* Masthead ----------------------- */
    
    .email-masthead {
      padding: 25px 0;
      text-align: center;
    }
    
    .email-masthead_logo {
      width: 94px;
    }
    
    .email-masthead_name {
      font-size: 16px;
      font-weight: bold;
      color: #A8AAAF;
      text-decoration: none;
      text-shadow: 0 1px 0 white;
    }
    /* Body ------------------------------ */
    
    .email-body {
      width: 100%;
      margin: 0;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #FFFFFF;
    }
    
    .email-body_inner {
      width: 570px;
      margin: 0 auto;
      padding: 0;
      -premailer-width: 570px;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #FFFFFF;
    }
    
    .email-footer {
      width: 570px;
      margin: 0 auto;
      padding: 0;
      -premailer-width: 570px;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      text-align: center;
    }
    
    .email-footer p {
      color: #6B6E76;
    }
    
    .body-action {
      width: 100%;
      margin: 30px auto;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      text-align: center;
    }
    
    .body-sub {
      margin-top: 25px;
      padding-top: 25px;
      border-top: 1px solid #EAEAEC;
    }
    
    .content-cell {
      padding: 35px;
    }
    /*Media Queries ------------------------------ */
    
    @media only screen and (max-width: 600px) {
      .email-body_inner,
      .email-footer {
        width: 100% !important;
      }
    }
    
    @media (prefers-color-scheme: dark) {
      body,
      .email-body,
      .email-body_inner,
      .email-content,
      .email-wrapper,
      .email-masthead,
      .email-footer {
        background-color: #333333 !important;
        color: #FFF !important;
      }
      p,
      ul,
      ol,
      blockquote,
      h1,
      h2,
      h3 {
        color: #FFF !important;
      }
      .attributes_content,
      .discount {
        background-color: #222 !important;
      }
      .email-masthead_name {
        text-shadow: none !important;
      }
    }
    
    :root {
      color-scheme: light dark;
      supported-color-schemes: light dark;
    }
    </style>
    <!--[if mso]>
    <style type="text/css">
      .f-fallback  {
        font-family: Arial, sans-serif;
      }
    </style>
  <![endif]-->
  </head>
  <body>
    <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0" role="presentation">
      <tr>
        <td align="center">
          <table class="email-content" width="100%" cellpadding="0" cellspacing="0" role="presentation">
            <tr>
              <td class="email-masthead" valign="middle" style="padding: 40px 0 30px 0;">
               <img src="https://eatnaija.com/public/img/Eat-naija.png" alt="EatNaija Logo." class="f-fallback email-masthead_name" width="200" height="150" style="display: block;" />

              </td>
            </tr>
            <!-- Email Body -->
            <tr>
              <td class="email-body" width="100%" cellpadding="0" cellspacing="0">
                <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                  <!-- Body content -->
                  <tr>
                    <td class="content-cell">
                      <div class="f-fallback">
                        <h1>Hi '.$user->name.',</h1>
                        <p>Thanks for choosing EatNaija. This email is the receipt for your purchase.</p>
                        <!-- Discount -->
                        <table class="purchase" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                          <tr>
                            
                             
                            
                          </tr>
                          <tr>
                            <td colspan="2">
                              <table class="purchase_content" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                  <th class="purchase_heading" align="left">
                                    <p class="f-fallback">Product Name</p>
                                  </th>
                                  <th class="purchase_heading" align="right">
                                    <p class="f-fallback">Amount</p>
                                  </th>
                                </tr> ';
                
               foreach($maincarts as $maincart){
                    
                         
                           
                          
   
                       $htmlContent .= '    
                               <tr>
                                  <td width="60%" class="purchase_item"><span class="f-fallback">'.$maincart->product_name.'</span></td>
                                  <td class="align-right" width="40%" class="purchase_item"><span class="f-fallback">₦'.number_format($maincart->price, 1).'</span></td>
                                </tr> ';
                              } 
     $htmlContent .= '          <tr>
                                  <td width="60%" class="purchase_footer" valign="middle">
                                    <p class="f-fallback purchase_total purchase_total--label">Total</p>
                                  </td>
                                  <td width="40%" class="purchase_footer" valign="middle">
                                    <p class="f-fallback purchase_total">₦'.number_format($request->amount, 1).'</p>
                                  </td>
                                </tr>
                                 <tr>
                                  <td width="60%" class="purchase_footer" valign="middle">
                                    <p class="f-fallback purchase_total purchase_total--label">Order ID</p>
                                  </td>
                                  <td width="40%" class="purchase_footer" valign="middle">
                                    <p class="f-fallback purchase_total">'.$request->number.'</p>
                                  </td>
                                </tr>
                                
                              </table>
                            </td>
                          </tr>
                        </table>
                        <p>Cheers,
                          <br>The EatNaija Team</p>
                        <!-- Action -->
                       
                       
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                  <tr>
                    <td class="content-cell" align="center">
                      <p class="f-fallback sub align-center">©2020 EatNaija</p>
                     
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>';  
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers); 
            }
         
           return response()->json(['status'=>'success','message'=>'Payment Successful']);
    //do something to your database
   

    }
    
    
    public function withdraw(Request $request)
{
    $vendors=Vendor::where('verified','1')->get();
    foreach($vendors as $vendor){
                $today=Checkout::where('vendor_id',$vendor->id)->where('created_at', '>=',  Carbon::now()->subDays(1))->sum('price');
if($vendor->bank_code AND $today > 0){
            $acc = $vendor->bank_code;
            $number = $vendor->account;
            $amount = $today;
            $naration = "Payment for Today's Sales on Eatnaija";
            $currency = "NGN";
            $callback_url = "https://eatnaija.com/vendors";
            $debit_currency = "NGN";
            $randomNum=substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyz"), 0, 11);

        $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.flutterwave.com/v3/transfers",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>"{\n    \"account_bank\": \"$acc\",\n    \"account_number\": \" $number\",\n    \"amount\": $amount,\n    \"narration\": \" $naration\",\n    \"currency\": \"$currency\",\n    \"reference\": \"  $randomNum\",\n    \"callback_url\": \"$callback_url\",\n    \"debit_currency\": \" $debit_currency\"\n}",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json",
    "Authorization: Bearer FLWSECK-13ec888e9d9f6fee8c3ca805118e3e1e-X"
  ),
));

$response = curl_exec($curl);

//curl_close($curl);
dd($response);
//$obj = json_decode($response,true);
//if ($obj['status'] == 'successful') {
   
   
   
//do something to your database
}
}
//else {
//return redirect('payments')->with('error','Withdrawal not Successful');
//}
}
    
}
