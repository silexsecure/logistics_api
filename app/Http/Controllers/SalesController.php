<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Vendor;
use App\Models\Categories;
use App\Models\User;
use App\Models\Sales;
use App\Models\Cart;
use App\Models\Comment;

use DB;

class SalesController extends Controller
{
    public function uploadform()
    {
     $cats = DB::table('category2')->get()->pluck('name','name')->toArray();

            return view('vendor.upload', compact('cats'));

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required' ,
            'category'=>'required' ,
            'price' => 'required' ,
            'promo_price' => 'required' ,
            'discount' => 'required' ,
            'description' => 'required',
            'location' =>'required',
            'images'=>'required',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
    //handle photo upload
    if($request->hasfile('image')){
        //get file name and extension
        $filenamewithext=$request->file('image')->getClientOriginalName();
        //get just file name
        $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);
        //get just extension
        $extension = $request -> file('image') -> getClientOriginalExtension();
        // filename to store
        $filenametostore= $filename. '_'.time().'.'.$extension;
        //upload image
        $path =$request->file('image')->storeAs('public/upload_image', $filenametostore);
    } else{
        $filenametostore='noimage.jpg';
    }
$vendor=Auth::guard('vendor')->user();
    $sale = new Sales;
    $sale ->name = $request->input('name');
    $sale ->category = $request->input('category');
    $sale ->price = $request->input('price');
    $sale ->promo_price = $request->input('promo_price');
    $sale ->vendor_name =$vendor->company_name ;
    $sale ->description = $request->input('description');
    $sale ->vendor_id = $vendor->id;
    $sale ->main_category = $vendor->category;
    $sale ->location = $request->input('location');
    $sale->image=$filenametostore;
    $sale-> save();


      return redirect('upload_menu')->with('success','Menu Uploaded Successfully');
          
    }


    public function list($id)
    {
        $user = Auth::user();
        
        if(!$user){
            $userid=0;
        }else{
            $userid=$user->id;
        } 
       
        $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentid) {
           $cart = NULL;   
            
           $cartnum = "";
         if(!$cartnum){
             $cart=0;
             }else{
                  $cartnum = $cart->count();
                   }  
        $vendor=Vendor::find($id);
        $ratings=Comment::where('product_id',$id)->get();
       $vendors = Sales::where('vendor_id',$id)->orderby('id', 'desc')->paginate(10);
        return view('list', compact('vendors','vendor','cartnum','ratings'));
    }else {
        $part = $parentid->parent_id;
      $cart = Cart::where('parent_id',$part)->get();
        $cartnum = $cart->count();
        if(!$cartnum){
           $cart=0;
           }else{
              $cartnum = $cart->count();
                } 
                $vendor=Vendor::find($id);
                $ratings=Comment::where('product_id',$id)->get();
                $vendors = Sales::where('vendor_id',$id)->orderby('id', 'desc')->paginate(10);
                 return view('list', compact('vendors','vendor','cartnum','ratings'));
             }
    }
    
    
    
     public function search(Request $request)
    {
        $user = Auth::user();
        
        if(!$user){
            $userid=0;
        }else{
            $userid=$user->id;
        } 
       
        $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentid) {
           $cart = NULL;   
            
           $cartnum = "";
         if(!$cartnum){
             $cart=0;
             }else{
                  $cartnum = $cart->count();
                   }  
   $item = \Request::get('item');  
   $location = \Request::get('location');  

   
        $vendors = Sales::where('name', 'like', '%'.$item.'%')->where('location', 'like', '%'.$location.'%')->orderby('id', 'desc')->paginate(5);
        
        return view('search', compact('vendors','cartnum'));
    }else {
        $part = $parentid->parent_id;
      $cart = Cart::where('parent_id',$part)->get();
        $cartnum = $cart->count();
        if(!$cartnum){
           $cart=0;
           }else{
              $cartnum = $cart->count();
                } 
               $item = \Request::get('item');  
   $location = \Request::get('location');  

   
        $vendors = Sales::where('name', 'like', '%'.$item.'%')->where('location', 'like', '%'.$location.'%')->orderby('id', 'desc')->paginate(5);
                 return view('search', compact('vendors','cartnum'));
             }
    }
    
    
}
