<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Vendor;
use App\Models\Categories;
use App\Models\Checkout;
use App\Models\Logistics;
use App\Models\Category;
use App\Models\User;
use App\Models\Userapp;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Models\Maincart;
use DB;
use App\Models\Sales;
use Config;
use App\Models\Cart;
use JWTAuth;
use App\Models\Report;
use App\Models\Data;

use Tymon\JWTAuth\Exceptions\JWTException;

class UsersController extends Controller
{
    
   public function __construct() {
       
        $this->vendors = new Userapp;
    }

    public function authenticate(Request $request)
        {
            
          \Config::set('jwt.user', 'App\Models\Userapp'); 
		\Config::set('auth.providers.users.model', \App\Models\Userapp::class);
           
      
            $credentials = $request->only('email', 'password');
 
            try {
                
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['status'=>'false','error' => 'invalid_credentials'], 400);
                }
            } catch (JWTException $e) {
                return response()->json(['status'=>'false','error' => 'could_not_create_token'], 500);
            }
            $user = JWTAuth::user();
             $userid=$user->id;
        $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentid) {
            $cart = 0;
             
        }
         else {
        $part = $parentid->parent_id;
       $cart = Cart::where('parent_id',$part)->count();
        }
                    return response()->json(['status'=>'true','message'=>'Login Successful','user'=> $user,'cart'=>$cart,'token'=> $token]);

        }

 protected function create_user(Request $request)
    {  
         
          \Config::set('jwt.user', 'App\Models\Userapp'); 
		\Config::set('auth.providers.users.model', \App\Models\Userapp::class);
 


 if($request->hasfile('image')){
    //get file name and extension
    $filenamewithext=$request->file('image')->getClientOriginalName();
    //get just file name
    $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);
    //get just extension
    $extension = $request -> file('image') -> getClientOriginalExtension();
    // filename to store
    $filenametostore= $filename. '_'.time().'.'.$extension;
    //upload image
    $path =$request->file('image')->storeAs('public/user_image', $filenametostore);
} else{
    $filenametostore='noimage.jpg';
}
$otp = rand(1000,9999);
       
        
              $check1 = DB::table('users')->where('email',$request->get('email'))->first();
              $check2 = DB::table('users')->where('phone',$request->get('phone'))->first();
          if($check1){
              return response()->json([
                  'status'=> false,
                  'message'=> 'User already exists..'
              ],409);
          }
elseif($check2){
              return response()->json([
                  'status'=> false,
                  'message'=> 'User already exists..'
              ],409);
          }
else{
        
        $user = Userapp::create([
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'state' => $request->get('state'),
            'password' =>Hash::make($request['password']),
            'name' => $request->get('name'),
            'otp' => $otp,
        ]); 
        
         $to = "$user->email"; 
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'gapaautoparts.com'; 
 
$subject = "Welcome to GapaNaija"; 
 
$htmlContent = '
    
     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style type="text/css">
a[x-apple-data-detectors] {color: inherit !important;}
</style>

</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 20px 0 30px 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
<tr>
<td align="center" bgcolor="#fd7e14" style="padding: 40px 0 30px 0;">
<img src="https://logistic.gapaautoparts.com/public/img/Gapa_new_1.png" alt="Gapanaija Logo." width="200" height="150" style="display: block;" />
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif;">
<h1 style="font-size: 24px; margin: 0;">Hello '.$user->name.'!</h1>
<h3>Welcome to Gapanaija </h3>
</td>
</tr>
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
<p style="margin: 0;">Your OTP is '.$user->otp.'</p>
<p>We lead you to places, reveal dishes we think you will like to visit and taste</p>
<p>Thanks for choosing GapaNaija</p>
</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td bgcolor="#fd7e14" style="padding: 30px 30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
<p style="margin: 0;">©2021 GapaNaija</p>
</td>
<tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>
    
    '; 
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers);
        $cart=  Maincart::create([
            'user_id' => $user->id,
        ]);
        
        
        
       return response()->json(['status'=>'true','otp'=>$otp, 'userid'=>$user->id ,'message'=>'Registration Successful']);
    }
    }
    
    
    
    
     protected function gmail_create_user(Request $request)
    {  
         
          \Config::set('jwt.user', 'App\Models\Userapp'); 
		\Config::set('auth.providers.users.model', \App\Models\Userapp::class);
 


  $password=12345678;
  $email= $request->get('email');
        
              $check1 = DB::table('users')->where('email',$request->get('email'))->first();
          if($check1){
               $credentials =[
                 'email'=>  $email,
                  'password'=> $password
                             ] ;
 
          
                
                $token = JWTAuth::attempt($credentials);
               
               $user = JWTAuth::user();
                $userid=$user->id;
        $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentid) {
            $cart = 0;
             
        }
         else {
        $part = $parentid->parent_id;
       $cart = Cart::where('parent_id',$part)->count();
        }
              return response()->json([
                  'status'=> true,
                  'user'=> $user,
                  'cart'=>(int)$cart,
                  'token'=>$token
                 
              ],200);
          }
else{
        
        $user = Userapp::create([
            'email' => $request->get('email'),
            'password' =>Hash::make(12345678),
            'name' => $request->get('name'),
            'image' => $request->get('image'),
        ]); 
         $credentials =[
                 'email'=>  $request->get('email'),
                  'password'=> $password
                             ] ;
 
          
                
                $token = JWTAuth::attempt($credentials);
               
               $userr = JWTAuth::user();
         $to = "$user->email"; 
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'gapaautoparts.com'; 
 
$subject = "Welcome to GapaNaija"; 
 
$htmlContent = '
    
     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style type="text/css">
a[x-apple-data-detectors] {color: inherit !important;}
</style>

</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 20px 0 30px 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
<tr>
<td align="center" bgcolor="#fd7e14" style="padding: 40px 0 30px 0;">
<img src="https://logistic.gapaautoparts.com/public/img/Gapa_new_1.png" alt="Gapanaija Logo." width="200" height="150" style="display: block;" />
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif;">
<h1 style="font-size: 24px; margin: 0;">Hello '.$user->name.'!</h1>
<h3>Welcome to GapaNaija </h3>
</td>
</tr>
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
<p style="margin: 0;">Welcome to Gapanaija</p>
<p>We lead you to places, reveal dishes we think you will like to visit and taste</p>
<p>Thanks for choosing GapaNaija</p>
</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td bgcolor="#fd7e14" style="padding: 30px 30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
<p style="margin: 0;">©2020 GapaNaija</p>
</td>
<tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>
    
    '; 
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers);
        $cart=  Maincart::create([
            'user_id' => $user->id,
        ]);
        
         $userid=$userr->id;
        $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentid) {
            $cart = 0;
             
        }
         else {
        $part = $parentid->parent_id;
       $cart = Cart::where('parent_id',$part)->count();
        }
        
       return response()->json(['status'=>'true', 'user'=>$userr,'cart'=>(int)$cart, 'token'=>$token,'message'=>'Registration Successful']);
    }
    }
    
     public function loginWithOtp(Request $request)
  {
      
       \Config::set('jwt.user', 'App\Models\Userapp'); 
		\Config::set('auth.providers.users.model', \App\Models\Userapp::class);

      
      $otp= $request->only('otp');
      $check = DB::table('users')->where('otp',$otp)->first();
     

      if(!is_null($check)){
          $user = Userapp::find($check->id);

          if($user->email_verified_at == 1){
              return response()->json([
                  'status'=> false,
                  'message'=> 'Account already verified..'
              ],409);
          }
else{
          $user->update(['isverified' => 1]);
          $token = JWTAuth::fromUser($user);
          return response()->json(['message'=>'Verification Successful','user'=> $user,'token'=> $token]);
          
      }
    }
      return response()->json(['status'=> false, 'message'=> "Verification code is invalid."],409);
    
  }
  
  public function changePassword(Request $request){
    
    
       \Config::set('jwt.user', 'App\Models\Userapp'); 
		\Config::set('auth.providers.users.model', \App\Models\Userapp::class);
      $request->get('currentPassword');
      $request->get('newPassword');
    $userr = JWTAuth::parseToken()->authenticate();


        if (!(Hash::check($request->get('currentPassword'), $userr->password))) {
            // The passwords matches
            return response()->json(['status'=>'false','message'=>'Your current password does not match with the password you provided. Please try again.']);
        }

        if(strcmp($request->get('currentPassword'), $request->get('newPassword')) == 0){
            //Current password and new password are same
             return response()->json(['status'=>'false','message'=>'New Password cannot be same as your current password. Please choose a different password.']);
        }


        //Change Password
        $user = JWTAuth::user();
        $user->password =Hash::make($request->get('newPassword'));
        $user->save();

                     return response()->json(['status'=>'true','message'=>'Password changed successfully !']);

    }
    
    public function profile(Request $request){
    
    
       \Config::set('jwt.user', 'App\Models\Userapp'); 
		\Config::set('auth.providers.users.model', \App\Models\Userapp::class);
     

        //Change Password
          $user = JWTAuth::parseToken()->authenticate();
       

                     return response()->json(['user'=>$user]);

    }
    
     public function addToCart($id)
    {
        
        \Config::set('jwt.user', 'App\Models\Userapp'); 
		\Config::set('auth.providers.users.model', \App\Models\Userapp::class);
        $product = Sales::find($id);
 
        if(!$product) {
 
            abort(404);
 
        }
        $cart = session()->get('cart');
        $user = JWTAuth::parseToken()->authenticate();
        $userid=$user->id;
        $parent= Maincart::where('user_id',$userid)->orderby('id','desc')->first();
$parentid= $parent->id;
        // if cart is empty then this the first product
        if(!$cart) {
           
            $cart = [
                
                    $id =>  [
                       
                            'parent_id'   => $parentid,
                            'user_id'   => $userid,
                            'vendor_id'   => $product->vendor_id,
                            'vendor_name'   => $product->vendor_name,
                            'product_id'   => $product->id,
                            "product_name" => $product->name,
                            "category" => $product->category,
                            "quantity" => 1,
                            "price" => $product->price,
                             "main_price"=>$product->price,
                            "description" => $product->description,
                            "image" => $product->image
                         
                    ]
            ];
 
            session()->put('cart', $cart);

        }
 
        // if cart not empty then check if this product exist then increment quantity
        $quantitys = Cart::where('user_id',$user->id)->where('product_id',$product->id)->first();
        if(!$quantitys){
            $quantity = 0;
            $p = $quantity + 1;
        }else{
           $quantity = $quantitys->quantity;
           $p = $quantity + 1;
          
        } 

        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id]  = [
           
                'parent_id'   => $parentid,
                'product_id'   => $product->id,
                'user_id'   => $userid,
                'vendor_id'   => $product->vendor_id,
                 'vendor_name'   => $product->vendor_name,
                "product_name" => $product->name,
                "category" => $product->category,
                "quantity" => $quantity + 1,
                "price" => $product->price * $p,
                  "main_price"=>$product->price,
                "description" => $product->description,
                "image" => $product->image
             
        ];
     
        $client = new Cart();
        foreach($cart as $client){
          
        Cart::updateOrcreate([
            'product_id' => $client['product_id'],

        ],
    [
        'parent_id'=> $client['parent_id'], 
        'vendor_id'=>$client['vendor_id'],
         'vendor_name'=>$client['vendor_name'],
        'user_id'=>$client['user_id'],
        'product_name' => $client['product_name'],
         'category' =>  $client['category'],
        'quantity' =>  $client['quantity'],
        'price' => $client['price'],
        'main_price' => $client['main_price'],
        'description' => $client['description'],
        'image'  => $client['image'],
       

      
       
       
    ]);
    
   
}
        session()->put('cart', $cart);
        
          $userid=$user->id;
        $parentidd = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentidd) {
            $cartt = 0;
             
        }
         else {
        $partt = $parentidd->parent_id;
       $cartt = Cart::where('parent_id',$partt)->count();
        }
       
        return response()->json(['status'=>'true','message'=>'Product added to cart successfully!','items'=>$cartt, 'cart'=>$client]);
    }
    
    
     public function subtractfromCart($id)
    {
        
        \Config::set('jwt.user', 'App\Models\Userapp'); 
		\Config::set('auth.providers.users.model', \App\Models\Userapp::class);
        $product = Sales::find($id);
 
        if(!$product) {
 
            abort(404);
 
        }
        $cart = session()->get('cart');
        $user = JWTAuth::parseToken()->authenticate();
        $userid=$user->id;
        $parent= Maincart::where('user_id',$userid)->orderby('id','desc')->first();
$parentid= $parent->id;
        // if cart is empty then this the first product
        if(!$cart) {
           
            $cart = [
                
                    $id =>  [
                       
                            'parent_id'   => $parentid,
                            'user_id'   => $userid,
                            'vendor_id'   => $product->vendor_id,
                            'vendor_name'   => $product->vendor_name,
                            'product_id'   => $product->id,
                            "product_name" => $product->name,
                            "category" => $product->category,
                            "quantity" => 1,
                            "price" => $product->price,
                             "main_price"=>$product->price,
                            "description" => $product->description,
                            "image" => $product->image
                         
                    ]
            ];
 
            session()->put('cart', $cart);

        }
 
        // if cart not empty then check if this product exist then increment quantity
        $quantitys = Cart::where('user_id',$user->id)->where('product_id',$product->id)->first();
        if(!$quantitys){
            $quantity = 0;
        }else{
           $quantity = $quantitys->quantity;
           $p = $quantity - 1;
        } 

        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id]  = [
           
                'parent_id'   => $parentid,
                'product_id'   => $product->id,
                'user_id'   => $userid,
                'vendor_id'   => $product->vendor_id,
                'vendor_name'   => $product->vendor_name,
                "product_name" => $product->name,
                "category" => $product->category,
                "quantity" => $quantity - 1,
                "price" => $product->price * $p,
                 "main_price"=>$product->price,
                "description" => $product->description,
                "image" => $product->image
             
        ];
     
        $client = new Cart();
        foreach($cart as $client){
          
        Cart::updateOrcreate([
            'product_id' => $client['product_id'],

        ],
    [
        'parent_id'=> $client['parent_id'], 
        'vendor_id'=>$client['vendor_id'],
         'vendor_name'=>$client['vendor_name'],
        'user_id'=>$client['user_id'],
        'product_name' => $client['product_name'],
        'quantity' =>  $client['quantity'],
        'category' =>  $client['category'],
        'price' => $client['price'],
        'main_price' => $client['main_price'],
        'description' => $client['description'],
        'image'  => $client['image'],
       
        
      
       
       
    ]);
   
}
        session()->put('cart', $cart);
       
        $userid=$user->id;
        $parentidd = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentidd) {
            $cartt = 0;
             
        }
         else {
        $partt = $parentidd->parent_id;
       $cartt = Cart::where('parent_id',$partt)->count();
        }
       
        return response()->json(['status'=>'true','message'=>'Product added to cart successfully!','items'=>$cartt, 'cart'=>$client]);
    }
    
      public function destroy2($id)
    {
          $user = JWTAuth::parseToken()->authenticate();
         $userid=$user->id;
        $parentidd = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentidd) {
            $cartt = 0;
             
        }
         else {
        $partt = $parentidd->parent_id;
       $cartt = Cart::where('parent_id',$partt)->count();
        }
     $sale=Cart::find($id);
        $sale->delete();
    return response()->json(['status'=>'true','message'=>'Product Removed from cart!','items'=>$cartt]);

    }
    
      public function viewcart()
    {
         \Config::set('jwt.user', 'App\Models\Userapp'); 
		\Config::set('auth.providers.users.model', \App\Models\Userapp::class);
        $user = JWTAuth::parseToken()->authenticate();
        $userid=$user->id;
        $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentid) {
            $cart = [];
             $cartt = 0;
              return response()->json([ 'cart'=>$cart,'items'=>$cartt]);
        }
         else {
        $partt = $parentid->parent_id;
       $cart = Cart::where('parent_id',$partt)->get();
 $cartt = Cart::where('parent_id',$partt)->count();
       return response()->json([ 'cart'=>$cart,'items'=>$cartt]);
}
       
    }
    
    
     public function restaurants(request $request)
    {       
        $brand = Vendor::where('category','Restaurants')->where('verified','1')->orderby('id', 'desc')->get();
        return response()->json(['restaurants'=>$brand]);    
        
    }
    
      public function ports(request $request)
    {       
        $brand = Vendor::where('category','Food Port')->where('verified','1')->orderby('id', 'desc')->get();
        return response()->json(['FoodPort'=>$brand]);    
        
    }
    
      public function cafe(request $request)
    {       
        $brand = Vendor::where('category','Cafe/Eatery')->where('verified','1')->orderby('id', 'desc')->get();
        return response()->json(['cafe'=>$brand]);    
        
    }
    
      public function wellness(request $request)
    {       
        $brand = Vendor::where('category','Healthy & Wellness Products')->where('verified','1')->orderby('id', 'desc')->get();
        return response()->json(['wellness'=>$brand]);    
        
    }
    
      public function company(request $request)
    {       
        $brand = Vendor::where('category','Food Company')->where('verified','1')->orderby('id', 'desc')->get();
        return response()->json(['FoodCompany'=>$brand]);    
        
    }
    
      public function equipment(request $request)
    {       
        $brand = Vendor::where('category','Food Equipment')->where('verified','1')->orderby('id', 'desc')->get();
        return response()->json(['FoodEquipment'=>$brand]);    
        
    }

 public function orderhistory(request $request)
    {       
         \Config::set('jwt.user', 'App\Models\Userapp'); 
		\Config::set('auth.providers.users.model', \App\Models\Userapp::class);
        $user = JWTAuth::parseToken()->authenticate();
        $userid=$user->id;
        $brand = Checkout::where('user_id',$userid)->orderby('id', 'desc')->get();
        return response()->json(['orderhistory'=>$brand]);    
        
    }

 public function menu($id)
    {     
       $vendor=Vendor::find($id);
       $vendors = Sales::where('vendor_id',$id)->orderby('id', 'desc')->get();
        return response()->json(['menu'=>$vendors]);    

}

  public function update_user(Request $request)
    {
         \Config::set('jwt.user', 'App\Models\Userapp'); 
		\Config::set('auth.providers.users.model', \App\Models\Userapp::class);
        $user = JWTAuth::parseToken()->authenticate();
        $userid=$user->id;
        $agent = User::find($userid);
        $agent ->phone =$request -> input('phone');
        //$agent ->state =$request -> input('state');
        //$agent ->city =$request -> input('city');
        $agent ->address =$request -> input('address');
        $agent->image=$request -> input('image');
        $agent -> save();
 
         return response()->json(['status'=>'true','message'=>'Profile Updated','user'=>$agent]);
    }
    
    
     public function checkout(request $request)
     {       
        
        $userid=$request->input('userid');
        $total=$request->input('total');
        
        $user= User::where('id',$userid)->first();
        
           $parent= Maincart::where('user_id',$userid)->orderby('id','desc')->first();
           $parentid= $parent->id;
        $maincarts = Cart::where('user_id',$userid)->where('parent_id',$parentid)->get();
         foreach($maincarts as $maincart){
        $main = $maincart->parent_id;
        }
 $len=8;
  $char = 'ABCDEF567890';
    $randomNumber = '';
    for ($i = 0; $i < $len; $i++) {
        $randomNumber .= $char[rand(0, $len - 1)];
    }
        foreach($maincarts as $maincart){
             $vat = 10;
                $price=$maincart->price;
              
               $totalprice=$price;
              Checkout::create([
                'name' => $user->name,
                'address1' => $user->address,
                'state' => $user->state,
                'user_id'=> $userid,
                'payment_status'=> 1,
                'product_id' => $maincart->product_id,
                'cart_id'  => $maincart->parent_id,
                'product'  => $maincart->product_name,
                'quantity'  => $maincart->quantity,
                'price'  => $totalprice,
                'vendor_id'  => $maincart->vendor_id,
                'image' => $maincart->image,
                'phone' => $user->phone,
                'email' => $user->email,
                'accept' => 0,
                'reject' => 0,
                'delivered' => 0,
                'order_no'=>$randomNumber,
                'total'=>$request->input('total'),
            
        ]);
        
        
        
         $maincart->delete();
         
          $to = "$user->email"; 
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'gapaautoparts.com'; 
 
$subject = "Receipt of Transaction"; 
 
$htmlContent = '
   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="x-apple-disable-message-reformatting" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="color-scheme" content="light dark" />
    <meta name="supported-color-schemes" content="light dark" />
    <title></title>
    <style type="text/css" rel="stylesheet" media="all">
    /* Base ------------------------------ */
    
    @import url("https://fonts.googleapis.com/css?family=Nunito+Sans:400,700&display=swap");
    body {
      width: 100% !important;
      height: 100%;
      margin: 0;
      -webkit-text-size-adjust: none;
    }
    
    a {
      color: #3869D4;
    }
    
    a img {
      border: none;
    }
    
    td {
      word-break: break-word;
    }
    
    .preheader {
      display: none !important;
      visibility: hidden;
      mso-hide: all;
      font-size: 1px;
      line-height: 1px;
      max-height: 0;
      max-width: 0;
      opacity: 0;
      overflow: hidden;
    }
    /* Type ------------------------------ */
    
    body,
    td,
    th {
      font-family: "Nunito Sans", Helvetica, Arial, sans-serif;
    }
    
    h1 {
      margin-top: 0;
      color: #333333;
      font-size: 22px;
      font-weight: bold;
      text-align: left;
    }
    
    h2 {
      margin-top: 0;
      color: #333333;
      font-size: 16px;
      font-weight: bold;
      text-align: left;
    }
    
    h3 {
      margin-top: 0;
      color: #333333;
      font-size: 14px;
      font-weight: bold;
      text-align: left;
    }
    
    td,
    th {
      font-size: 16px;
    }
    
    p,
    ul,
    ol,
    blockquote {
      margin: .4em 0 1.1875em;
      font-size: 16px;
      line-height: 1.625;
    }
    
    p.sub {
      font-size: 13px;
    }
    /* Utilities ------------------------------ */
    
    .align-right {
      text-align: right;
    }
    
    .align-left {
      text-align: left;
    }
    
    .align-center {
      text-align: center;
    }
    /* Buttons ------------------------------ */
    
    .button {
      background-color: #3869D4;
      border-top: 10px solid #3869D4;
      border-right: 18px solid #3869D4;
      border-bottom: 10px solid #3869D4;
      border-left: 18px solid #3869D4;
      display: inline-block;
      color: #FFF;
      text-decoration: none;
      border-radius: 3px;
      box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
      -webkit-text-size-adjust: none;
      box-sizing: border-box;
    }
    
    .button--green {
      background-color: #22BC66;
      border-top: 10px solid #22BC66;
      border-right: 18px solid #22BC66;
      border-bottom: 10px solid #22BC66;
      border-left: 18px solid #22BC66;
    }
    
    .button--red {
      background-color: #FF6136;
      border-top: 10px solid #FF6136;
      border-right: 18px solid #FF6136;
      border-bottom: 10px solid #FF6136;
      border-left: 18px solid #FF6136;
    }
    
    @media only screen and (max-width: 500px) {
      .button {
        width: 100% !important;
        text-align: center !important;
      }
    }
    /* Attribute list ------------------------------ */
    
    .attributes {
      margin: 0 0 21px;
    }
    
    .attributes_content {
      background-color: #F4F4F7;
      padding: 16px;
    }
    
    .attributes_item {
      padding: 0;
    }
    /* Related Items ------------------------------ */
    
    .related {
      width: 100%;
      margin: 0;
      padding: 25px 0 0 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    
    .related_item {
      padding: 10px 0;
      color: #CBCCCF;
      font-size: 15px;
      line-height: 18px;
    }
    
    .related_item-title {
      display: block;
      margin: .5em 0 0;
    }
    
    .related_item-thumb {
      display: block;
      padding-bottom: 10px;
    }
    
    .related_heading {
      border-top: 1px solid #CBCCCF;
      text-align: center;
      padding: 25px 0 10px;
    }
    /* Discount Code ------------------------------ */
    
    .discount {
      width: 100%;
      margin: 0;
      padding: 24px;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #F4F4F7;
      border: 2px dashed #CBCCCF;
    }
    
    .discount_heading {
      text-align: center;
    }
    
    .discount_body {
      text-align: center;
      font-size: 15px;
    }
    /* Social Icons ------------------------------ */
    
    .social {
      width: auto;
    }
    
    .social td {
      padding: 0;
      width: auto;
    }
    
    .social_icon {
      height: 20px;
      margin: 0 8px 10px 8px;
      padding: 0;
    }
    /* Data table ------------------------------ */
    
    .purchase {
      width: 100%;
      margin: 0;
      padding: 35px 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    
    .purchase_content {
      width: 100%;
      margin: 0;
      padding: 25px 0 0 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    
    .purchase_item {
      padding: 10px 0;
      color: #51545E;
      font-size: 15px;
      line-height: 18px;
    }
    
    .purchase_heading {
      padding-bottom: 8px;
      border-bottom: 1px solid #EAEAEC;
    }
    
    .purchase_heading p {
      margin: 0;
      color: #85878E;
      font-size: 12px;
    }
    
    .purchase_footer {
      padding-top: 15px;
      border-top: 1px solid #EAEAEC;
    }
    
    .purchase_total {
      margin: 0;
      text-align: right;
      font-weight: bold;
      color: #333333;
    }
    
    .purchase_total--label {
      padding: 0 15px 0 0;
    }
    
    body {
      background-color: #F4F4F7;
      color: #51545E;
    }
    
    p {
      color: #51545E;
    }
    
    p.sub {
      color: #6B6E76;
    }
    
    .email-wrapper {
      width: 100%;
      margin: 0;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #F4F4F7;
    }
    
    .email-content {
      width: 100%;
      margin: 0;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    /* Masthead ----------------------- */
    
    .email-masthead {
      padding: 25px 0;
      text-align: center;
    }
    
    .email-masthead_logo {
      width: 94px;
    }
    
    .email-masthead_name {
      font-size: 16px;
      font-weight: bold;
      color: #A8AAAF;
      text-decoration: none;
      text-shadow: 0 1px 0 white;
    }
    /* Body ------------------------------ */
    
    .email-body {
      width: 100%;
      margin: 0;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #FFFFFF;
    }
    
    .email-body_inner {
      width: 570px;
      margin: 0 auto;
      padding: 0;
      -premailer-width: 570px;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #FFFFFF;
    }
    
    .email-footer {
      width: 570px;
      margin: 0 auto;
      padding: 0;
      -premailer-width: 570px;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      text-align: center;
    }
    
    .email-footer p {
      color: #6B6E76;
    }
    
    .body-action {
      width: 100%;
      margin: 30px auto;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      text-align: center;
    }
    
    .body-sub {
      margin-top: 25px;
      padding-top: 25px;
      border-top: 1px solid #EAEAEC;
    }
    
    .content-cell {
      padding: 35px;
    }
    /*Media Queries ------------------------------ */
    
    @media only screen and (max-width: 600px) {
      .email-body_inner,
      .email-footer {
        width: 100% !important;
      }
    }
    
    @media (prefers-color-scheme: dark) {
      body,
      .email-body,
      .email-body_inner,
      .email-content,
      .email-wrapper,
      .email-masthead,
      .email-footer {
        background-color: #333333 !important;
        color: #FFF !important;
      }
      p,
      ul,
      ol,
      blockquote,
      h1,
      h2,
      h3 {
        color: #FFF !important;
      }
      .attributes_content,
      .discount {
        background-color: #222 !important;
      }
      .email-masthead_name {
        text-shadow: none !important;
      }
    }
    
    :root {
      color-scheme: light dark;
      supported-color-schemes: light dark;
    }
    </style>
    <!--[if mso]>
    <style type="text/css">
      .f-fallback  {
        font-family: Arial, sans-serif;
      }
    </style>
  <![endif]-->
  </head>
  <body>
    <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0" role="presentation">
      <tr>
        <td align="center">
          <table class="email-content" width="100%" cellpadding="0" cellspacing="0" role="presentation">
            <tr>
              <td class="email-masthead" style="padding: 40px 0 30px 0;">
                <a href="https://gapaautoparts.com" class="f-fallback email-masthead_name" style="padding: 40px 0 30px 0;">
               <img src="https://logistic.gapaautoparts.com/public/img/Gapa_new_1.png" alt="Gapanaija Logo." width="200" height="150" style="display: block;" />

              </a>
              </td>
            </tr>
            <!-- Email Body -->
            <tr>
              <td class="email-body" width="100%" cellpadding="0" cellspacing="0">
                <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                  <!-- Body content -->
                  <tr>
                    <td class="content-cell">
                      <div class="f-fallback">
                        <h1>Hi '.$user->name.',</h1>
                        <p>Thanks for choosing Gapanaija. This email is the receipt for your purchase.</p>
                        <!-- Discount -->
                        <table class="purchase" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                          <tr>
                            
                             
                            
                          </tr>
                          <tr>
                            <td colspan="2">
                              <table class="purchase_content" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                  <th class="purchase_heading" align="left">
                                    <p class="f-fallback">Product Name</p>
                                  </th>
                                  <th class="purchase_heading" align="right">
                                    <p class="f-fallback">Amount</p>
                                  </th>
                                </tr> ';
                
               foreach($maincarts as $maincart){
                    
                         
                           
                          
   
                       $htmlContent .= '    
                               <tr>
                                  <td width="60%" class="purchase_item"><span class="f-fallback">'.$maincart->product_name.'</span></td>
                                  <td class="align-right" width="40%" class="purchase_item"><span class="f-fallback">₦'.number_format($maincart->price, 1).'</span></td>
                                </tr> ';
                              } 
     $htmlContent .= '          <tr>
                                  <td width="60%" class="purchase_footer" valign="middle">
                                    <p class="f-fallback purchase_total purchase_total--label">Total</p>
                                  </td>
                                  <td width="40%" class="purchase_footer" valign="middle">
                                    <p class="f-fallback purchase_total">₦'.number_format($request->input('total'), 1).'</p>
                                  </td>
                                </tr>
                                 <tr>
                                  <td width="60%" class="purchase_footer" valign="middle">
                                    <p class="f-fallback purchase_total purchase_total--label">Order ID</p>
                                  </td>
                                  <td width="40%" class="purchase_footer" valign="middle">
                                    <p class="f-fallback purchase_total">'.$randomNumber.'</p>
                                  </td>
                                </tr>
                                
                              </table>
                            </td>
                          </tr>
                        </table>
                        <p>Cheers,
                          <br>The Gapanaija Team</p>
                        <!-- Action -->
                       
                       
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                  <tr>
                    <td class="content-cell" align="center">
                      <p class="f-fallback sub align-center">©2021 Gapanaija</p>
                     
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>';  
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers); 
         
        }
         
        
      

        $carts = Checkout::where('user_id',$userid)->where('cart_id',$parentid)->get();
           return response()->json(['status'=>'true','message'=>'You have successfully checked out','order_no'=>$randomNumber]);    
    }
    
    
           public function password_reset(Request $request)
    {
      $email = $request->input('email');
      
       $check=User::where('email',$email)->first();
if(!$check) {
    return response()->json(['status'=>'false','message'=>'Email not found'],409);
}
else {
    $user = User::find($check->id);
        $to = "$email"; 
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'gapaautoparts.com'; 
 
$subject = "Password Reset"; 
 
$htmlContent = '
     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style type="text/css">
a[x-apple-data-detectors] {color: inherit !important;}
</style>

</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 20px 0 30px 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
<tr>
<td align="center" bgcolor="#fd7e14" style="padding: 40px 0 30px 0;">
<img src="https://logistic.gapaautoparts.com/public/img/Gapa_new_1.png" alt="GapaNaija Logo." width="200" height="150" style="display: block;" />
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif;">
<h1 style="font-size: 24px; margin: 0;">Hello '.$user->name.'!</h1>
</td>
</tr>
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
<p style="margin: 0;">Follow this link to reset your password <a href="http://gapaautoparts.com/password_reset'.$user->id.'">Reset Password</a></p>
</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td bgcolor="#fd7e14" style="padding: 30px 30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
<p style="margin: 0;">©2020 GapaNaija</p>
</td>
<tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>'; 
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers); 
 
           return response()->json(['status'=>'true','message'=>'Email Sent']);
    }
        
    }
    
      public function report(Request $request)
    {
        $user = New Report;
        $user ->company_name=$request->input('company_name');
         $user ->company_id=$request->input('company_id');
        $user ->reports=$request->input('reports');        
        $user -> save();
  return response()->json(['status'=>'true','message'=>'Your Report has been submitted']);
    }
    
    
    public function checkout2($unit = 'k')
{

     \Config::set('jwt.user', 'App\Models\Userapp'); 
		\Config::set('auth.providers.users.model', \App\Models\Userapp::class);
         $user = JWTAuth::parseToken()->authenticate();
    $userid=$user->id;
    $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
    if(!$parentid) {
    }else{
        
    
              $sales = Cart::where('user_id',$userid)->orderby('id','desc')->get();
              foreach($sales as $sale){
                $details=Vendor::where('id',$sale->vendor_id)->orderby('id','desc')->first();
                $d= $details->address;
              }
            
  $apiKey = 'AIzaSyBZWSNk4j_3B_35MlhMvd0Nz94GJEtUa4U';
  $depature= $d; 
  $currentaddress = $user->address;

  $addressFrom = $depature;
  $addressTo = $currentaddress;

 
  // Change address format
  $formattedAddrFrom    = str_replace(' ', '+', $addressFrom);
  $formattedAddrTo     = str_replace(' ', '+', $addressTo);
  
  // Geocoding API request with start address
  $geocodeFrom = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($formattedAddrFrom).'&sensor=false&key='.$apiKey);
  $outputFrom = json_decode($geocodeFrom);
  if(!empty($outputFrom->error_message)){
      return $outputFrom->error_message;
  }
  
  // Geocoding API request with end address
  $geocodeTo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($formattedAddrTo).'&sensor=false&key='.$apiKey);
  $outputTo = json_decode($geocodeTo);
  if(!empty($outputTo->error_message)){
      return $outputTo->error_message;
  }
  
  // Get latitude and longitude from the geodata
  $latitudeFrom    = $outputFrom->results[0]->geometry->location->lat;
  $longitudeFrom    = $outputFrom->results[0]->geometry->location->lng;
  $latitudeTo        = $outputTo->results[0]->geometry->location->lat;
  $longitudeTo    = $outputTo->results[0]->geometry->location->lng;
  
  // Calculate distance between latitude and longitude
  $theta    = $longitudeFrom - $longitudeTo;
  $dist    = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
  $dist    = acos($dist);
  $dist    = rad2deg($dist);
  $miles    = $dist * 60 * 1.1515;
  
  // Convert unit and return distance
  $unit = strtoupper($unit);
  if($unit == "K"){
      $dist= round($miles * 1.609344, 2);
  }
  if($dist <= "10"){
    $distance = $dist*65.5;
}
if($dist >= "11"){
    $distance = $dist*100.6;
}
           return response()->json(['status'=>'true','delivery'=>$distance]);    
           
    }
  
}

       public function delivery(Request $request){
                 Data::create([
                  'name'=>$request->get('name'),
                  'phone'=>$request->get('phone'),
                  'email'=>$request->get('email'),
                  'pickup'=>$request->get('pickup'),
                  'depart'=>$request->get('depart'),
                  'currentadd'=>$request->get('currentadd'),
                  'cost'=>$request->get('cost'),
                  'delivery_code'=>$request->get('number'),
                  'pending'=>1,
            
                 ]); 
            }
            
            
            
    public function getCategories(Request $request){
              $getAllCategories = Categories::where('category_status','1')->get();
              return response()->json(['getAllCategories'=>$getAllCategories]);    
         }
         



}


?>