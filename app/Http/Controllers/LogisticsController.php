<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\deliveryMan;
use App\Models\Logistics;
use App\Models\Wallet;
use AfricasTalking\SDK\AfricasTalking;
  use Twilio\Rest\Client;
use DB;
use Config;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Carbon\Carbon;
use Rave;


class LogisticsController extends Controller
{
   
    public function __construct() {
       
        $this->deliveryMan = new deliveryMan;
    }
    
    public function index()
    {
        //dd(Hash::make('12345678'));
    }
    
    
    public function authenticate(Request $request)
        {
            
            $email=$request->username;
            $password=$request->password;
            
            
     $user = DB::table("delivery_men")->where('email', '=', $email)->first();
     if (!$user) {
        return response()->json(['success'=>false, 'message' => 'Login Fail, please check email id'],401);
     }
     if (!Hash::check($password, $user->password)) {
        return response()->json(['success'=>false, 'message' => 'Login Fail, pls check password'],401);
     }
 
            
            
         $deliveryMan = deliveryMan::SELECT('*')
       ->where('email','=',$email) 
       ->orderby('id', 'DESC')->first();
       $totalearn = DB::table("orders")->where(["delivery_id"=>$deliveryMan->id])->count();
       $totalorders = DB::table("orders")->where(["delivery_id"=>$deliveryMan->id])->sum("total_paid");
             return response()->json(['message'=>'Login Successful','deliveryMan' => $deliveryMan,"totalearning"=>$totalearn,"totalorders"=>$totalorders]);
       
            
        }
        
        public function me(Request $request){
            $id = $request->id;
           $user = DB::table("delivery_men")->where('id', '=', $id)->first();
     if (!$user) {
        return response()->json(['success'=>false, 'message' => 'Login Fail, please check  id'],401);
     }
     
     return response()->json(["deliveryMan"=>$user]);
        }
        
       public function getAllLogistics(){
       $Logistics = Logistics::SELECT('*')
       ->orderby('id', 'asc')->get();
        return response()->json(['AllLogistics'=>$Logistics]);    
    }

    public function getAllPendingLogistics(){
       $Logistics = Logistics::SELECT('*')
       ->where('pending','=','1') 
       ->orderby('id', 'asc')->get();
        return response()->json(['AllLogistics'=>$Logistics]);    
    }

    public function ViewSingleLogistics($id){
        $Logistics = Logistics::SELECT('*')
       ->where('id','=',$id) 
       ->orderby('id', 'asc')->get();
       
      
        return response()->json($Logistics);
    }

    public function pickALogistics(Request $request){
        //get id of a logistic and update the delivery man id against the 
        $logistic_id=$request->input('logistics_id');
        $delivery_man_id=$request->input('delivery_man_id');
        
        $logistic = Logistics::find($logistic_id);
        $logistic ->pending = '0';
        $logistic ->ongoing = '1';
        $logistic ->delivery_man = $request->input('delivery_man_id');
        
        //get the delivery man wallet balance
        $wallet= Wallet::where('userid',$delivery_man_id)->orderby('id','desc')->first();
        $wallet_balance= $wallet->balance;
        
        
        if($wallet_balance > 0){
            if($logistic->save()){
                
                //charged the Delivery man wallet
                $amount=50;
                $new_balance=$wallet_balance-$amount;
                $newWallet = Wallet::create([
                    'userid' => $delivery_man_id,
                    'balance' => $new_balance,
                    'amount' => $amount
                
                ]);
                if($newWallet){
                    return response()->json(['logistic_id'=>$logistic_id ,'message'=>'Successful','new_balance'=>$new_balance]);
                }else{
                    return response()->json(['logistic_id'=>$logistic_id ,'message'=>'Something went wrong, Could not charge Wallet']);
                }
               

            }
        }else{
            return response()->json(['logistic_id'=>$logistic_id ,'message'=>'Your Wallet balance is insuficient']);
        }
       

    }
    
    public function fundwalet(Request $request){
         $delivery_man_id=$request->delivery_man_id;
         $total=$request->total;
         
          $sql = Wallet::SELECT('*')
        ->where('userid','=',$delivery_man_id) 
        ->orderby('id', 'desc')->get()->first();
        
          $balance=$sql ->balance;
          
          $amountPaid=$balance + $total;
          
          
            Wallet::create([
                'userid' =>$delivery_man_id,
                'amount' =>$total,
                'balance' =>$amountPaid,
        ]);
       
        
        return response()->json(['response'=>"Wallet Funded Successfully"]); 
          
    }

    public function ViewAllMyDeliveredLogistics(Request $request){
        $delivery_man_id=$request->delivery_man_id;
        $AllMydeliveries = Logistics::SELECT('*')
        ->where('delivered','=','1') 
        ->where('delivery_man','=',$delivery_man_id) 
        ->orderby('id', 'asc')->get();
         return response()->json(['AllMydeliveries'=>$AllMydeliveries]);    
    }

    public function ViewAllMyOnGoingLogistics(Request $request){
        $delivery_man_id=$request->delivery_man_id;
        $AllMyOngoingdeliveries = Logistics::SELECT('*')
        ->where('ongoing','=','1') 
        ->where('delivery_man','=',$delivery_man_id) 
        ->orderby('id', 'asc')->get();
         return response()->json(['AllMyOngoingdeliveries'=>$AllMyOngoingdeliveries]); 
    }
    
     public function ViewMyRejectedLogistics(Request $request){
        $delivery_man_id=$request->delivery_man_id;
        $AllMyRejecteddeliveries = Logistics::SELECT('*')
        ->where('rejected','=','1') 
        ->where('delivery_man','=',$delivery_man_id) 
        ->orderby('id', 'asc')->get();
         return response()->json(['AllMyRejecteddeliveries'=>$AllMyRejecteddeliveries]); 
    }
    
      public function ViewMyCanceledLogistics(Request $request){
        $delivery_man_id=$request->delivery_man_id;
        $AllMyCanceleddeliveries = Logistics::SELECT('*')
        ->where('cancel_id','=','1') 
        ->where('delivery_man','=',$delivery_man_id) 
        ->orderby('id', 'asc')->get();
         return response()->json(['AllMyCanceleddeliveries'=>$AllMyCanceleddeliveries]); 
    }
    
public function SendDeliveryCode(Request $request){
        $logistic_id=$request->input('logistics_id');
        
        $logistic = Logistics::find($logistic_id);
        
        $to=$logistic->email;
        $name=$logistic->name;
        $phone=$logistic->phone;
        
        $delivery_code=$logistic->delivery_code;
        if($delivery_code == null){
            DB::table("logistics")->where(["id"=>$logistic->id])->update(["delivery_code"=>rand()]);
        }
        
        
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'EatNaija.com'; 
 
$subject = "EatNaija Logistics Services"; 
 
$htmlContent = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style type="text/css">
a[x-apple-data-detectors] {color: inherit !important;}
</style>

</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 20px 0 30px 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
<tr>
<td align="center" bgcolor="#fd7e14" style="padding: 40px 0 30px 0;">
<img src="https://eatnaija.com/public/img/logo-white.png" alt="EatNaija Logo." width="200" height="150" style="display: block;" />
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif;">
<h1 style="font-size: 24px; margin: 0;">Hello '.$name.'!</h1>
<h3>EatNaija Logistics Services</h3>
</td>
</tr>
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
<p style="margin: 0;">Your EatNaija Logistics Services Code is '.$delivery_code.'</p>
<p>We lead you to places, reveal dishes we think you will like to visit and taste</p>
<p>Thanks for choosing EatNaija</p>
</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td bgcolor="#fd7e14" style="padding: 30px 30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
<p style="margin: 0;">©2020 EatNaija</p>
</td>
<tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>'; 
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers);
   
            
    $dnd = 2;
	$from = "EatNaija";
	$to = $phone;
// 	$body = 'Your EatNaija Logistics Services Code is '..'We lead you to places, reveal dishes we think you will like to visit and taste. Thanks for choosing EatNaija';
	$body = "Your GapaNaija Logistics Services Code is $delivery_code , Kindly use this code to claim your items. Thanks for choosing GapaNaija";
// 	$token = "AAGpGeeFPidD5rtncl67dUpD9mEdHcY6dCnIraBFgNrO0qb0tUmQHy2afexs";


	
	

// function api_call($token, $from, $to, $body, $dnd = 2){
// $ch = curl_init();
// curl_setopt($ch, CURLOPT_URL,"https://www.bulksmsnigeria.com/api/v1/sms/create");
// curl_setopt($ch, CURLOPT_POST, 1);
// curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('api_token' => $token,'from' => $from,'to' => $to,'body' => $body, 'dnd' => $dnd)));
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// $server_output = curl_exec($ch);
// curl_close ($ch);

// return $server_output;
// }


// $arr = api_call($token, $from, $to, $body);


  // Your Account SID and Auth Token from twilio.com/console
$account_sid = 'ACa07d9dfe8d7018ff2d519f75dcb7df84';
$auth_token = '98ad77cf03cbffdcd030173e84c8beb9';
// In production, these should be environment variables. E.g.:
// $auth_token = $_ENV["TWILIO_AUTH_TOKEN"]

// A Twilio number you own with SMS capabilities
 
$num = preg_replace('/^(?:\+?234|0)?/','+234', $to);
$client = new Client($account_sid, $auth_token);
$client->messages->create(
    // Where to send a text message (your cell phone?)
    "$num",
    array(
        'from' => "+14043345663",
        'body' => $body
    )
);
 
 
   
        return response()->json(['emailResponse'=>$send,'response'=>'Success', 'delivery_code'=>$delivery_code]); 
        
    } 

public function confirmdelivery(Request $request){
        $logistic_id=$request->input('logistics_id');
        
        $logistic = Logistics::find($logistic_id);
        $logistic ->delivered = '1';
        $logistic ->ongoing = '0';
        $logistic ->pending = '0';
        $logistic ->save();
        
         if($logistic){
            return response()->json(['response'=>"Success"]); 
        }
}

public function canceledelivery(Request $request){
        $logistic_id=$request->input('logistics_id');
        
        $logistic = Logistics::find($logistic_id);
        $logistic ->cancel_id = '0';
     
        $logistic ->ongoing = '0';
        $logistic ->pending = '1';
        
        $logistic ->save();
        
         if($logistic){
            return response()->json(['response'=>"Success"]); 
        }
}

public function checkBalance(Request $request){
    $delivery_man_id=$request->input('delivery_man_id');
     $Wallet = Wallet::SELECT('*')
       ->where('userid','=',$delivery_man_id) 
       ->orderby('id', 'DESC')->first();
      return response()->json($Wallet);  
}

public function sms(){
    // Your Account SID and Auth Token from twilio.com/console
$account_sid = 'ACa07d9dfe8d7018ff2d519f75dcb7df84';
$auth_token = '98ad77cf03cbffdcd030173e84c8beb9';
// In production, these should be environment variables. E.g.:
// $auth_token = $_ENV["TWILIO_AUTH_TOKEN"]

// A Twilio number you own with SMS capabilities
 

$client = new Client($account_sid, $auth_token);
$client->messages->create(
    // Where to send a text message (your cell phone?)
    "+23408074725983",
    array(
        'from' => "+14043345663",
        'body' => "test"
    )
);

}


  
            public function withdraw(Request $request){
                $user = DB::table("delivery_men")->where(["id"=>$request->delivery_man_id])->first();
                if($user->account_number == null){
                    return response()->json(["message"=>"Please add account to withdraw"],400);
                }
                if(!$user){
                    return response()->json(["message"=>"User Not Found!"],404);
                }
                $amount = $request->amount;
                $id = rand();
                 
                    
                    if($amount  > $user->wallet){
                        return response()->json(["message"=>"insufficient fund"],400);
                    }
                    
                    DB::table("driver_trans")->insert(["userid"=>$user->id,"amount"=>$amount,"trans_id"=>$id,"method"=>"transfer","type"=>"withdraw","status"=>"pending"]);
                    DB::table("delivery_men")->where(["id"=>$user->id])->decrement("wallet",$amount);
                    return response()->json(["message"=>"Withdrawal SuccessFul, Pending For Approval"]);
                    
            }
            
            public function validate_bank(Request $request){
                $account_number = $request->account_number;
                $account_bank = $request->account_bank;
                $user = DB::table("delivery_men")->where(["id"=>$request->delivery_man_id])->first();
                if(!$user){
                    return response()->json(["message"=>"User not found!"],404);
                }
               $res =  \Http::withToken("FLWSECK_TEST-287ebe764d6c3eb9b8138796f61dfaf0-X")->post('https://api.flutterwave.com/v3/accounts/resolve',[
                    
                    "account_number"=>$account_number,
                    "account_bank"=>$account_bank
                    ]);
                    $obj = $res->object();
            
                if($res->failed()){
                    return response()->json(["message"=>"Something went wrong","error"=>$obj->data->message],400);
                }
                
                DB::table("delivery_men")->where(["id"=>$request->delivery_man_id])->update(["account_number"=>$account_number,"account_bank"=>$account_bank,"account_name"=>$obj->data->account_name]);
                return response()->json(["message"=>"Updated!"]);
            }
            public function transactions(Request $request){
                $user = DB::table("delivery_men")->where(["id"=>$request->delivery_man_id])->first();
                
                if(!$user){
                    return response()->json(["message"=>"User Not Found!"],404);
                }
               
                    
                   $data =  DB::table("driver_trans")->where(["userid"=>$user->id])->get();
                  
                    return response()->json(["data"=>$data]);
                    
            }
}