<?php

namespace App\Http\Controllers;

use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use App\Models\Categories;
use App\Models\Chatbot;

class BotManController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');

        $botman->hears('{message}', function($botman, $message) {

            if ($message = 'hi|Hi|hey|hey|hello|Hello') {
                $this->askName($botman);
            }else{
                $botman->reply("Sorry, Reply hi to continue");
            }

        });

        $botman->listen();
    }

    /**
     * Place your BotMan logic here.
     */
    public function askName($botman)
    {
        $botman->ask('Hello! What is your Name?', function(Answer $answer) {

            $name = $answer->getText();

            $categories= Categories::all();
            $question = Question::create('Nice to meet you '.$name.', What kind of Service are you looking for?')
            ->callbackId('select_service');
             foreach($categories as $category){
            $question->addButtons([
               
                Button::create($category->name)->value($category->name),
              
            ]);

             }
              $question->addButtons([
            Button::create('Cannot find what you are looking for?')->value('find')
              ]);
            
         return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                $restaurant=Chatbot::where('category','Restaurants')->orderby('id','desc')->first();
             $restname=$restaurant->name;
                if ($answer->getValue() === 'Restaurants') {
                   $this->say($restname);
                }
                elseif($answer->getValue() === 'FoodPort') {
                    $foodp=Chatbot::where('category','Food Port')->orderby('id','desc')->first();
             $foodpname=$foodp->name;
                   $this->say($foodpname);
                }
                 elseif($answer->getValue() === 'Cafe/Eatery') {
                       $cafe=Chatbot::where('category','Cafe/Eatery')->orderby('id','desc')->first();
             $cafename=$cafe->name;
                   $this->say($cafename);
                }
                 elseif($answer->getValue() === 'Healthy & Wellness Products') {
                       $health=Chatbot::where('category','Healthy & Wellness Products')->orderby('id','desc')->first();
             $healthname=$health->name;
                   $this->say($healthname);
                }
                 elseif($answer->getValue() === 'Food Company') {
                      $company=Chatbot::where('category','Food Company')->orderby('id','desc')->first();
             $com=$company->name;
                    $this->say($com);
                }
                 elseif($answer->getValue() === 'Food Equipment') {
                      $equipment=Chatbot::where('category','Food Equipment')->orderby('id','desc')->first();
             $equip=$equipment->name;
                   $this->say($equip);
                }
                
                
                  elseif($answer->getValue() === 'find') {
                   $this->say('Chat with a food manager on <a href=" https://api.whatsapp.com/send?phone=+2348036216373" target="_blank"> Whatsapp</a>');
                }
                
            else {
                    $this->say('Alright, lets talk about something else.');
                }
            }
        });
    });
    
    }
   
}