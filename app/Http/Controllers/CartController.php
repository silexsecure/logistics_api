<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Vendor;
use App\Models\Categories;
use App\Models\User;
use App\Models\Sales;
use App\Models\Maincart;
use App\Models\Cart;
class CartController extends Controller
{
    public function addToCart($id)
    {
        
       
        $product = Sales::find($id);
 
      
       
        $user = Auth::user();
 if(!$user){
          return redirect()->back()->with('error','please log in');
        }else{
         $userid=$user->id;
}         $parent= Maincart::where('user_id',$userid)->orderby('id','desc')->first();
$parentid= $parent->id;

        // if cart is empty then this the first product
        $totals = Cart::where('user_id',$user->id)->orderby('id','desc')->first();
        if(!$totals){
           $total=0;
       }else{
            $total = $totals->total;
          
        } 
        if(!$product->promo_price){
            $price=$product->price;
        }else{
            $price=$product->promo_price;
           
         } 
           
            $cart = [
                
                    $id =>  [
                       
                            'parent_id'   => $parentid,
                            'user_id'   => $userid,
                            'vendor_id'   => $product->vendor_id,
                            'vendor_name'   => $product->vendor_name,
                            'product_id'   => $product->id,
                            "product_name" => $product->title,
                            "quantity" => 1,
                            "price" => $price,
                            "main_price"=>$price,
                            "total" => $price + $total - $price ,
                            "category" => $product->category,
                            "description" => $product->description,
                            "image" => $product->image,
                         
                    ]
            ];
 

      
 
        // if cart not empty then check if this product exist then increment quantity
        $quantitys = Cart::where('user_id',$user->id)->where('product_id',$product->id)->first();
        if(!$quantitys){
            $quantity = 0;
            $p = $quantity + 1;
        }else{
           $quantity = $quantitys->quantity;
           $p = $quantity + 1;
          
        } 

        // if item not exist in cart then add to cart with quantity = 1
       
       
        
        $cart[$id]  = [
           
                'parent_id'   => $parentid,
                'product_id'   => $product->id,
                'user_id'   => $userid,
                'vendor_id'   => $product->vendor_id,
                'vendor_name'   => $product->vendor_name,
                "product_name" => $product->name,
                "quantity" => $quantity + 1,
                "price" => $price * $p,
                 "main_price"=>$price,
                "total" => $price + $total,
                "category" => $product->category,
                "description" => $product->description,
                "image" => $product->image,
             
        ];
        $client = new Cart();
        foreach($cart as $client){

        Cart::updateOrcreate([
            'product_id' => $client['product_id'],

        ],
    [
        'parent_id'=> $client['parent_id'], 
        'vendor_id'=>$client['vendor_id'],
        'vendor_name'=>$client['vendor_name'],
        'user_id'=>$client['user_id'],
        'product_name' => $client['product_name'],
        'quantity' =>  $client['quantity'],
        'price' => $client['price'],
         'main_price' => $client['main_price'],
        'total' => $client['total'] ,
        'category' => $client['category'],
        'description' => $client['description'],
        'image'  => $client['image'],
       
        
      
       
       
    ]);
   
}
       
        return redirect()->back()->with('success','Product Added to Cart');
    }


//pally add to cart

   


    public function destroy_cart($id)
    {
     $cat=Cart::find($id);
        $cat->delete();
        return redirect()->back()->with('success','Product removed from Cart');

    }

    public function viewcart()
    {
        $user = Auth::user();
        if(!$user){
                 return redirect()->back()->with('error','please log in');
               }else{
                $userid=$user->id;
       } 
        $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentid) {
            $cart = NULL;   
            $cartnum = "";
            if(!$cartnum){
               $cart=0;
                }else{
                   $cartnum = $cart->count();
                    }  
                    $sales = Cart::where('user_id',$userid)->orderby('id','desc')->get();
    
                        return view('cart')->with('cart',$cart)->with('cartnum',$cartnum)->with('sales',$sales);
                        
        }else {
        $part = $parentid->parent_id;
        $cart = Cart::where('parent_id',$part)->get();
        $cartnum = $cart->count();
        if(!$cartnum){
           $cart=0;
            }else{
               $cartnum = $cart->count();
                } 
                $total = Cart::where('user_id',$user->id)->sum('price');

 
                                 $sales = Cart::where('user_id',$userid)->orderby('id','desc')->get();

                    return view('cart')->with('sales',$sales)->with('cart',$cart)->with('cartnum',$cartnum)->with('total',$total);
  }

}

 public function add($id)
    {
        
       
        $product = Cart::find($id);
        $p=$product->quantity + 1;
        $product->quantity = $p;
        $product->price= $product->main_price*$p;
        $product->save();
        return redirect()->back();
}

public function sub($id)
    {
        
       
        $product = Cart::find($id);
         $p=$product->quantity - 1;
        $product->quantity = $p;
         $product->price= $product->main_price*$p;
        $product->save();
        return redirect()->back();
}
}
