<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Session;
class LoginController extends Controller{
    public function authenticate(Request $request){
        // Retrive Input
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // if success login
             if (Auth::user()->email_verified_at==1) {

           return Redirect::to(Session::get('url.intended'));
  }else{
      Auth::logout();
              return redirect()->back()->with('error','Please Verify Your Email');
  }
            //return redirect()->intended('/details');
        }
        // if failed login
        return redirect()->back()->with('error','Invalid Credentials');
    }
    
    public function login()
{
        Session::put('url.intended',URL::previous());
        return view('layout.footer');
}
}