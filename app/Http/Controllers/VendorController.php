<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Vendor;
use App\Models\Categories;
use App\Models\Category;
use App\Models\User;
use DB;
use App\Models\Sales;
use App\Models\Cart;
use App\Models\Checkout;
use Carbon\Carbon;
class VendorController extends Controller
{
    public function showvendorLoginForm()
    {
        return view('vendor.login',['url' => 'vendor']);
    }

    public function vendorLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('vendor')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect('vendor_dashboard');
                }
                return redirect('/login_vendor')->with('status', 'Your credentials do not match our records');

    }


    public function index()
    {
      $admin = Auth::guard('vendor')->user();
      if(!$admin) {
        return redirect('/login_vendor');
    }else{
        $image= Auth::guard('vendor')->user()->image;

       // $checkouts = Checkout::where('payment_status','1',)->get();
      //  $products = Sales::all();
       $products = Sales::where('vendor_id',$admin->id)->get();
      $sold = Checkout::where('vendor_id',$admin->id)->get();
      
      $latests= Checkout::where('vendor_id',$admin->id)->paginate(5);
      $total=Checkout::where('vendor_id',$admin->id)->where('accept','1')->sum('price');
      $last=Checkout::where('vendor_id',$admin->id)->where('accept','1')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->sum('price');
      $present=Checkout::where('vendor_id',$admin->id)->where('accept','1')->where('created_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->sum('price');

    $best_food= Checkout::select('product_id')
    ->selectRaw('COUNT(*) AS count')
    ->groupBy('product_id')
    ->where('vendor_id',$admin->id)
    ->orderByDesc('count')
    ->first();
    if(!$best_food) {
        $best_f="You're yet to upload an item";
    }else{
    $best_ff=Sales::where('id',$best_food->product_id)->first();
    $best_f= $best_ff->name;
    }
}

      return view('vendor.dashboard',compact(['products','sold','latests','total','last','present','best_f']));
    }

    public function showcategory()
    {
        $admin = Auth::guard('vendor')->user();
        if(!$admin) {
          return redirect('/login_vendor');
      }else{
        return view('vendor.addcategory');
    }
}
    protected function createcategory(Request $request)
    {  
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:250',
         ]);
      $categories = Category::create([
            'name' => $request->get('name'),
            
        ]);
        return redirect ('categories')->with('success', 'Category Added');
    }
    public function category(Request $request)
    {
        $admin = Auth::guard('vendor')->user();
        if(!$admin) {
          return redirect('/login_vendor');
      }else{
       $categories = Category::where('vendor_id',$admin->id)->orderby('id', 'desc')->paginate(10);
        return view ('vendor.addcategory')->with('categories', $categories);
    }
}


 public function showmenu()
    {
        $admin = Auth::guard('vendor')->user();
        if(!$admin) {
          return redirect('/login_vendor');
      }else{
     $agents = Sales::where('vendor_id',$admin->id)->orderby('id', 'desc')->paginate(10);
        return view('vendor.menu',compact('agents'));
    }
}

function profile(){
    
   
      $admin = Auth::guard('vendor')->user();
      if(!$admin) {
        return redirect('/login_vendor');
    }else{
        $image= Auth::guard('vendor')->user()->image;
    }
        
   
      return view('vendor.profile',compact(['admin']));
    
}

function updateVendorProfile(Request $request){
    
    
   $admin = Auth::guard('vendor')->user();
   $id=$admin->id;
   $req= Vendor::find($id);
   $req->company_name=$request->get('company_name');
  
   $req->phone=$request->get('phone_number');
   $req->bank_name=$request->get('bank_name');
   $req->bank_code=$request->get('bank_code');
   $req->address=$request->get('vendor_address');
   $req->city=$request->get('vendor_city');
   $req->state=$request->get('vendor_state');
   $req->account=$request->get('account_number');
   
   
$account_bank=$request->get('bank_code');
$account_number=$request->get('account_number');
$business_name=$request->get('company_name');
$business_email=$request->get('email');
$business_contact=$request->get('vendor_address');
$business_contact_mobile=$request->get('phone_number');
$business_mobile=$request->get('phone_number');

$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.flutterwave.com/v3/subaccounts",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>"{\n    \"account_bank\": \"$account_bank\",
  \n    \"account_number\": \"$account_number\",
  \n    \"business_name\": \"$business_name\",
  \n    \"business_email\": \"$business_email\",
  \n    \"business_contact\": \"$business_contact\",
  \n    \"business_contact_mobile\": \"$business_contact_mobile\",
  \n    \"business_mobile\": \"$business_mobile\",
  \n    \"country\": \"NG\",
  \n    \"meta\": [\n        {\n            \"meta_name\": \"mem_adr\",
  \n            \"meta_value\": \"0x16241F327213\"\n        }\n    ],
  \n    \"split_type\": \"percentage\",
  \n    \"split_value\": 0.1\n}",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json",
    "Authorization: FLWSECK-13ec888e9d9f6fee8c3ca805118e3e1e-X"
  ),
));

$response = curl_exec($curl);

curl_close($curl);

$obj = json_decode($response,true);
$s= $obj['status'];


//echo $obj['data']['id'];


             
             if($s=="success"){
                    $sub_accountID= $obj['data']['id'];
                    $subaccount_id=$obj['data']['subaccount_id'];
                    $req->sub_accountID = $sub_accountID;
                     $req->subAid = $subaccount_id;
                }
            
   
   $req->save();
   return redirect()->back()->with('success','Vendor Profile Updated Successfully');
}


public function showorders()
    {
        $admin = Auth::guard('vendor')->user();
        if(!$admin) {
          return redirect('/login_vendor');
      }else{
     $agents = Checkout::where('vendor_id',$admin->id)->where('accept','1')->orderby('id', 'desc')->paginate(10);
        return view('vendor.orders',compact('agents'));
    }
}

public function showrequests()
    {
        $admin = Auth::guard('vendor')->user();
        if(!$admin) {
          return redirect('/login_vendor');
      }else{
     $agents = Checkout::where('vendor_id',$admin->id)->orderby('id', 'desc')->paginate(10);
        return view('vendor.requests',compact('agents'));
    }
}


public function showrejected()
    {
        $admin = Auth::guard('vendor')->user();
        if(!$admin) {
          return redirect('/login_vendor');
      }else{
     $agents = Checkout::where('vendor_id',$admin->id)->where('reject','1')->orderby('id', 'desc')->paginate(10);
        return view('vendor.reject',compact('agents'));
    }
}


public function accept(Request $request, $id )
{ 
   $req= Checkout::find($id);
   $req->accept=1;
   $req->save();
   return redirect()->back()->with('success','Request has been accepted');
}

public function reject(Request $request, $id )
{ 
 
   $req= Checkout::find($id);
   $req->reject=1;
   $req->save();
   return redirect()->back()->with('success','Request has been rejected');
}


public function delete_store($id)
    {
     
    $sale = Sales::find($id);
    $sale->delete();


   return redirect()->back()->with('success','Post Deleted');
          
    }

}
