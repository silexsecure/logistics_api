<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Vendor;
use App\Models\Categories;
use App\Models\Cart;
use App\Models\User;
use App\Models\Sales;
use App\Models\Comment;
use App\Models\Newsletter;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Models\Maincart;
use Laravel\Fortify\Http\Controllers\RegisteredUserController;
use DB;
use App\Models\Report;
use App\Models\Cms;
use App\Models\Policy;
use App\Models\Advert;
use App\Models\Contact;

class HomeController extends Controller
{
    

    
     public function create(Request $request)
    {
        $otp = rand(1000,9999);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
             'phone' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'confirmed'],
           
        ])->validate();
        $user= User::create([
             'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'password' =>Hash::make($request['password']),
            'otp' => $otp,
          
        ]);
        
        $to = "$user->email"; 
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'EatNaija.com'; 
 
$subject = "Welcome to EatNaija"; 
 
$htmlContent = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style type="text/css">
a[x-apple-data-detectors] {color: inherit !important;}
</style>

</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 20px 0 30px 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
<tr>
<td align="center" bgcolor="#fd7e14" style="padding: 40px 0 30px 0;">
<img src="https://eatnaija.com/public/img/logo-white.png" alt="EatNaija Logo." width="200" height="150" style="display: block;" />
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif;">
<h1 style="font-size: 24px; margin: 0;">Hello '.$user->name.'!</h1>
<h3>Welcome to EatNaija </h3>
</td>
</tr>
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
<p style="margin: 0;">Your OTP is '.$user->otp.'</p>
<p>We lead you to places, reveal dishes we think you will like to visit and taste</p>
<p>Thanks for choosing EatNaija</p>
</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td bgcolor="#fd7e14" style="padding: 30px 30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
<p style="margin: 0;">©2020 EatNaija</p>
</td>
<tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>'; 
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers);
        $cart=  Maincart::create([
            'user_id' => $user->id,
        ]);
        
        
         return redirect('/login_with_otp');
    }
    
    
    
     public function otp_form(Request $request)
    {
        
   
       $user = Auth::user();
        
        if(!$user){
            $userid=0;
        }else{
            $userid=$user->id;
        } 
     $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentid) {
           $cart = NULL;   
            
           $cartnum = "";
         if(!$cartnum){
             $cart=0;
             }else{
                  $cartnum = $cart->count();
                   }  
                    
            return view('otp')->with('cartnum',$cartnum);
    }else {
        $part = $parentid->parent_id;
      $cart = Cart::where('parent_id',$part)->get();
        $cartnum = $cart->count();
        if(!$cartnum){
           $cartnum=0;
           }else{
              $cartnum = $cart->count();
                } 
                 $user = User::find($id);
            return view('otp')->with('cartnum',$cartnum);
    }
    }
    
    
     public function about(Request $request)
    {
        
   
       $user = Auth::user();
        
        if(!$user){
            $userid=0;
        }else{
            $userid=$user->id;
        } 
     $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentid) {
           $cart = NULL;   
            
           $cartnum = "";
         if(!$cartnum){
             $cart=0;
             }else{
                  $cartnum = $cart->count();
                   }  
                 $about=Cms::where('id','1')->first();   
            return view('about')->with('cartnum',$cartnum)->with('about',$about);
    }else {
        $part = $parentid->parent_id;
      $cart = Cart::where('parent_id',$part)->get();
        $cartnum = $cart->count();
        if(!$cartnum){
           $cartnum=0;
           }else{
              $cartnum = $cart->count();
                }
              $about=Cms::where('id','1')->first();
            return view('about')->with('cartnum',$cartnum)->with('about',$about);
    }
    }
    
     public function privacy(Request $request)
    {
        
   
       $user = Auth::user();
        
        if(!$user){
            $userid=0;
        }else{
            $userid=$user->id;
        } 
     $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentid) {
           $cart = NULL;   
            
           $cartnum = "";
         if(!$cartnum){
             $cart=0;
             }else{
                  $cartnum = $cart->count();
                   }  
                    $policy=Policy::where('id','1')->first(); 
            return view('privacy')->with('cartnum',$cartnum)->with('policy',$policy);
    }else {
        $part = $parentid->parent_id;
      $cart = Cart::where('parent_id',$part)->get();
        $cartnum = $cart->count();
        if(!$cartnum){
           $cartnum=0;
           }else{
              $cartnum = $cart->count();
                } 
                 $policy=Policy::where('id','1')->first();
            return view('privacy')->with('cartnum',$cartnum)->with('policy',$policy);
    }
    }
    
    
     public function contactus(Request $request)
    {
        
   
       $user = Auth::user();
        
        if(!$user){
            $userid=0;
        }else{
            $userid=$user->id;
        } 
     $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentid) {
           $cart = NULL;   
            
           $cartnum = "";
         if(!$cartnum){
             $cart=0;
             }else{
                  $cartnum = $cart->count();
                   }  
                 
            return view('contact')->with('cartnum',$cartnum);
    }else {
        $part = $parentid->parent_id;
      $cart = Cart::where('parent_id',$part)->get();
        $cartnum = $cart->count();
        if(!$cartnum){
           $cartnum=0;
           }else{
              $cartnum = $cart->count();
                }
            return view('contact')->with('cartnum',$cartnum);
    }
    }
    
    
    
    
    public function loginWithOtp(Request $request)
  {
      $otp= $request->only('otp');
      $check = DB::table('users')->where('otp',$otp)->first();
     

      if(!is_null($check)){
          $user = User::find($check->id);

          if($user->email_verified_at == 1){
              return redirect('/login_with_otp')->with(
                  'success', 'Account already verified..'
              );
          }
else{
          $user->email_verified_at = 1;
          $user->save();
         
          Auth::attempt(['email' => $user->email, 'password' => $user->password]);
          return redirect('/')->with('success','Email Verified, You can now Login');
          
      }
    }
     return redirect('/login_with_otp')->with(
                  'success', 'Invalid OTP'
              );
    
  }
    
    
    
    public function index()
    {

        $user = Auth::user();
        
        if(!$user){
            $userid=0;
        }else{
            $userid=$user->id;
        } 
       
        $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentid) {
           $cart = NULL;   
            
           $cartnum = "";
         if(!$cartnum){
             $cart=0;
             }else{
                  $cartnum = $cart->count();
                   }  
                  
                   $restaurant = Vendor::where('category','Restaurants')->where('verified','1')->get();
                   $food_port = Vendor::where('category','Food Port')->where('verified','1')->get();
                   $cafe = Vendor::where('category','Cafe/Eatery')->where('verified','1')->get();
                   $snack = Vendor::where('category','Snack Spot')->where('verified','1')->get();
                   $foodC = Vendor::where('category','Food Company')->where('verified','1')->get();
                   $foodE = Vendor::where('category','Food Equipment')->where('verified','1')->get();

                   $vendors= Vendor::where('category','Restaurants')->where('verified','1')->orderby('id','asc')->paginate(3);
                    $adverts= Advert::orderby('id','asc')->get();
                  //  $pally = Pally::where('paid','1')->where('expire_at','>',Carbon::now())->orderby('id','desc')->get();

                   // $sales = Sales::orderby('id','desc')->paginate(8);
               


                        return view('index',compact(['restaurant','adverts','food_port','cafe','snack','foodC','foodE','vendors','cartnum']));//->with('cart',$cart)->with('cartnum',$cartnum)->with('sales',$sales)->with('pally',$pally)->with('bests',$bests);
                        
        }else {
       $part = $parentid->parent_id;
     $cart = Cart::where('parent_id',$part)->get();
       $cartnum = $cart->count();
       if(!$cartnum){
          $cart=0;
          }else{
             $cartnum = $cart->count();
               } 
              
               // $totals = Cart::where('user_id',$user->id)->orderby('id','desc')->first();
               // $totals2 = Cart::where('user_id',$user->id)->orderby('id','asc')->first();
               // if($totals < $totals2){
                   // $total= $totals2->total;
                   //  }else{
                       
                     //   $total= $totals->total;
                      //   } 
                      //   if(count($cart)==1){
                        //    $total= $totals2->price;
                         //    }
                         //    if(count($cart)==2){
                         //       $total= $totals2->price + $totals->price;
                         //        }

                         //        $bests = Sales::orderby('id','asc')->paginate(8);

                       //          $pally = Pally::where('paid','1')->where('expire_at','>',Carbon::now())->orderby('id','desc')->get();

              //  $sales = Sales::orderby('id','desc')->paginate(8);
              $restaurant = Vendor::where('category','Restaurants')->where('verified','1')->get();
                   $food_port = Vendor::where('category','Food Port')->where('verified','1')->get();
                   $cafe = Vendor::where('category','Cafe/Eatery')->where('verified','1')->get();
                   $snack = Vendor::where('category','Snack Spot')->where('verified','1')->get();
                   $foodC = Vendor::where('category','Food Company')->where('verified','1')->get();
                   $foodE = Vendor::where('category','Food Equipment')->where('verified','1')->get();

                   $vendors= Vendor::orderby('id','asc')->paginate(3);
                   $adverts= Advert::orderby('id','asc')->get();

                   return view('index',compact(['restaurant','food_port','cafe','snack','foodC','foodE','vendors','cartnum','adverts']));//->with('cart',$cart)->with('cartnum',$cartnum)->with('sales',$sales)->with('pally',$pally)->with('bests',$bests);

              //  return view('index');//->with('cart',$cart)->with('cartnum',$cartnum)->with('sales',$sales)->with('pally',$pally)->with('bests',$bests)->with('total',$total);
       
    } 
      
} 


public function restaurant()
{
    $user = Auth::user();
        
    if(!$user){
        $userid=0;
    }else{
        $userid=$user->id;
    } 
   
    $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
    if(!$parentid) {
       $cart = NULL;   
        
       $cartnum = "";
     if(!$cartnum){
         $cartnum=0;
         }else{
              $cartnum = $cart->count();
               }  
   $vendors=Vendor::where('category','Restaurants')->where('verified','1')->orderby('id','desc')->paginate(5);
        return view('restaurant', compact('vendors','cartnum'));
    }else {
        $part = $parentid->parent_id;
      $cart = Cart::where('parent_id',$part)->get();
        $cartnum = $cart->count();
        if(!$cartnum){
           $cartnum=0;
           }else{
              $cartnum = $cart->count();
                } 
                $vendors=Vendor::where('category','Restaurants')->where('verified','1')->orderby('id','desc')->paginate(5);
                return view('restaurant', compact('vendors','cartnum'));
}
}



public function port()
{
    $user = Auth::user();
        
    if(!$user){
        $userid=0;
    }else{
        $userid=$user->id;
    } 
   
    $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
    if(!$parentid) {
       $cart = NULL;   
        
       $cartnum = "";
     if(!$cartnum){
         $cartnum=0;
         }else{
              $cartnum = $cart->count();
               }  
   $vendors=Vendor::where('category','Food Port')->where('verified','1')->orderby('id','desc')->paginate(5);
        return view('port.ports', compact('vendors','cartnum'));
    }else {
        $part = $parentid->parent_id;
      $cart = Cart::where('parent_id',$part)->get();
        $cartnum = $cart->count();
        if(!$cartnum){
           $cartnum=0;
           }else{
              $cartnum = $cart->count();
                } 
                $vendors=Vendor::where('category','Food Port')->where('verified','1')->orderby('id','desc')->paginate(5);
                return view('port.ports', compact('vendors','cartnum'));
}
}

public function cafe()
{
    $user = Auth::user();
        
    if(!$user){
        $userid=0;
    }else{
        $userid=$user->id;
    } 
   
    $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
    if(!$parentid) {
       $cart = NULL;   
        
       $cartnum = "";
     if(!$cartnum){
         $cartnum=0;
         }else{
              $cartnum = $cart->count();
               }  
   $vendors=Vendor::where('category','Cafe/Eatery')->where('verified','1')->orderby('id','desc')->paginate(5);
        return view('cafe.index', compact('vendors','cartnum'));
    }else {
        $part = $parentid->parent_id;
      $cart = Cart::where('parent_id',$part)->get();
        $cartnum = $cart->count();
        if(!$cartnum){
           $cartnum=0;
           }else{
              $cartnum = $cart->count();
                } 
                $vendors=Vendor::where('category','Cafe/Eatery')->where('verified','1')->orderby('id','desc')->paginate(5);
                return view('cafe.index', compact('vendors','cartnum'));
}
}

public function wellness()
{
    $user = Auth::user();
        
    if(!$user){
        $userid=0;
    }else{
        $userid=$user->id;
    } 
   
    $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
    if(!$parentid) {
       $cart = NULL;   
        
       $cartnum = "";
     if(!$cartnum){
         $cartnum=0;
         }else{
              $cartnum = $cart->count();
               }  
   $vendors=Vendor::where('category','Healthy & Wellness Products')->where('verified','1')->orderby('id','desc')->paginate(5);
        return view('wellness.index', compact('vendors','cartnum'));
    }else {
        $part = $parentid->parent_id;
      $cart = Cart::where('parent_id',$part)->get();
        $cartnum = $cart->count();
        if(!$cartnum){
           $cartnum=0;
           }else{
              $cartnum = $cart->count();
                } 
                $vendors=Vendor::where('category','Healthy & Wellness Products')->where('verified','1')->orderby('id','desc')->paginate(5);
                return view('wellness.index', compact('vendors','cartnum'));
}
}

public function company()
{
    $user = Auth::user();
        
    if(!$user){
        $userid=0;
    }else{
        $userid=$user->id;
    } 
   
    $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
    if(!$parentid) {
       $cart = NULL;   
        
       $cartnum = "";
     if(!$cartnum){
         $cartnum=0;
         }else{
              $cartnum = $cart->count();
               }  
   $vendors=Vendor::where('category','Food Company')->where('verified','1')->orderby('id','desc')->paginate(5);
        return view('company.index', compact('vendors','cartnum'));
    }else {
        $part = $parentid->parent_id;
      $cart = Cart::where('parent_id',$part)->get();
        $cartnum = $cart->count();
        if(!$cartnum){
           $cartnum=0;
           }else{
              $cartnum = $cart->count();
                } 
                $vendors=Vendor::where('category','Food Company')->where('verified','1')->orderby('id','desc')->paginate(5);
                return view('company.index', compact('vendors','cartnum'));
}
}


public function equipment()
{
    $user = Auth::user();
        
    if(!$user){
        $userid=0;
    }else{
        $userid=$user->id;
    } 
   
    $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
    if(!$parentid) {
       $cart = NULL;   
        
       $cartnum = "";
     if(!$cartnum){
         $cartnum=0;
         }else{
              $cartnum = $cart->count();
               }  
   $vendors=Vendor::where('category','Food Equipment')->where('verified','1')->orderby('id','desc')->paginate(5);
        return view('equipment.index', compact('vendors','cartnum'));
    }else {
        $part = $parentid->parent_id;
      $cart = Cart::where('parent_id',$part)->get();
        $cartnum = $cart->count();
        if(!$cartnum){
           $cartnum=0;
           }else{
              $cartnum = $cart->count();
                } 
                $vendors=Vendor::where('category','Food Equipment')->where('verified','1')->orderby('id','desc')->paginate(5);
                return view('equipment.index', compact('vendors','cartnum'));
}
}

public function checkout($unit = 'k')
{

    $user = Auth::user();
    $userid=$user->id;
    $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
    if(!$parentid) {
        $cart = NULL;
        $vatTopay=0;
         $price=0;
          $delivery=0;
        $cartnum = "";
        if(!$cartnum){
           $cart=0;
            }else{
               $cartnum = $cart->count();
                }  
                $sales = Cart::where('user_id',$userid)->orderby('id','desc')->get();
    
                        return view('checkout')->with('cart',$cart)->with('cartnum',$cartnum)->with('sales',$sales);
                    
    }else {
    $part = $parentid->parent_id;
    $vendor_id= $parentid->vendor_id;
    
    $cart = Cart::where('parent_id',$part)->get();
    $cartnum = $cart->count();
    if(!$cartnum){
       $cart=0;
        }else{
           $cartnum = $cart->count();
            } 
              $sales = Cart::where('user_id',$userid)->orderby('id','desc')->get();
              foreach($sales as $sale){
                $details=Vendor::where('id',$sale->vendor_id)->orderby('id','desc')->first();
              }
            
$apiKey = 'AIzaSyD1wnXxMJDWVXxG9SEAgZyq_VIMO4fB9-s';
  $vendor_address=$details->address; 
  $user_address =Auth::user()->address;

 

  $vendor_geocode = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?origins='.urlencode($vendor_address).'&destinations='.urlencode($user_address).'&key='.$apiKey);
  $vendor_geocode = json_decode($vendor_geocode);
  
  $status=$vendor_geocode->status;
  if($status !="INVALID_REQUEST"){
       $distance = $vendor_geocode->rows[0]->elements[0]->distance->text;
  
  $distance=chop($distance,"km ");
  }else{
      $distance=0;
  }
 
 
            $vatTopay=0;
            $delivery=$distance * 5;
           $price = Cart::where('user_id',$user->id)->sum('price');
           
           $total=$price + $delivery;
           

     $sales = Cart::where('user_id',$userid)->orderby('id','desc')->get();
    

     $vendor= Vendor::where('id',$vendor_id)->orderby('id','desc')->first();
     $subAccount= $vendor->subAid;
        
        
    return view('checkout')->with('sales',$sales)->with('subAccount',$subAccount)->with('cart',$cart)->with('cartnum',$cartnum)->with('total',$total)->with('vatTopay',$vatTopay)->with('price',$price)->with('delivery',$delivery);
           
} 
  
} 


public function update_user(Request $request, $id)
    {
       

        $agent = User::find($id);
        $agent ->phone =$request -> input('phone');
        $agent ->state =$request -> input('state');
        $agent ->city =$request -> input('city');
        $agent ->address =$request -> input('address');
         $agent -> save();
 
          return redirect()->back();
    }
    
    
     public function profile($id)
    {
        
   
        $user = Auth::user();
        
        if(!$user){
            $userid=0;
             return redirect('/');
        }else{
            $userid=$user->id;
        } 
    $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
    if(!$parentid) {
        $cart = NULL;
        $vatTopay=0;
         $price=0;
          $delivery=0;
        $cartnum = "";
        if(!$cartnum){
           $cart=0;
            }else{
               $cartnum = $cart->count();
                }  
         $agent = User::find($id);
            return view('profile')->with('cartnum',$cartnum);
    }else {
        $part = $parentid->parent_id;
      $cart = Cart::where('parent_id',$part)->get();
        $cartnum = $cart->count();
        if(!$cartnum){
           $cartnum=0;
           }else{
              $cartnum = $cart->count();
                } 
                 $agent = User::find($id);
            return view('profile')->with('cartnum',$cartnum);
    }
    }
    public function update_profile(Request $request, $id)
    {
         //handle photo upload
    if($request->hasfile('image')){
        //get file name and extension
        $filenamewithext=$request->file('image')->getClientOriginalName();
        //get just file name
        $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);
        //get just extension
        $extension = $request -> file('image') -> getClientOriginalExtension();
        // filename to store
        $filenametostore= $filename. '_'.time().'.'.$extension;
        //upload image
        $path =$request->file('image')->storeAs('public/user_image', $filenametostore);
    } else{
        $filenametostore='noimage.jpg';
    }
       

        $agent = User::find($id);
        $agent ->phone =$request -> input('phone');
        $agent ->state =$request -> input('state');
        $agent ->city =$request -> input('city');
        $agent ->address =$request -> input('address');
        $agent->image=$filenametostore;
         $agent -> save();
 
          return redirect()->back()->with('success','Profile Updated');
    }
    
    public function reviews(Request $request)
    {
        
      
            $name = $request->input('name');
                $rating = $request->input('ratings');
                $comment = $request->input('comment');
                $product_id = $request->input('product_id');
               
                Comment::create([
                    'name'=>$name,
                    'ratings'=>$rating,
                    'comment' => $comment,
                    'product_id' => $product_id,
                    
                ]);
     $sale= Vendor::find($product_id);
     $comm1=Comment::where('ratings','1')->where('product_id',$sale->id)->orderby('id','desc')->get();
     $comm2=Comment::where('ratings','2')->where('product_id',$sale->id)->orderby('id','desc')->get();
     $comm3=Comment::where('ratings','3')->where('product_id',$sale->id)->orderby('id','desc')->get();
     $comm4=Comment::where('ratings','4')->where('product_id',$sale->id)->orderby('id','desc')->get();
     $comm5=Comment::where('ratings','5')->where('product_id',$sale->id)->orderby('id','desc')->get();
    
     $main = (5*$comm5->count() + 4*$comm4->count() + 3*$comm3->count() + 2*$comm2->count() + 1*$comm1->count()) / ($comm5->count()+$comm4->count()+$comm3->count()+$comm2->count()+$comm1->count());
     $sale->rating = number_format($main, 1);
         $sale->save();
                return redirect()->back()->with('success', 'Thanks for your review, Review more to get a meal discount');   

            }
            
            
            
            
              public function password_reset(Request $request)
    {
      $email = $request->input('email');
      
       $check=User::where('email',$email)->first();
if(!$check) {
   return redirect()->back()->with('success','User not found') ;
}
else {
    $user = User::find($check->id);
        $to = "$email"; 
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'EatNaija.com'; 
 
$subject = "Password Reset"; 
 
$htmlContent = '
     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style type="text/css">
a[x-apple-data-detectors] {color: inherit !important;}
</style>

</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 20px 0 30px 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
<tr>
<td align="center" bgcolor="#fd7e14" style="padding: 40px 0 30px 0;">
<img src="https://eatnaija.com/public/img/logo-white.png" alt="EatNaija Logo." width="200" height="150" style="display: block;" />
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif;">
<h1 style="font-size: 24px; margin: 0;">Hello '.$user->name.'!</h1>
</td>
</tr>
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
<p style="margin: 0;">Follow this link to reset your password <a href="http://eatnaija.com/password_reset'.$user->id.'">Reset Password</a></p>
</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td bgcolor="#fd7e14" style="padding: 30px 30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
<p style="margin: 0;">©2020 EatNaija</p>
</td>
<tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>'; 
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers); 
 
          return redirect()->back()->with('success','Email Sent');
    }
    }
    
 public function reset_password($id)
    {
        
   
       $user = Auth::user();
        
        if(!$user){
            $userid=0;
        }else{
            $userid=$user->id;
        } 
     $parentid = Cart::where('user_id',$userid)->orderby('id','desc')->first();
        if(!$parentid) {
           $cart = NULL;   
            
           $cartnum = "";
         if(!$cartnum){
             $cart=0;
             }else{
                  $cartnum = $cart->count();
                   }  
                    $user = User::find($id);
            return view('reset')->with('cartnum',$cartnum)->with('user',$user);
    }else {
        $part = $parentid->parent_id;
      $cart = Cart::where('parent_id',$part)->get();
        $cartnum = $cart->count();
        if(!$cartnum){
           $cartnum=0;
           }else{
              $cartnum = $cart->count();
                } 
                 $user = User::find($id);
            return view('reset')->with('cartnum',$cartnum)->with('user',$user);
    }
    }
    
    
    public function reset(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:8|confirmed',
          ]);
 
        $user = User::find($id);
        $user ->password =Hash::make($request['password']);        
        $user -> save();
 
          return redirect('/')->with('error','Password Updated');
    }
    
      public function newsletter(Request $request)
    {
        $user = New Newsletter;
        $user ->email=$request->input('email');        
        $user -> save();
 
          return redirect('/')->with('success','You have successfully subscribed to our newletters');
    }
    
     public function report(Request $request)
    {
        $user = New Report;
        $user ->company_name=$request->input('company_name');
         $user ->company_id=$request->input('company_id');
        $user ->reports=$request->input('reports');        
        $user -> save();
 
          return redirect()->back()->with('success','Your Report has been submitted');
    }
    
    
      public function contact(Request $request)
    {
      $email = $request->input('email');
       $name = $request->input('name');
     $message = $request->input('message');
      $subject = $request->input('subject');
      
        $agents = Contact::create([
            'email' => $email,
            'name' => $name,
            'subject' => $subject,
            'message' => $message,
        ]);
       
$to = "eat9aija@gmail.com"; 
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'EatNaija Contact Form'; 
 
$subject = "$subject"; 
 
$htmlContent = '
     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style type="text/css">
a[x-apple-data-detectors] {color: inherit !important;}
</style>

</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 20px 0 30px 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
<tr>
<td align="center" bgcolor="#fd7e14" style="padding: 40px 0 30px 0;">
<img src="https://eatnaija.com/public/img/logo-white.png" alt="EatNaija Logo." width="200" height="150" style="display: block;" />
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif;">
<h1 style="font-size: 24px; margin: 0;">Email from '.$name.'!</h1>
<p><a href="'.$email.'">'.$email.'</a></p>
</td>
</tr>
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
<p style="margin: 0;">'.$message.'</p>
</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td bgcolor="#fd7e14" style="padding: 30px 30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
<p style="margin: 0;">©2020 EatNaija</p>
</td>
<tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>'; 
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers); 
 
          return redirect()->back()->with('success','Email Sent');
    }
   
    
}
