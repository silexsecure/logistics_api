<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Vendor;
use App\Models\Categories;
use App\Models\Logistics;
use App\Models\Category;
use App\Models\User;
use DB;
use App\Models\Sales;
use Config;
use App\Models\Cart;
use App\Models\Checkout;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Carbon\Carbon;
use Rave;
class VendorappController extends Controller
{
    
   public function __construct() {
       
        $this->vendors = new Vendor;
    }

     
        
    public function authenticate(Request $request)
        {
             config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
           
      
            $credentials = $request->only('email', 'password');
 
            try {
                
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 400);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }
             if (JWTAuth::user()->suspend==1) {
                    return response()->json(['error' => 'Your Account has been Suspended'], 400);
                }
            $user = JWTAuth::user();
             $userid= $user->id;
             $total=Checkout::where('vendor_id', $userid)->where('accept','1')->sum('price');
        $last=Checkout::where('vendor_id', $userid)->where('accept','1')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->sum('price');
        $present=Checkout::where('vendor_id', $userid)->where('accept','1')->where('created_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->sum('price');
                $soldP=Checkout::where('vendor_id', $userid)->where('accept','1')->where('created_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->count();
                        $soldL=Checkout::where('vendor_id', $userid)->where('accept','1')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->count();
            return response()->json(['message'=>'Login Successful','vendor'=> $user,'token'=> $token,'total'=>(int)$total,'lastmonth'=>(int)$last,'presentmonth'=>(int)$present,'soldlastmonth'=>(int)$soldL,'soldthismonth'=>(int)$soldP]);
        }
        
        
         public function dashboard(Request $request)
        {
             config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
            $user=JWTAuth::parseToken()->authenticate();
             $userid= $user->id;
             $total=Checkout::where('vendor_id', $userid)->where('accept','1')->sum('price');
        $last=Checkout::where('vendor_id', $userid)->where('accept','1')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->sum('price');
        $present=Checkout::where('vendor_id', $userid)->where('accept','1')->where('created_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->sum('price');
                $soldP=Checkout::where('vendor_id', $userid)->where('accept','1')->where('created_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->count();
                        $soldL=Checkout::where('vendor_id', $userid)->where('accept','1')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->count();
            return response()->json(['vendor'=> $user,'total'=>$total,'lastmonth'=>$last,'presentmonth'=>$present,'soldlastmonth'=>$soldL,'soldthismonth'=>$soldP]);
        }

 protected function create_vendor(Request $request)
    {  
       config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);

$validator = Validator::make($request->all(), [
               
                'email' => 'required|string|email|max:255|unique:vendor',
               

            ]);

            if($validator->fails()){
                    return response()->json($validator->errors()->toJson(), 400);
            }

 if($request->hasfile('image')){
    //get file name and extension
    $filenamewithext=$request->file('image')->getClientOriginalName();
    //get just file name
    $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);
    //get just extension
    $extension = $request -> file('image') -> getClientOriginalExtension();
    // filename to store
    $filenametostore= $filename. '_'.time().'.'.$extension;
    //upload image
    $path =$request->file('image')->storeAs('public/vendor_image', $filenametostore);
} else{
    $filenametostore='noimage.jpg';
}
            $vendor = Vendor::create([
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'state' => $request->get('state'),
            'city' => $request->get('city'),
            'password' =>Hash::make($request['password']),
            'image' =>$request->get('image'),
            'address' => $request->get('address'),
            'company_name' => $request->get('company_name'),
            'category' => $request->get('category'),
            'address_log' => $request->get('address_log'),
            'address_lat' => $request->get('address_lat'),
            'verified'=>1,
        ]);
        $token = JWTAuth::fromUser($vendor);
            return response()->json(['message'=>'Login Successful','vendor'=> $vendor,'token'=> $token]);
    }

 public function store(Request $request)
    {
        config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
		
$vendor=JWTAuth::parseToken()->authenticate();
    $sale = new Sales;
    $sale ->name = $request->input('name');
    $sale ->category = $request->input('category');
    $sale ->price = $request->input('price');
    $sale ->promo_price = $request->input('promo_price');
    $sale ->vendor_name =$vendor->company_name ;
    $sale ->description = $request->input('description');
    $sale ->vendor_id = $vendor->id;
    $sale ->main_category = $vendor->category;
    $sale ->location = $vendor->state;
    $sale->image=$request->get('image');
    $sale-> save();


       return response()->json(['sale'=>$sale ,'message'=>'Upload Successful']);
          
    }

 public function update_store(Request $request,$id)
    {
        config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
		
$vendor=JWTAuth::parseToken()->authenticate();
    $sale = Sales::find($id);
    $sale ->name = $request->input('name');
    $sale ->category = $request->input('category');
    $sale ->price = $request->input('price');
    $sale ->promo_price = $request->input('promo_price');
    $sale ->vendor_name =$vendor->company_name ;
    $sale ->description = $request->input('description');
    $sale ->vendor_id = $vendor->id;
    $sale ->main_category = $vendor->category;
    $sale->image=$request->get('image');
    $sale-> save();


       return response()->json(['sale'=>$sale ,'message'=>'Update Successful']);
          
    }


public function delete_store($id)
    {
        config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
		
$vendor=JWTAuth::parseToken()->authenticate();
    $sale = Sales::find($id);
    $sale->delete();


       return response()->json(['sale'=>$sale ,'message'=>'Post Deleted']);
          
    }


public function products(Request $request )
{  
     config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
   
    $user = JWTAuth::parseToken()->authenticate();
    $userid= $user->id;
    $sales= Sales::where('vendor_id',$userid)->orderby('id','desc')->get();
   

    return response()->json(['sales'=>$sales]);

    
}
public function categories(Request $request )
{ 
$categories = DB::select('select * from category');
 
   return response()->json(['categories'=>$categories]);
}

public function requests(Request $request )
{ 
 config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
   
    $user = JWTAuth::parseToken()->authenticate();
    $vendorid= $user->id;
  $cats=Checkout::where('vendor_id',$vendorid)->orderby('id','desc')->get();
   return response()->json(['Requests'=>$cats]);
}

public function five_requests(Request $request )
{ 
 config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
   
    $user = JWTAuth::parseToken()->authenticate();
    $vendorid= $user->id;
  $cats=Checkout::where('vendor_id',$vendorid)->orderby('id','desc')->take(5)->get();
   return response()->json(['Requests'=>$cats]);
}

public function accept(Request $request, $id )
{ 
 config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
   $req= Checkout::find($id);
   $req->accept=1;
   $req->save();
   
   $venderID=$req->vendor_id;
    $venderIDDetails = Vendor::find($venderID);
    
   
   $len=8;
  $char = 'ABCDEF567890';
    $randomNumber = '';
    for ($i = 0; $i < $len; $i++) {
        $randomNumber .= $char[rand(0, $len - 1)];
    }
    
   
    $logistic = Logistics::create([
            'name' => $req->name,
            'pickup' => $venderIDDetails->address,
            'depart' => $req->address1,
            'currentadd' => $req->address1,
            'cost' => $req->price,
            'payment' => $req->total,
            'phone' => $req->phone,
            'delivery_code' => $randomNumber,
        ]);
   
   
   return response()->json(['message'=>'Request has been accepted', 'Request'=>$req, 'logistic'=>$logistic ]);
}

public function reject(Request $request, $id )
{ 
 config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
   $req= Checkout::find($id);
   $req->reject=1;
   $req->save();
   return response()->json(['message'=>'Request has been rejected', 'Request'=>$req]);
}

public function deliver(Request $request, $id )
{ 
 config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
   $req= Checkout::find($id);
   $req->delivered=1;
   $req->save();
   return response()->json(['message'=>'Product has been delivered', 'Request'=>$req]);
}

public function amount(Request $request )
{  
     config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
   
    $user = JWTAuth::parseToken()->authenticate();
    $userid= $user->id;
        $total=Checkout::where('vendor_id', $userid)->where('accept','1')->sum('price');
        $last=Checkout::where('vendor_id', $userid)->where('accept','1')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->sum('price');
        $present=Checkout::where('vendor_id', $userid)->where('accept','1')->where('created_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->sum('price');


    return response()->json(['Total'=>$total,'Last month'=>$last,'Present month'=>$present]);

    
}

 protected function createcategory(Request $request)
    { 
         config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
   
    $user = JWTAuth::parseToken()->authenticate();
    $userid= $user->id;
 $category = Category::create([
            'name' => $request->get('name'),
            'vendor_id'=>$userid,
            
        ]);
    return response()->json(['Category'=>$category]);
    }
    
     public function category(Request $request)
    {
 config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
   
    $user = JWTAuth::parseToken()->authenticate();
    $userid= $user->id;
       $categories = Category::where('vendor_id',$userid)->orderby('id', 'desc')->get()->pluck('name')->toArray();
    return response()->json(['Categories'=>$categories]);
   
}


public function changePassword(Request $request){
    
    config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
      $request->get('currentPassword');
      $request->get('newPassword');
    $userr = JWTAuth::parseToken()->authenticate();


        if (!(Hash::check($request->get('currentPassword'), $userr->password))) {
            // The passwords matches
            return response()->json(['status'=>'false','message'=>'Your current password does not match with the password you provided. Please try again.']);
        }

        if(strcmp($request->get('currentPassword'), $request->get('newPassword')) == 0){
            //Current password and new password are same
             return response()->json(['status'=>'false','message'=>'New Password cannot be same as your current password. Please choose a different password.']);
        }


        //Change Password
        $user = JWTAuth::user();
        $user->password =Hash::make($request->get('newPassword'));
        $user->save();

                     return response()->json(['status'=>'true','message'=>'Password changed successfully !']);

    }

public function update_vendor(Request $request)
    {
        config()->set( 'auth.defaults.guard', 'vendor' );
          \Config::set('jwt.user', 'App\Models\Vendor'); 
		\Config::set('auth.providers.vendors.model', \App\Models\Vendor::class);
		
            $user=  JWTAuth::parseToken()->authenticate();
            
            
            $userid= $user->id;
            $subAid= $user->subAid;
            
            $vendor = Vendor::find($userid);
            
            
            $vendor->email = $request->get('email');
            $vendor->phone = $request->get('phone');
            $vendor->state = $request->get('state');
            $vendor->city = $request->get('city');
            $vendor->image =$request->get('image');
            $vendor->address = $request->get('address');
            $vendor->company_name = $request->get('company_name');
            $vendor->category = $request->get('category');
             $vendor->bank_name = $request->get('bank_name');
            $vendor->bank_code = $request->get('bank_code');
             $vendor->account = $request->get('account');
             
          
if($subAid==""){
    $url = "https://api.paystack.co/subaccount";
  $fields = [
    'business_name' => $request->get('company_name'),
    'settlement_bank' => $request->get('bank_code'), 
    'account_number' => $request->get('account'), 
    'percentage_charge' => 5.0 
  ];
  $fields_string = http_build_query($fields);
  //open connection
  $ch = curl_init();
  
  //set the url, number of POST vars, POST data
  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_POST, true);
  curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "Authorization: Bearer sk_live_c2d2bd5f33c48bc7222b31a3875390fcc111945d",
    "Cache-Control: no-cache",
  ));
  
  //So that curl_exec returns the contents of the cURL; rather than echoing it
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 
  
  //execute post
  $result = curl_exec($ch);
  $obj = json_decode($result,true);
  $s= $obj['status'];
  
  if($s=="true"){
                    $sub_accountID= $obj['data']['integration'];
                    $subaccount_code=$obj['data']['subaccount_code'];
                    
                    $vendor->sub_accountID = $sub_accountID;
                     $vendor->subAid = $subaccount_code;
                }
}

            $vendor->save();
            


       return response()->json(['status'=>'true','message'=>'Update Successful','data'=>$vendor]);
          
    }

  public function password_reset(Request $request)
    {
      $email = $request->input('email');
      
       $check=Vendor::where('email',$email)->first();
if(!$check) {
   return response()->json(['status'=>'false','message'=>'Email not found'],409);
}
else {
    $user = Vendor::find($check->id);
        $to = "$email"; 
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'EatNaija.com'; 
 
$subject = "Password Reset"; 
 
$htmlContent = '
     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style type="text/css">
a[x-apple-data-detectors] {color: inherit !important;}
</style>

</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 20px 0 30px 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
<tr>
<td align="center" bgcolor="#fd7e14" style="padding: 40px 0 30px 0;">
<img src="https://eatnaija.com/public/img/logo-white.png" alt="EatNaija Logo." width="200" height="150" style="display: block;" />
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif;">
<h1 style="font-size: 24px; margin: 0;">Hello '.$user->company_name.'!</h1>
</td>
</tr>
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
<p style="margin: 0;">Follow this link to reset your password <a href="https://eatnaija.com/resetvendorpassword'.$user->id.'">Reset Password</a></p>
</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td bgcolor="#fd7e14" style="padding: 30px 30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
<p style="margin: 0;">©2020 EatNaija</p>
</td>
<tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>'; 
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers); 
 
           return response()->json(['status'=>'true','message'=>'Email Sent']);
    }
    }
    
    
    public function reset_password3($id)
    {
        
      $user = Vendor::find($id);
     return view('vendor.reset')->with('user',$user);
   
    }
    
    
    public function reset3(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:8|confirmed',
          ]);
 
        $user = Vendor::find($id);
        $user ->password =Hash::make($request['password']);        
        $user -> save();
 
          return redirect()->back()->with('error','Password Updated');
    }
 public function bank_details(Request $request)
    {
        $country = 'NG';
        $data = Rave::listofBankForTransfer($country);
        $banks = json_decode(json_encode($data), true);
        
        
           return response()->json(['banks'=>$banks]);
 }
 
    public function getVendorDetails($id){
        $vendor = Vendor::find($id);
        return response()->json(['vendor_details'=>$vendor]);
    }

    public function amountInKM(){
        $amountInKM=5;
         return response()->json(['amountInKM'=>$amountInKM]);
    }
    
    
    function getDistanceAmount(Request $request){
         $vendor_id=$request->get('vendor_id');
         $user_id=$request->get('user_id');
         
        
         
          $vendor = Vendor::find($vendor_id);
          $vendor_address=$vendor->address;
          
          $user=User::find($user_id);
          $user_address=$user->address;
          
         $apiKey = 'AIzaSyD1wnXxMJDWVXxG9SEAgZyq_VIMO4fB9-s';
          $vendor_geocode = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?origins='.urlencode($vendor_address).'&destinations='.urlencode($user_address).'&key='.$apiKey);
  $vendor_geocode = json_decode($vendor_geocode);
  
  $status=$vendor_geocode->status;
  if($status !="INVALID_REQUEST"){
       $distance = $vendor_geocode->rows[0]->elements[0]->distance->text;
  
  $distance=chop($distance,"km ");
  }else{
      $distance=0;
  }
 
 
            $vatTopay=0;
            $delivery_price=$distance * 5;
             return response()->json(['delivery_price'=>$delivery_price]);
    }
    
    }