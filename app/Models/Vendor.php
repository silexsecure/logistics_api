<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
class Vendor extends  Authenticatable implements JWTSubject
{
          protected $guard = 'vendor';

      protected $fillable = [
        'email',
        'phone',
        'address',
        'state' ,
        'city',
        'company_name',
        'password',
        'image' ,
        'category',
        'suspend',
        'verified',
        'bank_name',
        'bank_code',
        'account',
        'sub_accountID',
        'subAid',
];

protected $table = 'vendor';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}
