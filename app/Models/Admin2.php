<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Admin2 extends model 
{
    use HasFactory;

    protected $guard = 'admin';
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'image' ,
];

protected $table = 'admin2';

protected $hidden = [
    'password', 'remember_token',
];

}
