<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class deliveryMan extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'phone',
        'email' ,
        'password',
        'image',
      
];
protected $table ='delivery_men';
public $primarykey = 'id';
}
