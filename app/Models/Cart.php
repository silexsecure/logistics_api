<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    protected $fillable = [
        'product_id','parent_id','user_id', 'description', 'product_name','price','total','category','quantity','image','vendor_id','vendor_name','main_price'
    ];

    protected $table ='cart';
    public $primarykey = 'id';
    public $timestamps = true;
}
