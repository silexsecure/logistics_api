<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;

class Admin extends model implements AuthContract 
{
    use Authenticatable;

    protected $guard = 'admin';
    protected $fillable = [
        'name',
        'email',
        'password',
        'image' ,
];

protected $table = 'admin';

protected $hidden = [
    'password', 'remember_token',
];

}
