<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'phone',
        'state' ,
        'city' ,
        'address1',
        'address2' ,
        'user_id',
        'price',
        'product',
        'payment_status',
        'vendor_id',
        'cart_id',
        'product_id',
        'pally_id',
        'quantity',
        'image',
        'delivered',
        'accept',
        'reject',
        'order_no',
        'total',
      
];
protected $table ='checkout';
public $primarykey = 'id';
}
