<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'category',
        'price',
        'promo_price',
        'description',
        'location' ,
        'vendor_id' ,
        'vendor_name' ,
        'image' ,
        'main_category',
];

protected $table = 'sales';
}
